import React from "react";
import {
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  Platform,
} from "react-native";
import DefaultScreen from "../../components/DefaultScreen";
import AppModal from "../../components/Modal";
import AppInput from "../../components/AppInput";
import ServiceHeader from "../../components/ServiceHeader";
import ServiceAmountChargePrev from "../../components/ServiceAmountChargePrev";
import ConfirmModal from "../../components/ConfirmModal";
import SuccessModal from "../../components/SuccessModal";
import PreviewButton from "../../components/PreviewButton";
import axios from "axios";
import ConfirmConnection from "../../ConfirmNetConnection";
import GLOBAL from "../../globalState";
import GLOBAL_FUNCTION from "../../globalFunctions";
import { Ionicons } from "@expo/vector-icons";
const { width } = Dimensions.get("window");

export default class SmsPinScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      smsContacts: JSON.parse(JSON.stringify(GLOBAL.smsRecipients)),
      pin: "",
      trxid: "",
      datetime: "",
      isLoading: false,
      isOpenSmsPrevModal: false,
      isSmsContactsPrevModal: false,
      isConfirmModalVisible: false,
      isSuccessModalVisible: false,
    };
  }

  static navigationOptions = ({ navigation }) => ({
    headerLeft: (
      <Ionicons
        name={Platform.OS === "ios" ? "ios-arrow-back" : "md-arrow-back"}
        size={Platform.OS === "ios" ? 35 : 24}
        color={"#fff"}
        style={
          Platform.OS === "ios"
            ? { marginBottom: -4, width: 25, marginLeft: 9 }
            : { marginBottom: -4, width: 25, marginLeft: 20 }
        }
        onPress={() => {
          navigation.goBack();
        }}
      />
    ),
    title: GLOBAL.serviceName,
    headerTintColor: "#fff",
    headerStyle: {
      backgroundColor: "#00cec9",
    },
    headerTitleStyle: {
      color: "#fff",
    },
  });

  componentDidMount() {}
  toggleModal = () => {
    this.setState({
      isConfirmModalVisible: !this.state.isConfirmModalVisible,
    });
  };
  toggleSmsPrevModal = () => {
    this.setState({
      isOpenSmsPrevModal: !this.state.isOpenSmsPrevModal,
    });
  };
  toggleSmsContactsPrevModal = () => {
    this.setState({
      isSmsContactsPrevModal: !this.state.isSmsContactsPrevModal,
    });
  };
  smsPrevContent = () => {
    return (
      <ScrollView style={{ padding: 20 }}>
        <Text style={{ fontSize: width * 0.05 }}>{GLOBAL.smsText}</Text>
        <View style={{ height: 40 }}></View>
      </ScrollView>
    );
  };

  smsRecipients = () => {
    //console.log(GLOBAL.smsRecipients);
    let reserveArr = this.state.smsContacts;
    return (
      <ScrollView style={{ padding: 20 }}>
        {reserveArr.map((item, key) => (
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-between",
              padding: width * 0.015,
            }}
            key={key}
          >
            <Text style={{ fontSize: width * 0.07 }}>{item}</Text>
            <TouchableOpacity
              style={{
                paddingRight: width * 0.015,
                paddingLeft: width * 0.015,
              }}
              onPress={() => {
                reserveArr.splice(key, 1);
                this.setState({ smsContacts: reserveArr });
              }}
            >
              <Ionicons name="md-trash" size={width * 0.07} color="#B83227" />
            </TouchableOpacity>
          </View>
        ))}
        <View style={{ height: 40 }}></View>
      </ScrollView>
    );
  };

  toggleSuccessModal = () => {
    this.setState({ isSuccessModalVisible: !this.state.isSuccessModalVisible });
  };

  sendMoney = async () => {
    const connectionStatus = await ConfirmConnection.CheckNetConnection();
    if (connectionStatus) {
      this.setState({ isLoading: !this.state.isLoading });

      var allData = {
        apps_key: GLOBAL.rechargeScreen.apps_key,
        token: GLOBAL.rechargeScreen.token,
        phone_no: this.state.phone_no,
        amount: this.state.amount,
        type: this.state.type,
        operator: null,
        pin: this.state.pin,
        service_id: GLOBAL.service_id,
      };

      axios
        .post(`https://${GLOBAL.appUrl}/api/requestSubmit`, allData)
        .then((res) => {
          if (res.data.status == 1) {
            this.toggleModal();
            GLOBAL.userBalance = res.data.balance;
            this.setState({
              isLoading: !this.state.isLoading,
              trxid: res.data.trxid,
              datetime: res.data.datetime,
            });
            this.toggleSuccessModal();
          } else if (res.data.status == 50) {
            GLOBAL_FUNCTION.logOut(this.props);
          } else if (res.data.status == 40) {
            this.setState({ isLoading: !this.state.isLoading });
            alert(res.data.message);
          }
        });
    }
  };

  render() {
    return (
      <DefaultScreen
        gridFlex={true}
        screenData={
          <View>
            <ConfirmModal
              toggleModal={this.toggleModal}
              confirmAction={this.sendMoney}
              isModalVisible={this.state.isConfirmModalVisible}
              confirmTitle={GLOBAL.serviceName}
              stateData={this.state}
              data={[
                {
                  text: "TOTAL SMS",
                  value: GLOBAL.totalSms,
                  rightBorder: 1,
                  bottomBorder: 0,
                },
                {
                  text: "PER SMS",
                  value: 0.2,
                  rightBorder: 0,
                  bottomBorder: 0,
                },
                {
                  text: "TOTAL",
                  value: Number((GLOBAL.totalSms * 0.2).toFixed(2)),
                  rightBorder: 1,
                  bottomBorder: 1,
                },
                {
                  text: "NEW BALANCE",
                  value:
                    Number(GLOBAL.userBalance) -
                    Number((GLOBAL.totalSms * 0.2).toFixed(2)),
                  rightBorder: 0,
                  bottomBorder: 1,
                },
              ]}
            />

            <SuccessModal
              toggleModal={() => {
                this.toggleSuccessModal();
                GLOBAL_FUNCTION.backToHome(this.props);
              }}
              confirmTitle={GLOBAL.serviceName}
              isSuccessModalVisible={this.state.isSuccessModalVisible}
              stateData={this.state}
              data={[
                {
                  text: "TRANS ID",
                  value: this.state.trxid,
                  rightBorder: 1,
                  bottomBorder: 0,
                },
                {
                  text: "REQUEST TIME",
                  value: this.state.datetime,
                  rightBorder: 0,
                  bottomBorder: 0,
                },
                {
                  text: "TOTAL SMS",
                  value: GLOBAL.totalSms,
                  rightBorder: 1,
                  bottomBorder: 0,
                },
                {
                  text: "PER SMS",
                  value: 0.2,
                  rightBorder: 0,
                  bottomBorder: 0,
                },
                {
                  text: "TOTAL",
                  value: Number((GLOBAL.totalSms * 0.2).toFixed(2)),
                  rightBorder: 1,
                  bottomBorder: 1,
                },
                {
                  text: "NEW BALANCE",
                  value:
                    Number(GLOBAL.userBalance) -
                    Number((GLOBAL.totalSms * 0.2).toFixed(2)),
                  rightBorder: 0,
                  bottomBorder: 1,
                },
              ]}
            />

            <AppModal
              toggleModal={this.toggleSmsPrevModal}
              modalContent={this.smsPrevContent()}
              isModalVisible={this.state.isOpenSmsPrevModal}
            />
            <AppModal
              toggleModal={this.toggleSmsContactsPrevModal}
              modalContent={this.smsRecipients()}
              isModalVisible={this.state.isSmsContactsPrevModal}
            />

            <ServiceHeader
              title={"FOR"}
              imgHeightWidth={{
                width: width * 0.12,
                height: width * 0.12,
              }}
              centerData={GLOBAL.smsRecipient || GLOBAL.smsRecipients}
              imgName={GLOBAL.imgName}
              buttonPressed={this.toggleSmsContactsPrevModal}
            />

            <View
              style={{ borderBottomWidth: 1, borderBottomColor: "#ecf0f1" }}
            ></View>
            <ServiceAmountChargePrev
              amountTitle={"TOTAL SMS"}
              amount={GLOBAL.totalSms}
              chargeTitle={"PER SMS"}
              charge={"0.20"}
              hidePlusSign={true}
              total={(GLOBAL.totalSms * 0.2).toFixed(2)}
            />
            <PreviewButton
              buttonPressed={this.toggleSmsPrevModal}
              buttonTitle={"SMS PREVIEW"}
            />
            <AppInput
              placeHolder={"Enter PIN"}
              placeholderTextColor={"#bdc3c7"}
              keyboardType={"decimal-pad"}
              leftIcon={"lock"}
              leftIconColor={"#00CDC8"}
              rightIconColor={"#00CDC8"}
              autoFocus={true}
              inputValue={(inputValue) => this.setState({ pin: inputValue })}
              next={this.toggleModal}
              secureTextEntry={true}
            />
          </View>
        }
      />
    );
  }
}
