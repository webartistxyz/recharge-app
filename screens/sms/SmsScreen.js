import React from "react";
import {
  View,
  Dimensions,
  Text,
  TextInput,
  TouchableOpacity,
  Platform,
  StyleSheet,
} from "react-native";
import { Entypo } from "@expo/vector-icons";
import { Ionicons } from "@expo/vector-icons";
import DefaultScreen from "../../components/DefaultScreen";
import GLOBAL from "../../globalState";
const { width } = Dimensions.get("window");
export default class SmsScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      recipient: "",
      msg: "",
      type: "",
      maxCharInitial: 0,
      remainingChar: 0,
      messages: 0,
    };
  }

  static navigationOptions = ({ navigation }) => ({
    headerLeft: (
      <Ionicons
        name={Platform.OS === "ios" ? "ios-arrow-back" : "md-arrow-back"}
        size={Platform.OS === "ios" ? 35 : 24}
        color={"#fff"}
        style={
          Platform.OS === "ios"
            ? { marginBottom: -4, width: 25, marginLeft: 9 }
            : { marginBottom: -4, width: 25, marginLeft: 20 }
        }
        onPress={() => {
          navigation.goBack();
        }}
      />
    ),
    title: GLOBAL.serviceName,
    headerTintColor: "#fff",
    headerStyle: {
      backgroundColor: "#00cec9",
    },
    headerTitleStyle: {
      color: "#fff",
    },
  });

  componentDidMount() {}
  isUnicode(str) {
    for (var i = 0, n = str.length; i < n; i++) {
      if (str.charCodeAt(i) > 255) {
        return true;
      }
    }
    return false;
  }

  checkSms() {
    if (this.state.msg.length) {
      if (this.isUnicode(this.state.msg)) {
        this.setState(
          {
            type: "Unicode",
            maxCharInitial: 70,
            messages: 1,
          },
          () => this.get_character()
        );
      }

      if (!this.isUnicode(this.state.msg)) {
        this.setState(
          {
            type: "Plain",
            maxCharInitial: 160,
            messages: 1,
          },
          () => this.get_character()
        );
      }
    } else {
      return;
    }
  }
  get_character() {
    var chars = this.state.msg.length;
    var messages = Math.ceil(chars / this.state.maxCharInitial);
    this.setState({ messages }, () =>
      this.setState({
        remainingChar:
          Number(this.state.messages * this.state.maxCharInitial) - chars,
      })
    );
  }

  nextScreen = () => {
    GLOBAL.smsRecipient = this.state.recipient;
    GLOBAL.totalSms = this.state.messages;
    GLOBAL.smsType = this.state.type;
    GLOBAL.smsText = this.state.msg;
    this.props.navigation.navigate("SmsPin");
  };
  moveToSmsPhoneNO = () => {
    this.setState({ recipient: "" });
    GLOBAL.smsRecipient = "";
    GLOBAL.totalSms = this.state.messages;
    GLOBAL.smsType = this.state.type;
    GLOBAL.smsText = this.state.msg;
    this.props.navigation.navigate("SmsPhoneNunbers");
  };

  render() {
    return (
      <DefaultScreen
        gridFlex={true}
        screenData={
          <View style={{ alignItems: "center" }}>
            <View style={styles.textPadding}>
              <TextInput
                autoFocus={true}
                placeholder={"Write your message here"}
                placeholderTextColor={"#9E9E9E"}
                numberOfLines={5}
                multiline={true}
                maxHeight={width * 0.3}
                onChangeText={(msg) => {
                  this.setState({ msg }, () => this.checkSms());
                }}
              />
            </View>
            {this.state.msg.length ? (
              <View style={styles.textPadding}>
                <Text style={{ color: "#bdc3c7" }}>
                  {`[ ${this.state.remainingChar} characters remaining/${this.state.messages} Message(s) ]`}
                </Text>
              </View>
            ) : null}

            {/* <View style={{ marginBottom: 1 }}>
              <Text>Hello</Text>
            </View> */}
            <View style={styles.searchBox}>
              <TextInput
                style={styles.searchInput}
                autoCapitalize="none"
                placeholder="Recipient"
                autoCorrect={false}
                keyboardType="phone-pad"
                value={this.state.recipient}
                onChangeText={(recipient) => {
                  this.setState({ recipient });
                }}
              />

              {this.state.msg.length ? (
                <TouchableOpacity
                  onPress={this.moveToSmsPhoneNO}
                  style={styles.contactIcon}
                >
                  <Ionicons
                    name="md-person-add"
                    size={width * 0.06}
                    color="#bdc3c7"
                  />
                </TouchableOpacity>
              ) : null}
            </View>

            <View>
              {this.state.msg && this.state.recipient.length === 11 ? (
                <TouchableOpacity onPress={this.nextScreen}>
                  <Entypo
                    name="arrow-right"
                    size={width * 0.09}
                    color={"#00cec9"}
                  />
                </TouchableOpacity>
              ) : null}
            </View>
          </View>
        }
      />
    );
  }
}
const styles = StyleSheet.create({
  searchBox: {
    justifyContent: "center",
    flexDirection: "row",
    padding: width * 0.04,
    borderTopWidth: 1,
    borderTopColor: "#00cec9",
  },

  searchInput: {
    flex: 9,
    alignItems: "flex-start",
  },
  contactIcon: {
    flex: 1,
    alignItems: "flex-end",
    justifyContent: "center",
  },
  textPadding: {
    paddingBottom: width * 0.04,
    paddingLeft: width * 0.04,
    paddingRight: width * 0.04,
  },
});
