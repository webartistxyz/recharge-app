import React from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  TouchableOpacity,
  Alert,
  BackHandler,
  Dimensions,
} from "react-native";
import { NavigationEvents } from "react-navigation";
import DefaultScreen from "../components/DefaultScreen";
import GLOBAL from "../globalState";
import LoderModal from "../components/LoaderModal";
import { Ionicons } from "@expo/vector-icons";
import GLOBAL_FUNCTIONS from "../globalFunctions";
import ConfirmConnection from "../ConfirmNetConnection";
const { width, height } = Dimensions.get("window");

export default class HomeScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      services: this.props.navigation.getParam("userRole", []),
      apps_key: this.props.navigation.getParam("apps_key", ""),
      isTapped: true,
      showbalance: "TAP FOR BALANCE",
      isLoading: false,
    };
  }

  static navigationOptions = ({ navigation }) => ({
    title: "Netoch Home",
    headerLeft: (
      <TouchableOpacity
        style={{ paddingLeft: width * 0.085 }}
        onPress={() => navigation.openDrawer()}
      >
        <Ionicons name="md-menu" size={width * 0.09} color="#fff" />
      </TouchableOpacity>
    ),
    headerStyle: {
      backgroundColor: "#4050B5",
    },
    headerTintColor: "#fff",
    headerTitleStyle: {
      fontWeight: "bold",
    },
  });

  componentWillUnmount() {
    this._onBlurr();
  }

  _getBalance = async () => {
    this.setState({
      showbalance: GLOBAL.userBalance,
    });
    setTimeout(() => {
      this.setState({ showbalance: "TAP FOR BALANCE" });
    }, 4000);
  };

  _onFocus = () => {
    BackHandler.addEventListener("hardwareBackPress", this.handleBackPress);
  };

  _onBlurr = () => {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackPress);
  };

  handleBackPress = () => {
    this.props.navigation.closeDrawer();
    Alert.alert(
      "Exit App",
      "Do you want to exit?",
      [
        { text: "No", style: "cancel" },
        {
          text: "Yes",
          onPress: () => {
            BackHandler.exitApp();
            GLOBAL_FUNCTIONS.logOut(this.props);
            this._onBlurr();
          },
        },
      ],
      { cancelable: false }
    );
    return true;
  };

  navLink = (borderWidth, nav, icon, text) => {
    return (
      <TouchableOpacity
        style={[
          styles.item,
          {
            borderRightWidth: borderWidth,
          },
        ]}
        onPress={async () => {
          const connectionStatus = await ConfirmConnection.CheckNetConnection();
          if (connectionStatus) {
            this.props.navigation.navigate(nav);
          }
        }}
      >
        <Ionicons name={icon} size={width * 0.1} color="#4050B5" />
        <View style={{ paddingTop: height * 0.01 }}>
          <Text>{text}</Text>
        </View>
      </TouchableOpacity>
    );
  };

  render() {
    const mobile = GLOBAL.rechargeScreen.mobile;
    const userRoles = GLOBAL.rechargeScreen.userRole;
    return (
      <DefaultScreen
        screenBackgroundColor={"#4050B5"}
        screenData={
          <View>
            <NavigationEvents
              onWillFocus={this._onFocus}
              onWillBlur={this._onBlurr}
            />

            <LoderModal isLoading={this.state.isLoading} />

            <View style={styles.userInfo}>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate("ChangePicture")}
                style={styles.Imagecontainer}
              >
                <Image
                  style={styles.profilePic}
                  source={{ uri: `https://${GLOBAL.userImage}` }}
                />
              </TouchableOpacity>

              <View style={styles.userName}>
                <Text style={{ fontWeight: "bold", fontSize: width * 0.06 }}>
                  {mobile}{" "}
                </Text>
                {this.state.showbalance === "TAP FOR BALANCE" ? (
                  <TouchableOpacity onPress={this._getBalance}>
                    <View style={styles.tapForBalance}>
                      <View style={styles.takaSign}>
                        <Text style={{ color: "#fff" }}>৳</Text>
                      </View>

                      {
                        <View>
                          <Text
                            style={{
                              fontSize: width * 0.032,
                              fontWeight: "bold",
                            }}
                          >
                            {this.state.showbalance}
                          </Text>
                        </View>
                      }
                    </View>
                  </TouchableOpacity>
                ) : (
                  <View
                    style={{
                      alignItems: "flex-start",
                      justifyContent: "center",
                      height: height * 0.045,
                    }}
                  >
                    <View style={styles.balanceStyle}>
                      <View style={styles.takaSign}>
                        <Text style={{ color: "#fff" }}>৳</Text>
                      </View>

                      {
                        <View>
                          <Text style={{ fontSize: width * 0.05 }}>
                            {this.state.showbalance}
                          </Text>
                        </View>
                      }
                    </View>
                  </View>
                )}
              </View>
            </View>
            <ScrollView showsVerticalScrollIndicator={false}>
              <View style={styles.row}>
                {userRoles.map((item, key) => (
                  <TouchableOpacity
                    style={[
                      styles.item,
                      {
                        borderRightWidth: 1,
                      },
                    ]}
                    key={key}
                    onPress={async () => {
                      GLOBAL.service_id = item.id;
                      GLOBAL.imgName = { uri: `https://${item.logo}` };
                      GLOBAL.serviceName = item.name;

                      const connectionStatus = await ConfirmConnection.CheckNetConnection();
                      if (connectionStatus) {
                        this.props.navigation.navigate(item.code);
                      }
                    }}
                  >
                    <Image
                      style={{ width: width * 0.1, height: width * 0.1 }}
                      source={{ uri: `https://${item.logo}` }}
                    />
                    <View style={{ paddingTop: height * 0.01 }}>
                      <Text>{item.name}</Text>
                    </View>
                  </TouchableOpacity>
                ))}

                {this.navLink(1, "Reports", "md-stats", "Reports")}
                {this.navLink(
                  0,
                  "BalanceTransfer",
                  "md-paper-plane",
                  "Transfer"
                )}
                {this.navLink(1, "Settings", "md-settings", "Settings")}
              </View>
            </ScrollView>
          </View>
        }
      />
    );
  }
}

const styles = StyleSheet.create({
  userInfo: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    padding: width * 0.035,
    height: height * 0.12,
    borderBottomWidth: 3,
    borderColor: "#4050B5",
    width: "100%",
  },
  Imagecontainer: {
    height: width * 0.13,
    width: width * 0.13,
    marginEnd: width * 0.035,
  },
  profilePic: {
    flex: 2,
    height: width * 0.13,
    width: width * 0.13,
    borderRadius: 100,
  },
  userName: {
    flex: 6,
    flexDirection: "column",
    justifyContent: "center",
  },
  tapForBalance: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    width: width * 0.41,
    height: height * 0.046,
    backgroundColor: "#D4D6E5",
    borderRadius: 30,
  },
  balanceStyle: {
    flexDirection: "row",
    backgroundColor: "#D4D6E5",
    justifyContent: "center",
    alignItems: "center",
    paddingBottom: width * 0.01,
    paddingTop: width * 0.01,
    paddingRight: width * 0.03,
    paddingLeft: width * 0.03,
    borderRadius: 30,
  },
  takaSign: {
    alignItems: "center",
    justifyContent: "center",
    fontSize: width * 0.014,
    padding: width * 0.014,
    height: width * 0.06,
    width: width * 0.06,
    backgroundColor: "#4050B5",
    borderRadius: 100,
    marginEnd: width * 0.015,
  },
  row: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "flex-start",
    flexWrap: "wrap",
  },
  item: {
    justifyContent: "center",
    alignItems: "center",
    borderBottomWidth: 1,
    borderColor: "#DAE0E2",
    width: "33.33%",
    height: height * 0.16,
  },
});
