import React from "react";
import { View, Dimensions, Platform } from "react-native";
import DefaultScreen from "../../components/DefaultScreen";
import ServiceHeader from "../../components/ServiceHeader";
import ServiceAmount from "../../components/ServiceAmount";
import { Ionicons } from "@expo/vector-icons";
import AvailAbleBalance from "../../components/AvailAbleBalance";
import GLOBAL from "../../globalState";
const { width } = Dimensions.get("window");
const leftData = [
  {
    title: "500",
    value: false,
  },
  {
    title: "1000",
    value: false,
  },
  {
    title: "3000",
    value: false,
  },
  {
    title: "5000",
    value: false,
  },
];

export default class ServiceAmountScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      type: "PERSONAL",
      amount: "",
    };
  }

  static navigationOptions = ({ navigation }) => ({
    headerLeft: (
      <Ionicons
        name={Platform.OS === "ios" ? "ios-arrow-back" : "md-arrow-back"}
        size={Platform.OS === "ios" ? 35 : 24}
        color={"#fff"}
        style={
          Platform.OS === "ios"
            ? { marginBottom: -4, width: 25, marginLeft: 9 }
            : { marginBottom: -4, width: 25, marginLeft: 20 }
        }
        onPress={() => {
          navigation.goBack();
        }}
      />
    ),
    title: GLOBAL.serviceName,
    headerTintColor: "#fff",
    headerStyle: {
      backgroundColor: "#00cec9",
    },
    headerTitleStyle: {
      color: "#fff",
    },
  });

  moveToServicePin = (amount) => {
    this.props.navigation.navigate("ServicePin", {
      amount: this.state.amount,
    });
  };

  render() {
    return (
      <DefaultScreen
        gridFlex={true}
        screenData={
          <View>
            <ServiceHeader
              title={"FOR"}
              imgHeightWidth={{
                width: width * 0.12,
                height: width * 0.12,
              }}
              centerData={GLOBAL.service_mob_no}
              imgName={GLOBAL.imgName}
            />
            <ServiceAmount
              moveToPin={(data) => this.moveToServicePin(data.amount)}
              leftDataFlex={3}
              leftData={leftData}
              selectedLeftData={(amount) => this.setState({ amount })}
              inputValue={(amount) => this.setState({ amount })}
            />
            <AvailAbleBalance />
          </View>
        }
      />
    );
  }
}
