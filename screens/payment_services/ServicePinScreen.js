import React from "react";
import { View, Dimensions, Platform } from "react-native";
import { Ionicons } from "@expo/vector-icons";
import DefaultScreen from "../../components/DefaultScreen";
import AppInput from "../../components/AppInput";
import ServiceHeader from "../../components/ServiceHeader";
import ServiceAmountChargePrev from "../../components/ServiceAmountChargePrev";
import ServiceType from "../../components/ServiceType";
import ConfirmModal from "../../components/ConfirmModal";
import SuccessModal from "../../components/SuccessModal";
import axios from "axios";
import ConfirmConnection from "../../ConfirmNetConnection";
import GLOBAL from "../../globalState";
import GLOBAL_FUNCTION from "../../globalFunctions";
const { width } = Dimensions.get("window");
export default class ServicePinScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      type: "PERSONAL",
      amount: this.props.navigation.getParam("amount", "0"),
      pin: "",
      trxid: "",
      datetime: "",
      phone_no: GLOBAL.service_mob_no,
      isLoading: false,
      isConfirmModalVisible: false,
      isSuccessModalVisible: false,
    };
  }

  static navigationOptions = ({ navigation }) => ({
    headerLeft: (
      <Ionicons
        name={Platform.OS === "ios" ? "ios-arrow-back" : "md-arrow-back"}
        size={Platform.OS === "ios" ? 35 : 24}
        color={"#fff"}
        style={
          Platform.OS === "ios"
            ? { marginBottom: -4, width: 25, marginLeft: 9 }
            : { marginBottom: -4, width: 25, marginLeft: 20 }
        }
        onPress={() => {
          navigation.goBack();
        }}
      />
    ),
    title: GLOBAL.serviceName,
    headerTintColor: "#fff",
    headerStyle: {
      backgroundColor: "#00cec9",
    },
    headerTitleStyle: {
      color: "#fff",
    },
  });

  componentDidMount() {}
  toggleModal = () => {
    this.setState({ isConfirmModalVisible: !this.state.isConfirmModalVisible });
  };
  toggleSuccessModal = () => {
    this.setState({ isSuccessModalVisible: !this.state.isSuccessModalVisible });
  };

  sendMoney = async () => {
    const connectionStatus = await ConfirmConnection.CheckNetConnection();
    if (connectionStatus) {
      this.setState({ isLoading: !this.state.isLoading });

      var allData = {
        apps_key: GLOBAL.rechargeScreen.apps_key,
        token: GLOBAL.rechargeScreen.token,
        phone_no: this.state.phone_no,
        amount: this.state.amount,
        type: this.state.type,
        operator: null,
        pin: this.state.pin,
        service_id: GLOBAL.service_id,
      };

      axios
        .post(`https://${GLOBAL.appUrl}/api/requestSubmit`, allData)
        .then((res) => {
          if (res.data.status == 1) {
            this.toggleModal();
            GLOBAL.userBalance = res.data.balance;
            this.setState({
              isLoading: !this.state.isLoading,
              trxid: res.data.trxid,
              datetime: res.data.datetime,
            });
            this.toggleSuccessModal();
          } else if (res.data.status == 50) {
            GLOBAL_FUNCTION.logOut(this.props);
          } else if (res.data.status == 40) {
            this.setState({ isLoading: !this.state.isLoading });
            alert(res.data.message);
          }
        });
    }
  };

  render() {
    return (
      <DefaultScreen
        gridFlex={true}
        screenData={
          <View>
            <ConfirmModal
              toggleModal={this.toggleModal}
              confirmAction={this.sendMoney}
              isModalVisible={this.state.isConfirmModalVisible}
              confirmTitle={GLOBAL.serviceName}
              stateData={this.state}
              data={[
                {
                  text: "TOTAL",
                  value: this.state.amount,
                  rightBorder: 1,
                  bottomBorder: 0,
                },
                {
                  text: "CHARGE",
                  value: 5.9,
                  rightBorder: 0,
                  bottomBorder: 0,
                },
                {
                  text: "TYPE",
                  value: this.state.type,
                  rightBorder: 1,
                  bottomBorder: 1,
                },
                {
                  text: "NEW BALANCE",
                  value: Number(GLOBAL.userBalance) - Number(this.state.amount),
                  rightBorder: 0,
                  bottomBorder: 1,
                },
              ]}
            />

            <SuccessModal
              toggleModal={() => {
                this.toggleSuccessModal();
                GLOBAL_FUNCTION.backToHome(this.props);
              }}
              confirmTitle={GLOBAL.serviceName}
              isSuccessModalVisible={this.state.isSuccessModalVisible}
              stateData={this.state}
              data={[
                {
                  text: "TRANS ID",
                  value: this.state.trxid,
                  rightBorder: 1,
                  bottomBorder: 0,
                },
                {
                  text: "REQUEST TIME",
                  value: this.state.datetime,
                  rightBorder: 0,
                  bottomBorder: 0,
                },
                {
                  text: "TOTAL",
                  value: this.state.amount,
                  rightBorder: 1,
                  bottomBorder: 0,
                },
                {
                  text: "CHARGE",
                  value: 5.9,
                  rightBorder: 0,
                  bottomBorder: 0,
                },
                {
                  text: "TYPE",
                  value: this.state.type,
                  rightBorder: 1,
                  bottomBorder: 1,
                },
                {
                  text: "NEW BALANCE",
                  value: Number(GLOBAL.userBalance) - Number(this.state.amount),
                  rightBorder: 0,
                  bottomBorder: 1,
                },
              ]}
            />

            <ServiceHeader
              title={"FOR"}
              imgHeightWidth={{
                width: width * 0.12,
                height: width * 0.12,
              }}
              centerData={GLOBAL.service_mob_no}
              imgName={GLOBAL.imgName}
            />

            <View
              style={{ borderBottomWidth: 1, borderBottomColor: "#ecf0f1" }}
            ></View>
            <ServiceAmountChargePrev
              amount={this.state.amount}
              charge={"0.00"}
              total={this.state.amount}
            />

            <ServiceType
              typeOne={"PERSONAL"}
              typeTwo={"AGENT"}
              defaultType={this.state.type}
              selectedType={(type) => this.setState({ type: type })}
            />

            <AppInput
              placeHolder={"Enter PIN"}
              placeholderTextColor={"#bdc3c7"}
              keyboardType={"decimal-pad"}
              leftIcon={"lock"}
              leftIconColor={"#00CDC8"}
              rightIconColor={"#00CDC8"}
              autoFocus={true}
              inputValue={(inputValue) => this.setState({ pin: inputValue })}
              next={this.toggleModal}
              secureTextEntry={true}
            />
          </View>
        }
      />
    );
  }
}
