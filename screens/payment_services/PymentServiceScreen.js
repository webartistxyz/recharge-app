import React from "react";
import { Dimensions, Image, Platform } from "react-native";
import { Ionicons } from "@expo/vector-icons";
import GetMobileNumbers from "../../components/GetMobileNumbers";
import GLOBAL from "../../globalState";
const { width } = Dimensions.get("window");

export default class PymentServiceScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      phone_no: "",
      isValidNo: false,
    };
  }

  static navigationOptions = ({ navigation }) => ({
    headerLeft: (
      <Ionicons
        name={Platform.OS === "ios" ? "ios-arrow-back" : "md-arrow-back"}
        size={Platform.OS === "ios" ? 35 : 24}
        color={"#fff"}
        style={
          Platform.OS === "ios"
            ? { marginBottom: -4, width: 25, marginLeft: 9 }
            : { marginBottom: -4, width: 25, marginLeft: 20 }
        }
        onPress={() => {
          navigation.goBack();
        }}
      />
    ),
    title: GLOBAL.serviceName,
    headerRight: (
      <Image
        style={{
          width: width * 0.07,
          height: width * 0.07,
          borderRadius: 5,
          marginRight: width * 0.07,
        }}
        source={GLOBAL.imgName}
      />
    ),
    headerTintColor: "#fff",
    headerStyle: {
      backgroundColor: "#BA2F16",
    },
    headerTitleStyle: {
      color: "#fff",
    },
  });

  componentDidMount() {}

  next(phone_no) {
    this.props.navigation.navigate("ServiceAmount");
    GLOBAL.service_mob_no = phone_no;
  }
  validatePhoneNo(phone_no) {
    if (phone_no.length > 4) {
      this.setState({ isValidNo: true });
    } else {
      this.setState({ isValidNo: false });
    }
  }

  render() {
    return (
      <GetMobileNumbers
        setPhoneNO={(phone_no) => {
          this.setState({ phone_no });
          this.validatePhoneNo(phone_no);
        }}
        isValidNo={this.state.isValidNo}
        moveTo={() => {
          this.next(this.state.phone_no);
        }}
        mobileNOAndPrefix={(mobile_no, prefix) => {
          this.next(mobile_no);
        }}
      />
    );
  }
}
