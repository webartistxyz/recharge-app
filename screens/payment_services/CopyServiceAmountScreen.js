import React from "react";
import { StyleSheet, Text, View, Dimensions, Platform } from "react-native";
import DefaultScreen from "../../components/DefaultScreen";
import LoderModal from "../../components/LoaderModal";
import AppInput from "../../components/AppInput";
import ServiceHeader from "../../components/ServiceHeader";
import ServiceAmount from "../../components/ServiceAmount";
import ConfirmModal from "../../components/ConfirmModal";
import SuccessModal from "../../components/SuccessModal";
import { Container, Tab, Tabs } from "native-base";
import { Ionicons } from "@expo/vector-icons";
import axios from "axios";
import ConfirmConnection from "../../ConfirmNetConnection";
import GLOBAL from "../../globalState";
import GLOBAL_FUNCTION from "../../globalFunctions";
const { width } = Dimensions.get("window");
const leftData = [
  {
    title: "PERSONAL",
    value: true,
  },
  {
    title: "AGENT",
    value: false,
  },
];

export default class CopyServiceAmountScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      type: "PERSONAL",
      amount: "",
      pin: "",
      phone_no: GLOBAL.service_mob_no,
      isLoading: false,
      isConfirmModalVisible: false,
      isSuccessModalVisible: false,
    };
  }

  static navigationOptions = ({ navigation }) => ({
    headerLeft: (
      <Ionicons
        name={Platform.OS === "ios" ? "ios-arrow-back" : "md-arrow-back"}
        size={Platform.OS === "ios" ? 35 : 24}
        color={"#fff"}
        style={
          Platform.OS === "ios"
            ? { marginBottom: -4, width: 25, marginLeft: 9 }
            : { marginBottom: -4, width: 25, marginLeft: 20 }
        }
        onPress={() => {
          navigation.goBack();
        }}
      />
    ),
    title: GLOBAL.serviceName,
    headerTintColor: "#fff",
    headerStyle: {
      backgroundColor: "#00cec9",
    },
    headerTitleStyle: {
      color: "#fff",
    },
  });

  componentDidMount() {}
  toggleModal = () => {
    this.setState({ isConfirmModalVisible: !this.state.isConfirmModalVisible });
  };
  toggleSuccessModal = () => {
    this.setState({ isSuccessModalVisible: !this.state.isSuccessModalVisible });
  };

  sendMoney = () => {
    this.setState({ isLoading: !this.state.isLoading });
    setTimeout(() => {
      this.setState({ isLoading: !this.state.isLoading });
      this.toggleSuccessModal();
    }, 2000);
  };

  render() {
    return (
      <DefaultScreen
        gridFlex={true}
        screenData={
          <View>
            <ConfirmModal
              toggleModal={this.toggleModal}
              confirmAction={this.sendMoney}
              isModalVisible={this.state.isConfirmModalVisible}
              confirmTitle={"Bkash Account"}
              stateData={this.state}
              data={[
                {
                  text: "TOTAL",
                  value: this.state.amount,
                  rightBorder: 1,
                  bottomBorder: 0,
                },
                {
                  text: "CHARGE",
                  value: 5.9,
                  rightBorder: 0,
                  bottomBorder: 0,
                },
                {
                  text: "TYPE",
                  value: this.state.type,
                  rightBorder: 1,
                  bottomBorder: 1,
                },
                {
                  text: "NEW BALANCE",
                  value: Number(GLOBAL.userBalance) - Number(this.state.amount),
                  rightBorder: 0,
                  bottomBorder: 1,
                },
              ]}
            />

            <SuccessModal
              toggleModal={() => {
                this.toggleSuccessModal();
                GLOBAL_FUNCTION.backToHome(this.props);
              }}
              confirmTitle={"Recharge"}
              isSuccessModalVisible={this.state.isSuccessModalVisible}
              stateData={this.state}
              data={[
                {
                  text: "TRANS ID",
                  value: this.state.trxid,
                  rightBorder: 1,
                  bottomBorder: 0,
                },
                {
                  text: "REQUEST TIME",
                  value: this.state.datetime,
                  rightBorder: 0,
                  bottomBorder: 0,
                },
                {
                  text: "TOTAL",
                  value: this.state.amount,
                  rightBorder: 1,
                  bottomBorder: 0,
                },
                {
                  text: "CHARGE",
                  value: 5.9,
                  rightBorder: 0,
                  bottomBorder: 0,
                },
                {
                  text: "TYPE",
                  value: this.state.type,
                  rightBorder: 1,
                  bottomBorder: 1,
                },
                {
                  text: "NEW BALANCE",
                  value: Number(GLOBAL.userBalance) - Number(this.state.amount),
                  rightBorder: 0,
                  bottomBorder: 1,
                },
              ]}
            />

            <ServiceHeader
              title={"FOR"}
              imgHeightWidth={{
                width: width * 0.12,
                height: width * 0.12,
              }}
              centerData={GLOBAL.service_mob_no}
              imgName={GLOBAL.imgName}
            />
            <ServiceAmount
              moveToPin={(data) => this._moveToRechargePin(data.amount)}
              notChangeInputValue={true}
              leftData={leftData}
              leftDataFlex={3}
              hideRightArrow={true}
              selectedLeftData={(data) => this.setState({ type: data })}
              inputValue={(amount) => this.setState({ amount })}
              leftDataFontSize={{ fontSize: width * 0.028 }}
            />
            <View
              style={{ borderBottomWidth: 1, borderBottomColor: "#ecf0f1" }}
            ></View>
            <AppInput
              placeHolder={"Enter PIN"}
              placeholderTextColor={"#bdc3c7"}
              keyboardType={"decimal-pad"}
              leftIcon={"lock"}
              leftIconColor={"#00CDC8"}
              rightIconColor={"#00CDC8"}
              autoFocus={false}
              inputValue={(inputValue) => this.setState({ pin: inputValue })}
              next={this.toggleModal}
              hideRightArrow={
                this.state.amount && this.state.type ? false : true
              }
              secureTextEntry={true}
            />
            <View
              style={{
                alignItems: "center",
                justifyContent: "center",
                paddingBottom: width * 0.1,
                paddingTop: width * 0.04,
              }}
            >
              <Text style={{ fontSize: width * 0.05, color: "#00cec9" }}>
                Available Balance ৳{GLOBAL.userBalance}
              </Text>
            </View>
          </View>
        }
      />
    );
  }
}
