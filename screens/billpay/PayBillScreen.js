import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  TouchableOpacity,
  TextInput,
  ScrollView,
  Platform,
} from "react-native";
import AppModal from "../../components/Modal";
import ServiceHeader from "../../components/ServiceHeader";
import DefaultScreen from "../../components/DefaultScreen";
import { Entypo } from "@expo/vector-icons";
import { Ionicons } from "@expo/vector-icons";
const { width } = Dimensions.get("window");
const allMonths = new Array(
  "Jan",
  "Feb",
  "Mar",
  "Apr",
  "May",
  "Jun",
  "Jul",
  "Aug",
  "Sept",
  "Oct",
  "Oct",
  "Oct"
);
const d = new Date();
const date = new Date();
let months = [];

export default class PayBillScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      bill_acc: "",
      selected_month: allMonths[d.getMonth()] + " " + d.getFullYear(),
      isModalVisible: false,
    };
  }

  static navigationOptions = ({ navigation }) => ({
    headerLeft: (
      <Ionicons
        name={Platform.OS === "ios" ? "ios-arrow-back" : "md-arrow-back"}
        size={Platform.OS === "ios" ? 35 : 24}
        color={"#fff"}
        style={
          Platform.OS === "ios"
            ? { marginBottom: -4, width: 25, marginLeft: 9 }
            : { marginBottom: -4, width: 25, marginLeft: 20 }
        }
        onPress={() => {
          navigation.goBack();
        }}
      />
    ),
    title: "Pay Bill",
    headerTintColor: "#fff",
    headerStyle: {
      backgroundColor: "#00cec9",
    },
    headerTitleStyle: {
      color: "#fff",
    },
  });

  componentDidMount() {
    this.getLast12Months();
    d.setFullYear(date.getFullYear());
    d.setMonth(date.getMonth());
  }

  getLast12Months = () => {
    months = [];
    for (i = 0; i <= 11; i++) {
      months.unshift(allMonths[d.getMonth()] + " " + d.getFullYear());
      d.setMonth(d.getMonth() - 1);
    }
  };
  _modalContent = () => {
    return (
      <ScrollView
        keyboardShouldPersistTaps="always"
        showsVerticalScrollIndicator={false}
        style={{ paddingLeft: width * 0.05 }}
      >
        {months.map((month, index) => (
          <TouchableOpacity
            style={{ padding: width * 0.039 }}
            key={index}
            onPress={() =>
              this.setState({ selected_month: month, isModalVisible: false })
            }
          >
            <Text style={{ fontSize: width * 0.04 }}>{month}</Text>
          </TouchableOpacity>
        ))}
      </ScrollView>
    );
  };

  moveToPinScreen = () => {
    this.props.navigation.navigate("BillPin", {
      imgHeightWidth: this.props.navigation.getParam("imgHeightWidth"),
      imgName: this.props.navigation.getParam("imgName"),
      billName: this.props.navigation.getParam("billName"),
      selected_month: this.state.selected_month,
      bill_acc: this.state.bill_acc,
    });
  };

  render() {
    return (
      <DefaultScreen
        gridFlex={true}
        screenData={
          <View>
            <AppModal
              toggleModal={() =>
                this.setState({ isModalVisible: !this.state.isModalVisible })
              }
              isModalVisible={this.state.isModalVisible}
              modalContent={this._modalContent()}
            />

            <ServiceHeader
              imgHeightWidth={this.props.navigation.getParam("imgHeightWidth")}
              imgName={this.props.navigation.getParam("imgName")}
              centerData={this.props.navigation.getParam("billName")}
            />

            <View style={{ borderBottomWidth: 1, borderColor: "#DAE0E2" }}>
              <View style={{ padding: width * 0.03 }}>
                <Text style={{ fontSize: width * 0.03, color: "#00cec9" }}>
                  BILL PERIOD
                </Text>
                <TouchableOpacity
                  onPress={() =>
                    this.setState({
                      isModalVisible: !this.state.isModalVisible,
                    })
                  }
                  style={{ alignItems: "flex-start", justifyContent: "center" }}
                >
                  <View style={styles.monthButton}>
                    <Ionicons
                      style={styles.arrowSign}
                      name="md-arrow-dropdown"
                      size={width * 0.05}
                      color="#00CEC9"
                    />
                    <Text
                      style={{
                        fontSize: width * 0.03,
                        color: "#00CEC9",
                        paddingRight: width * 0.014,
                      }}
                    >
                      {" "}
                      {this.state.selected_month}
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>

            <View style={{ padding: width * 0.03 }}>
              <Text style={{ fontSize: width * 0.03, color: "#00cec9" }}>
                ENTER ACCOUNT NUMBER
              </Text>
              <View style={styles.inputContainer}>
                <TextInput
                  style={{ flex: 9 }}
                  autoCapitalize="none"
                  placeholderTextColor={"#bdc3c7"}
                  onChangeText={(bill_acc) => {
                    this.setState({ bill_acc });
                  }}
                  value={this.state.bill_acc}
                  placeholder="ENTER ACCOUNT NUMBER"
                  keyboardType="default"
                  autoCorrect={false}
                />
                <View style={{ flex: 1 }}>
                  {this.state.bill_acc.length > 3 ? (
                    <TouchableOpacity
                      style={styles.nextButton}
                      onPress={this.moveToPinScreen}
                    >
                      <Entypo
                        name="arrow-right"
                        size={width * 0.09}
                        color="#00CDC8"
                      />
                    </TouchableOpacity>
                  ) : (
                    <View style={styles.nextButton}>
                      <Entypo
                        name="arrow-right"
                        size={width * 0.09}
                        color="#bdc3c7"
                      />
                    </View>
                  )}
                </View>
              </View>
            </View>
          </View>
        }
      />
    );
  }
}

const styles = StyleSheet.create({
  monthButton: {
    marginTop: width * 0.04,
    flexDirection: "row",
    backgroundColor: "#EEF1F1",
    justifyContent: "center",
    alignItems: "center",
    padding: width * 0.03,
    borderRadius: 30,
  },
  arrowSign: {
    paddingLeft: width * 0.014,
  },
  inputContainer: {
    flexDirection: "row",
  },
  nextButton: {
    alignItems: "center",
    justifyContent: "center",
  },
});
