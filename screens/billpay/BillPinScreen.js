import React from "react";
import { View, Platform } from "react-native";
import DefaultScreen from "../../components/DefaultScreen";
import AppInput from "../../components/AppInput";
import ServiceHeader from "../../components/ServiceHeader";
import ServiceAmountChargePrev from "../../components/ServiceAmountChargePrev";
import ConfirmModal from "../../components/ConfirmModal";
import SuccessModal from "../../components/SuccessModal";
import AvailAbleBalance from "../../components/AvailAbleBalance";
import { Ionicons } from "@expo/vector-icons";
import ConfirmConnection from "../../ConfirmNetConnection";
import GLOBAL from "../../globalState";
import GLOBAL_FUNCTION from "../../globalFunctions";
export default class BillPinScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      type: "PERSONAL",
      amount: this.props.navigation.getParam("amount", "3000"),
      selected_month: this.props.navigation.getParam("selected_month"),
      bill_acc: this.props.navigation.getParam("bill_acc"),
      pin: "",
      isLoading: false,
      isConfirmModalVisible: false,
      isSuccessModalVisible: false,
    };
  }

  static navigationOptions = ({ navigation }) => ({
    headerLeft: (
      <Ionicons
        name={Platform.OS === "ios" ? "ios-arrow-back" : "md-arrow-back"}
        size={Platform.OS === "ios" ? 35 : 24}
        color={"#fff"}
        style={
          Platform.OS === "ios"
            ? { marginBottom: -4, width: 25, marginLeft: 9 }
            : { marginBottom: -4, width: 25, marginLeft: 20 }
        }
        onPress={() => {
          navigation.goBack();
        }}
      />
    ),
    title: GLOBAL.serviceName,
    headerTintColor: "#fff",
    headerStyle: {
      backgroundColor: "#00cec9",
    },
    headerTitleStyle: {
      color: "#fff",
    },
  });

  componentDidMount() {}
  toggleModal = () => {
    this.setState({ isConfirmModalVisible: !this.state.isConfirmModalVisible });
  };
  toggleSuccessModal = () => {
    this.setState({ isSuccessModalVisible: !this.state.isSuccessModalVisible });
  };

  payBill = () => {
    this.setState({ isLoading: !this.state.isLoading });
    setTimeout(() => {
      this.setState({ isLoading: !this.state.isLoading });
      this.toggleSuccessModal();
    }, 2000);
  };

  render() {
    return (
      <DefaultScreen
        gridFlex={true}
        screenData={
          <View>
            <ConfirmModal
              toggleModal={this.toggleModal}
              confirmAction={this.payBill}
              isModalVisible={this.state.isConfirmModalVisible}
              confirmTitle={"ACCOUNT NUMBER"}
              accountNo={this.state.bill_acc}
              stateData={this.state}
              data={[
                {
                  text: "TOTAL",
                  value: this.state.amount,
                  rightBorder: 1,
                  bottomBorder: 0,
                },
                {
                  text: "CHARGE",
                  value: 5.9,
                  rightBorder: 0,
                  bottomBorder: 0,
                },
                {
                  text: "MONTH",
                  value: this.state.selected_month,
                  rightBorder: 1,
                  bottomBorder: 1,
                },
                {
                  text: "NEW BALANCE",
                  value: Number(GLOBAL.userBalance) - Number(this.state.amount),
                  rightBorder: 0,
                  bottomBorder: 1,
                },
              ]}
            />

            <SuccessModal
              toggleModal={() => {
                this.toggleSuccessModal();
                GLOBAL_FUNCTION.backToHome(this.props);
              }}
              confirmTitle={"Recharge"}
              isSuccessModalVisible={this.state.isSuccessModalVisible}
              stateData={this.state}
              data={[
                {
                  text: "TRANS ID",
                  value: this.state.trxid,
                  rightBorder: 1,
                  bottomBorder: 0,
                },
                {
                  text: "REQUEST TIME",
                  value: this.state.datetime,
                  rightBorder: 0,
                  bottomBorder: 0,
                },
                {
                  text: "TOTAL",
                  value: this.state.amount,
                  rightBorder: 1,
                  bottomBorder: 0,
                },
                {
                  text: "CHARGE",
                  value: 5.9,
                  rightBorder: 0,
                  bottomBorder: 0,
                },
                {
                  text: "MONTH",
                  value: this.state.selected_month,
                  rightBorder: 1,
                  bottomBorder: 1,
                },
                {
                  text: "NEW BALANCE",
                  value: Number(GLOBAL.userBalance) - Number(this.state.amount),
                  rightBorder: 0,
                  bottomBorder: 1,
                },
              ]}
            />

            <ServiceHeader
              title={"FOR"}
              imgHeightWidth={this.props.navigation.getParam("imgHeightWidth")}
              centerData={this.state.bill_acc}
              imgName={this.props.navigation.getParam("imgName")}
            />

            <View
              style={{ borderBottomWidth: 1, borderBottomColor: "#ecf0f1" }}
            ></View>
            <ServiceAmountChargePrev
              month={this.state.selected_month}
              amount={this.state.amount}
              charge={"0.00"}
              total={this.state.amount}
            />

            <AppInput
              placeHolder={"Enter PIN"}
              placeholderTextColor={"#bdc3c7"}
              keyboardType={"decimal-pad"}
              leftIcon={"lock"}
              leftIconColor={"#00CDC8"}
              rightIconColor={"#00CDC8"}
              autoFocus={true}
              inputValue={(inputValue) => this.setState({ pin: inputValue })}
              next={this.toggleModal}
              secureTextEntry={true}
            />
            <AvailAbleBalance />
          </View>
        }
      />
    );
  }
}
