import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  Image,
  TouchableHighlight,
  Platform,
} from "react-native";
import DefaultScreen from "../../components/DefaultScreen";
import { Ionicons } from "@expo/vector-icons";
import { Container, Tab, Tabs } from "native-base";
const { width } = Dimensions.get("window");

export default class AllBillsScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  static navigationOptions = ({ navigation }) => ({
    headerLeft: (
      <Ionicons
        name={Platform.OS === "ios" ? "ios-arrow-back" : "md-arrow-back"}
        size={Platform.OS === "ios" ? 35 : 24}
        color={"#fff"}
        style={
          Platform.OS === "ios"
            ? { marginBottom: -4, width: 25, marginLeft: 9 }
            : { marginBottom: -4, width: 25, marginLeft: 20 }
        }
        onPress={() => {
          navigation.goBack();
        }}
      />
    ),
    title: "Pay Bill",
    headerTintColor: "#fff",
    headerStyle: {
      backgroundColor: "#00cec9",
    },
    headerTitleStyle: {
      color: "#fff",
    },
  });

  componentDidMount() {}

  billContent(imgName, imgHeightWidth, billName, dsc) {
    return (
      <TouchableHighlight
        underlayColor="#ecf0f1"
        onPress={() =>
          this.props.navigation.navigate("PayBill", {
            imgName,
            imgHeightWidth,
            billName,
            dsc,
          })
        }
        style={{
          width: width * 0.93,
          borderBottomWidth: 1,
          borderColor: "#DAE0E2",
        }}
      >
        <View style={{ flexDirection: "row", padding: width * 0.03 }}>
          <View
            style={{
              flex: 2,
              paddingEnd: width * 0.032,
              justifyContent: "center",
            }}
          >
            <View style={[styles.imgageShadow, imgHeightWidth]}>
              <Image style={imgHeightWidth} source={imgName} />
            </View>
          </View>
          <View style={{ flexDirection: "column", flex: 12 }}>
            <Text style={{ fontSize: width * 0.05 }}>{billName}</Text>
            <Text style={{ fontSize: width * 0.036 }}>{dsc}</Text>
          </View>
        </View>
      </TouchableHighlight>
    );
  }

  render() {
    return (
      <DefaultScreen
        screenData={
          <Container>
            <Tabs
              tabBarUnderlineStyle={{ backgroundColor: "#00cec9", height: 3 }}
            >
              <Tab
                textStyle={{ color: "#636e72" }}
                tabStyle={{ backgroundColor: "#d2dae2" }}
                activeTabStyle={{ backgroundColor: "#fff" }}
                activeTextStyle={{ color: "#1e272e" }}
                heading="Electricity"
              >
                {this.billContent(
                  require("../../assets/palli_bidyut.png"),
                  { width: width * 0.08, height: width * 0.11 },
                  "Palli Bidyut",
                  "NA"
                )}
                {this.billContent(
                  require("../../assets/dpdc.png"),
                  { width: width * 0.09, height: width * 0.09 },
                  "DPDC",
                  "NA"
                )}
                {this.billContent(
                  require("../../assets/desco.png"),
                  { width: width * 0.1, height: width * 0.01 },
                  "DESCO",
                  "NA"
                )}
              </Tab>
              <Tab
                textStyle={{ color: "#636e72" }}
                tabStyle={{ backgroundColor: "#d2dae2" }}
                activeTabStyle={{ backgroundColor: "#fff" }}
                activeTextStyle={{ color: "#1e272e" }}
                heading="Gas"
              >
                {this.billContent(
                  require("../../assets/titas.png"),
                  { width: width * 0.09, height: width * 0.09 },
                  "Titas Gas",
                  "NA"
                )}
                {this.billContent(
                  require("../../assets/jalalabadgas.png"),
                  { width: width * 0.09, height: width * 0.09 },
                  "Jalalabad Gas",
                  "NA"
                )}
              </Tab>
            </Tabs>
          </Container>
        }
      />
    );
  }
}

const styles = StyleSheet.create({
  imgageShadow: {
    borderRadius: 100,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 4 },
    shadowOpacity: 0.3,
    shadowRadius: 2.65,
    elevation: 8,
  },
});
