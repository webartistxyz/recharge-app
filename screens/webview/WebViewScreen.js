import React from "react";
import { StyleSheet, View, WebView, Platform } from "react-native";
import { Ionicons } from "@expo/vector-icons";
import LoderModal from "../../components/LoaderModal";

export default class WebViewScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
    };
  }

  static navigationOptions = ({ navigation }) => ({
    headerLeft: (
      <Ionicons
        name={Platform.OS === "ios" ? "ios-arrow-back" : "md-arrow-back"}
        size={Platform.OS === "ios" ? 35 : 24}
        color={"#fff"}
        style={
          Platform.OS === "ios"
            ? { marginBottom: -4, width: 25, marginLeft: 9 }
            : { marginBottom: -4, width: 25, marginLeft: 20 }
        }
        onPress={() => {
          navigation.goBack();
        }}
      />
    ),
    title: "Sign Up",
    headerTintColor: "#fff",
    headerStyle: {
      backgroundColor: "#4050B5",
    },
    headerTitleStyle: {
      color: "#fff",
    },
  });
  componentDidMount() {
    this.setState({ isLoading: !this.state.isLoading });
    setTimeout(() => {
      this.setState({ isLoading: !this.state.isLoading });
    }, 2500);
  }

  render() {
    return (
      <View style={styles.container}>
        <LoderModal isLoading={this.state.isLoading} />
        <WebView
          source={{ uri: this.props.navigation.getParam("webViewUrL", "") }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#4050B5",
  },
});
