import React from "react";
import { AsyncStorage, Alert } from "react-native";
import axios from "axios";
import GLOBAL from "../globalState";
import GLOBAL_FUNCTIONS from "../globalFunctions";
import ConfirmConnection from "../ConfirmNetConnection";

class ReactFunctions extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false
    };
  }

  async forgotPinOrPass() {
    const connectionStatus = await ConfirmConnection.CheckNetConnection();
    if (connectionStatus) {
      return new Promise((resolve, reject) => {
        axios
          .post(`https://${GLOBAL.appUrl}/api/forgot_all`, {
            member_id: GLOBAL.userId
          })
          .then(res => {
            resolve(true);
          })
          .catch(error => {
            reject(error);
          });
      });
    }
  }
  logOut(props) {
    AsyncStorage.removeItem("user_data");
    GLOBAL_FUNCTIONS.logOut(props);
  }
}

export default reactFunctions = new ReactFunctions();
