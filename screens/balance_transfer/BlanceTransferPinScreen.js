import React from "react";
import { View, Dimensions, TextInput, Platform } from "react-native";
import DefaultScreen from "../../components/DefaultScreen";
import AppInput from "../../components/AppInput";
import ServiceHeader from "../../components/ServiceHeader";
import ServiceAmountChargePrev from "../../components/ServiceAmountChargePrev";
import ServiceType from "../../components/ServiceType";
import ConfirmModal from "../../components/ConfirmModal";
import SuccessModal from "../../components/SuccessModal";
import axios from "axios";
import ConfirmConnection from "../../ConfirmNetConnection";
import GLOBAL from "../../globalState";
import { Ionicons } from "@expo/vector-icons";
import GLOBAL_FUNCTION from "../../globalFunctions";
const { width } = Dimensions.get("window");
export default class BlanceTransferPinScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      type: "PERSONAL",
      amount: this.props.navigation.getParam("amount", "0"),
      note: "",
      pin: "",
      trxid: "",
      datetime: "",
      transfer_to: GLOBAL.service_mob_no,
      isLoading: false,
      isConfirmModalVisible: false,
      isSuccessModalVisible: false,
    };
  }

  static navigationOptions = ({ navigation }) => ({
    headerLeft: (
      <Ionicons
        name={Platform.OS === "ios" ? "ios-arrow-back" : "md-arrow-back"}
        size={Platform.OS === "ios" ? 35 : 24}
        color={"#fff"}
        style={
          Platform.OS === "ios"
            ? { marginBottom: -4, width: 25, marginLeft: 9 }
            : { marginBottom: -4, width: 25, marginLeft: 20 }
        }
        onPress={() => {
          navigation.goBack();
        }}
      />
    ),
    title: GLOBAL.serviceName,
    headerTintColor: "#fff",
    headerStyle: {
      backgroundColor: "#00cec9",
    },
    headerTitleStyle: {
      color: "#fff",
    },
  });

  componentDidMount() {}
  toggleModal = () => {
    this.setState({ isConfirmModalVisible: !this.state.isConfirmModalVisible });
  };
  toggleSuccessModal = () => {
    this.setState({ isSuccessModalVisible: !this.state.isSuccessModalVisible });
  };

  borderBottom() {
    return (
      <View
        style={{ borderBottomWidth: 1, borderBottomColor: "#ecf0f1" }}
      ></View>
    );
  }
  transferBalance = async () => {
    const connectionStatus = await ConfirmConnection.CheckNetConnection();
    if (connectionStatus) {
      this.setState({ isLoading: !this.state.isLoading });

      var allData = {
        apps_key: GLOBAL.rechargeScreen.apps_key,
        token: GLOBAL.rechargeScreen.token,
        transfer_to: this.state.transfer_to,
        amount: this.state.amount,
        note: this.state.note,
        pin_code: this.state.pin,
      };

      axios
        .post(`https://${GLOBAL.appUrl}/api/balanceTransfer`, allData)
        .then((res) => {
          //console.log(res.data);
          if (res.data.status == 10) {
            this.toggleModal();
            GLOBAL.userBalance = res.data.balance;
            this.setState({
              isLoading: !this.state.isLoading,
              trxid: res.data.trxid,
              datetime: res.data.datetime,
            });
            this.toggleSuccessModal();
          } else if (res.data.status == 50) {
            GLOBAL_FUNCTION.logOut(this.props);
          } else {
            this.setState({ isLoading: !this.state.isLoading });
            alert(res.data.message);
          }
        });
    }
  };

  render() {
    return (
      <DefaultScreen
        gridFlex={true}
        screenData={
          <View>
            <ConfirmModal
              toggleModal={this.toggleModal}
              confirmAction={this.transferBalance}
              isModalVisible={this.state.isConfirmModalVisible}
              confirmTitle={GLOBAL.serviceName}
              accountNo={GLOBAL.service_mob_no}
              stateData={this.state}
              data={[
                {
                  text: "TOTAL",
                  value: this.state.amount,
                  rightBorder: 1,
                  bottomBorder: 0,
                },
                {
                  text: "CHARGE",
                  value: 0.0,
                  rightBorder: 0,
                  bottomBorder: 0,
                },
                {
                  text: "USER",
                  value: this.props.navigation.getParam(
                    "centerDataName",
                    "N/A"
                  ),
                  rightBorder: 1,
                  bottomBorder: 1,
                },
                {
                  text: "NEW BALANCE",
                  value: Number(GLOBAL.userBalance) - Number(this.state.amount),
                  rightBorder: 0,
                  bottomBorder: 1,
                },
              ]}
            />

            <SuccessModal
              toggleModal={() => {
                this.toggleSuccessModal();
                GLOBAL_FUNCTION.backToHome(this.props);
              }}
              confirmTitle={"Transfer"}
              isSuccessModalVisible={this.state.isSuccessModalVisible}
              accountNo={GLOBAL.service_mob_no}
              stateData={this.state}
              data={[
                {
                  text: "TRANS ID",
                  value: this.state.trxid,
                  rightBorder: 1,
                  bottomBorder: 0,
                },
                {
                  text: "REQUEST TIME",
                  value: this.state.datetime,
                  rightBorder: 0,
                  bottomBorder: 0,
                },
                {
                  text: "TOTAL",
                  value: this.state.amount,
                  rightBorder: 1,
                  bottomBorder: 0,
                },
                {
                  text: "CHARGE",
                  value: 0.0,
                  rightBorder: 0,
                  bottomBorder: 0,
                },
                {
                  text: "USER",
                  value: this.props.navigation.getParam(
                    "centerDataName",
                    "N/A"
                  ),
                  rightBorder: 1,
                  bottomBorder: 1,
                },
                {
                  text: "NEW BALANCE",
                  value: Number(GLOBAL.userBalance) - Number(this.state.amount),
                  rightBorder: 0,
                  bottomBorder: 1,
                },
              ]}
            />

            <ServiceHeader
              title={"FOR"}
              imgHeightWidth={{
                width: width * 0.12,
                height: width * 0.12,
              }}
              centerData={GLOBAL.service_mob_no}
              centerDataName={this.props.navigation.getParam(
                "centerDataName",
                "N/A"
              )}
              imgName={GLOBAL.imgName}
            />

            {this.borderBottom()}
            <ServiceAmountChargePrev
              amount={this.state.amount}
              charge={"0.00"}
              total={this.state.amount}
            />

            <TextInput
              style={{ textAlign: "center", padding: width * 0.03 }}
              onChangeText={(note) => {
                this.setState({ note });
              }}
              value={this.state.note}
              multiline={true}
              maxHeight={width * 0.2}
              placeholder="Wrtie something about your transfer"
              keyboardType="default"
              autoCorrect={false}
            />
            {this.borderBottom()}

            <AppInput
              placeHolder={"Enter PIN"}
              placeholderTextColor={"#bdc3c7"}
              keyboardType={"decimal-pad"}
              leftIcon={"lock"}
              leftIconColor={"#00CDC8"}
              rightIconColor={"#00CDC8"}
              autoFocus={true}
              inputValue={(inputValue) => this.setState({ pin: inputValue })}
              next={this.toggleModal}
              secureTextEntry={true}
            />
          </View>
        }
      />
    );
  }
}
