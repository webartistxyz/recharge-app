import React from "react";
import {
  View,
  Dimensions,
  Platform,
  TextInput,
  TouchableOpacity,
  ScrollView,
  StyleSheet,
} from "react-native";
import axios from "axios";
import PhoneNoModal from "../../components/Modal";
import GetMobileNumbers from "../../components/GetMobileNumbers";
import { Ionicons } from "@expo/vector-icons";
import DefaultScreen from "../../components/DefaultScreen";
import ServiceAmount from "../../components/ServiceAmount";
import AvailAbleBalance from "../../components/AvailAbleBalance";
import LoderModal from "../../components/LoaderModal";
import GLOBAL from "../../globalState";
const { width } = Dimensions.get("window");
const leftData = [
  {
    title: "500",
    value: false,
  },
  {
    title: "1000",
    value: false,
  },
  {
    title: "3000",
    value: false,
  },
  {
    title: "5000",
    value: false,
  },
];
export default class BalanceTransferScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      transfer_to: "",
      amount: "",
      isOpenModal: false,
      isValidNo: false,
      isLoading: false,
    };
  }

  static navigationOptions = ({ navigation }) => {
    const { state } = navigation;
    const params = state.params || {};

    return {
      headerLeft: (
        <Ionicons
          name={Platform.OS === "ios" ? "ios-arrow-back" : "md-arrow-back"}
          size={Platform.OS === "ios" ? 35 : 24}
          color={"#fff"}
          style={
            Platform.OS === "ios"
              ? { marginBottom: -4, width: 25, marginLeft: 9 }
              : { marginBottom: -4, width: 25, marginLeft: 20 }
          }
          onPress={() => {
            navigation.goBack();
          }}
        />
      ),
      title: "Balance Transfer",
      headerTintColor: "#fff",
      headerStyle: {
        backgroundColor: params.backgroundColor || "#00cec9",
      },
      headerTitleStyle: {
        color: "#fff",
      },
    };
  };

  componentDidMount() {
    this.props.navigation.setParams({
      backgroundColor: "#00cec9",
    });
  }

  toggleModal = (color) => {
    this.setState({
      isOpenModal: !this.state.isOpenModal,
    });
    this.props.navigation.setParams({
      backgroundColor: color,
    });
  };

  validatePhoneNo(phone_no) {
    if (phone_no.length > 4) {
      this.setState({ isValidNo: true });
    } else {
      this.setState({ isValidNo: false });
    }
  }

  modalContent = () => {
    return (
      <GetMobileNumbers
        setPhoneNO={(phone_no) => {
          this.setState({ transfer_to: phone_no });
          this.validatePhoneNo(phone_no);
        }}
        isValidNo={this.state.isValidNo}
        notValidate={true}
        moveTo={() => {
          this.toggleModal("#00cec9");
        }}
        mobileNOAndPrefix={(phone_no, prefix) => {
          this.toggleModal("#00cec9");
          this.setState({ transfer_to: phone_no });
          this.validatePhoneNo(phone_no);
        }}
      />
    );
  };

  moveToPinScreen = async () => {
    this.setState({ isLoading: !this.state.isLoading });
    var allData = {
      apps_key: GLOBAL.rechargeScreen.apps_key,
      token: GLOBAL.rechargeScreen.token,
      transfer_to: this.state.transfer_to,
    };
    try {
      const userId = await axios.post(
        `https://${GLOBAL.appUrl}/api/numberToMemberCheck`,
        allData
      );
      if (userId.data.status == "10") {
        this.setState({ isLoading: !this.state.isLoading });
        GLOBAL.service_mob_no = this.state.transfer_to;
        GLOBAL.imgName = { uri: `https://${userId.data.memberImage}` };
        GLOBAL.serviceName = "Balance Transfer";
        this.props.navigation.navigate("BlanceTransferPin", {
          amount: this.state.amount,
          centerDataName: userId.data.memberName,
        });
      } else {
        this.setState({ isLoading: !this.state.isLoading });
        alert("User not found!");
      }
    } catch (error) {
      this.setState({ isLoading: !this.state.isLoading });
      console.log(error);
    }

    // GLOBAL.totalSms = this.state.messages;
    // GLOBAL.smsType = this.state.type;
    // GLOBAL.smsText = this.state.msg;
    // this.props.navigation.navigate("SmsPin");
  };

  render() {
    return (
      <DefaultScreen
        gridFlex={true}
        screenData={
          <View style={{ alignItems: "center" }}>
            <LoderModal isLoading={this.state.isLoading} />
            <PhoneNoModal
              modalWidth="100%"
              modalHeight="100%"
              toggleModal={() => this.toggleModal("#00cec9")}
              isModalVisible={this.state.isOpenModal}
              modalContent={this.modalContent()}
              iconColor={"#B83227"}
            />

            <View style={styles.searchBox}>
              <TextInput
                style={styles.searchInput}
                autoCapitalize="none"
                placeholder="Recipient Account No"
                autoCorrect={false}
                keyboardType="phone-pad"
                value={this.state.transfer_to}
                onChangeText={(transfer_to) => {
                  this.setState({ transfer_to });
                }}
              />

              <TouchableOpacity
                onPress={() => this.toggleModal("#B83227")}
                style={styles.contactIcon}
              >
                <Ionicons
                  name="md-person-add"
                  size={width * 0.06}
                  color="#bdc3c7"
                />
              </TouchableOpacity>
            </View>

            <ServiceAmount
              moveToPin={(data) => this.moveToPinScreen(data.amount)}
              hideRightArrow={
                this.state.transfer_to && this.state.amount ? false : true
              }
              leftDataFlex={3}
              leftData={leftData}
              selectedLeftData={(amount) => this.setState({ amount })}
              inputValue={(amount) => this.setState({ amount })}
            />
            <AvailAbleBalance />
          </View>
        }
      />
    );
  }
}
const styles = StyleSheet.create({
  searchBox: {
    justifyContent: "center",
    flexDirection: "row",
    borderBottomWidth: 1,
    borderColor: "#bdc3c7",
    padding: width * 0.04,
  },

  searchInput: {
    flex: 9,
    alignItems: "flex-start",
  },
  contactIcon: {
    flex: 1,
    alignItems: "flex-end",
    justifyContent: "center",
  },
});
