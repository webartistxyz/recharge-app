import React from "react";
import {
  StyleSheet,
  Text,
  Image,
  View,
  TouchableOpacity,
  Dimensions,
  AsyncStorage,
  ScrollView
} from "react-native";
import { NavigationActions, StackActions } from "react-navigation";
import AppInput from "../components/AppInput";
import Constants from "expo-constants";
import axios from "axios";
import GLOBAL from "../globalState";
import LoderModal from "../components/LoaderModal";
import ConfirmConnection from "../ConfirmNetConnection";
const { width, height } = Dimensions.get("window");

export default class UserNameScreeen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      apps_key: "",
      username: GLOBAL.userId || "",
      deviceid: Constants.installationId,
      isLoading: false
    };
  }

  static navigationOptions = {
    title: "Pin",
    header: null
  };

  componentDidMount() {
    this.getUserData("user_data");
  }

  _moveSignup = () => {
    this.props.navigation.navigate("Signup");
  };
  getUserData = async key => {
    try {
      let result = await AsyncStorage.getItem(key);
      var data = JSON.parse(result);
      if (data) {
        if (data.apps_key) {
          GLOBAL.app_details = {
            apps_key: data.apps_key,
            token: data.token,
            mobile: data.mobile
          };

          const resetAction = StackActions.reset({
            index: 0,
            key: null,
            actions: [NavigationActions.navigate({ routeName: "PinScreen" })]
          });
          this.props.navigation.dispatch(resetAction);
        }
      }
    } catch (err) {
      console.log(err);
    }
  };

  checkUserID = async () => {
    const connectionStatus = await ConfirmConnection.CheckNetConnection();
    if (connectionStatus) {
      this.setState({ isLoading: !this.state.isLoading });
      if (this.state.username) {
        axios
          .post(`https://${GLOBAL.appUrl}/api/useChek`, {
            username: this.state.username
          })
          .then(res => {
            this.setState({ isLoading: !this.state.isLoading });
            //console.log(res.data)
            if (res.data.status === "true") {
              GLOBAL.userId = this.state.username;
              this.props.navigation.navigate("Password");
              //console.log(GLOBAL.userId)
            } else {
              alert("User ID not found!");
            }
          })
          .catch(err => {
            console.log(err);
          });
      } else {
        alert("Please enter your username.");
      }
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <LoderModal isLoading={this.state.isLoading} />

        <View style={{ alignItems: "center" }}>
          <View style={styles.logoContainer}>
            <Image
              style={{ height: width * 0.18, width: width * 0.18 }}
              source={require("../assets/o_logo.png")}
            />
          </View>
          <ScrollView
            keyboardShouldPersistTaps="always"
            showsVerticalScrollIndicator={false}
          >
            <View
              style={{
                backgroundColor: "#fff",
                width: width * 0.93,
                marginBottom: "1%",
                borderRadius: 2
              }}
            >
              <AppInput
                inputTitle={"ENTER YOUR NETOCH USER ID"}
                placeHolder={"Enter User ID"}
                placeholderTextColor={"#bdc3c7"}
                keyboardType={"default"}
                leftIcon={"user"}
                leftIconColor={"#4050B5"}
                autoFocus={true}
                setInputValue={this.state.username}
                inputValue={inputValue =>
                  this.setState({ username: inputValue })
                }
                next={this.checkUserID}
              />
            </View>

            <View
              style={{ flexDirection: "row", justifyContent: "space-between" }}
            >
              <View style={styles.footer_one}>
                <TouchableOpacity
                  onPress={() => {}}
                  style={{
                    padding: width * 0.03,
                    backgroundColor: "#fff",
                    borderRadius: 30
                  }}
                >
                  <Text>FORGOT USER ID? </Text>
                </TouchableOpacity>
              </View>
              <View style={styles.footer_two}>
                <TouchableOpacity
                  onPress={this._moveSignup}
                  style={{
                    padding: width * 0.03,
                    backgroundColor: "#fff",
                    borderRadius: 30
                  }}
                >
                  <Text> SIGN UP </Text>
                </TouchableOpacity>
              </View>
            </View>
          </ScrollView>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#4050B5"
  },
  logoContainer: {
    alignItems: "center",
    marginTop: height * 0.12,
    marginBottom: "4%"
  },
  footer_one: {
    alignItems: "flex-start",
    marginTop: "2%",
    marginLeft: "2%"
  },
  footer_two: {
    alignItems: "flex-end",
    marginTop: "2%",
    marginRight: "2%"
  }
});
