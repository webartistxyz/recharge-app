import React from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Platform,
  Dimensions,
} from "react-native";
import { Ionicons } from "@expo/vector-icons";
const { width, height } = Dimensions.get("window");

export default class SettingsScreen extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    headerLeft: (
      <Ionicons
        name={Platform.OS === "ios" ? "ios-arrow-back" : "md-arrow-back"}
        size={Platform.OS === "ios" ? 35 : 24}
        color={"#fff"}
        style={
          Platform.OS === "ios"
            ? { marginBottom: -4, width: 25, marginLeft: 9 }
            : { marginBottom: -4, width: 25, marginLeft: 20 }
        }
        onPress={() => {
          navigation.goBack();
        }}
      />
    ),
    title: "Settings",
    headerStyle: {
      backgroundColor: "#4050B5",
    },
    headerTintColor: "#fff",
    headerTitleStyle: {
      fontWeight: "bold",
    },
  });

  settingsLink = (nav, text) => {
    return (
      <TouchableOpacity
        activeOpacity={0.8}
        onPress={() => this.props.navigation.navigate(nav)}
        style={styles.gridContainer}
      >
        <View style={styles.gridContent}>
          <Text style={styles.contentText}>{text}</Text>
          <Ionicons
            style={styles.contentIcon}
            name="md-arrow-forward"
            size={width * 0.06}
            color="#4050B5"
          />
        </View>
      </TouchableOpacity>
    );
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={{ marginTop: height * 0.03 }}>
          {this.settingsLink("ChangePicture", "Change Picture")}
          {this.settingsLink("Nid", "Set NID")}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#4050B5",
    alignItems: "center",
  },
  gridContainer: {
    width: width * 0.93,
    marginBottom: 1,
    backgroundColor: "#fff",
  },
  gridContent: {
    padding: width * 0.05,
    flexDirection: "row",
    justifyContent: "space-between",
  },
  contentText: {
    alignItems: "center",
    fontSize: width * 0.04,
  },
  contentIcon: {
    alignItems: "center",
  },
});
