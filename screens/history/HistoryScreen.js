import React from "react";
import {
  StyleSheet,
  View,
  FlatList,
  TouchableOpacity,
  Dimensions,
  TextInput,
  Text,
  ActivityIndicator,
  Platform,
} from "react-native";
import { Table, TableWrapper, Row, Cell } from "react-native-table-component";

import HistoryContent from "../../components/HistoryContent";
import HistoryDetailsModal from "./HistoryDetailsModal";
import axios from "axios";
import GLOBAL from "../../globalState";
import { Ionicons } from "@expo/vector-icons";
import ConfirmConnection from "../../ConfirmNetConnection";
const { width, height } = Dimensions.get("window");

export default class HistoryScreen extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      historys: [],
      tempHistorys: [],
      isModalVisible: false,
      modalData: {},
      search: "",
      refreshing: false,
      paginator: {},
      tableHead: ["Service", "Pcode", "Number", "Status", "Amount"],
      tableData: [
        ["1", "2", "3", "4", "f"],
        ["a", "b", "c", "d", "e"],
        ["1", "2", "3", "4", "5"],
        ["a", "b", "c", "d", "e"],
      ],
      widthArr: [40, 30, 180, 100, 100],
    };
  }

  static navigationOptions = ({ navigation }) => ({
    headerLeft: (
      <Ionicons
        name={Platform.OS === "ios" ? "ios-arrow-back" : "md-arrow-back"}
        size={Platform.OS === "ios" ? 35 : 24}
        color={"#fff"}
        style={
          Platform.OS === "ios"
            ? { marginBottom: -4, width: 25, marginLeft: 9 }
            : { marginBottom: -4, width: 25, marginLeft: 20 }
        }
        onPress={() => {
          navigation.goBack();
        }}
      />
    ),
    title: "Service History",
    headerTintColor: "#fff",
    headerStyle: {
      backgroundColor: "#4050B5",
    },
    headerTitleStyle: {
      color: "#fff",
    },
  });

  async componentDidMount() {
    const connectionStatus = await ConfirmConnection.CheckNetConnection();
    if (connectionStatus)
      this._getHistory(`https://${GLOBAL.appUrl}/api/RechargeHistory`);
  }

  makePagination(data) {
    let paginator = {
      current_page: data.current_page,
      last_page: data.last_page,
      next_page_url: data.next_page_url,
      prev_page_url: data.prev_page_url,
    };
    this.setState({ paginator });
  }

  _getHistory = (url) => {
    this.setState({ isLoading: !this.state.isLoading });
    axios
      .post(url, {
        apps_key: GLOBAL.rechargeScreen.apps_key,
        token: GLOBAL.rechargeScreen.token,
        search: this.state.search,
      })
      .then((res) => {
        //console.log(res.data)
        this.setState(
          {
            isLoading: !this.state.isLoading,
            historys: [...this.state.historys, ...res.data.data],
            tempHistorys: [...this.state.historys, ...res.data.data],
          },
          () => this.makePagination(res.data)
        );
      });
  };

  refreshAndSearchHistory = () => {
    axios
      .post(`https://${GLOBAL.appUrl}/api/RechargeHistory`, {
        apps_key: GLOBAL.rechargeScreen.apps_key,
        token: GLOBAL.rechargeScreen.token,
        search: this.state.search,
      })
      .then((res) => {
        this.setState(
          {
            isLoading: false,
            refreshing: false,
            historys: res.data.data,
            tempHistorys: res.data.data,
          },
          () => {
            this.makePagination(res.data);
          }
        );
      });
  };

  handleRefresh = () => {
    this.setState(
      {
        refreshing: !this.state.refreshing,
        search: "",
        historys: [],
        tempHistorys: [],
      },
      () => {
        this.refreshAndSearchHistory();
      }
    );
  };

  search_history = () => {
    this.setState(
      {
        isLoading: !this.state.isLoading,
        historys: [],
        tempHistorys: [],
      },
      () => this.refreshAndSearchHistory()
    );

    // var newData = [];

    // if (phone_no.length && this.state.tempHistorys.length) {
    //   newData = this.state.tempHistorys.filter( historyDetails => {
    //     const string = JSON.stringify(historyDetails);
    //     let numberString = String(phone_no);

    //     return string.includes(numberString) > -1;
    //     }
    //   );
    //   this.setState({
    //     historys: newData
    //   });
    // }else{
    //   this.setState({
    //     historys: this.state.tempHistorys
    //   })
    // }
  };

  renderFooter = () => {
    return (
      <View style={styles.footer}>
        {this.state.isLoading ? (
          <ActivityIndicator color="black" style={{ margin: 15 }} />
        ) : null}
      </View>
    );
  };

  render() {
    return (
      <View style={styles.container}>
        <View
          style={[
            styles.gridContainer,
            { height: height * 0.95, width: width * 0.93 },
          ]}
        >
          <HistoryDetailsModal
            toggleModal={() => {
              this.setState({ isModalVisible: !this.state.isModalVisible });
            }}
            isModalVisible={this.state.isModalVisible}
            modalData={this.state.modalData}
          />

          <View
            style={{
              justifyContent: "center",
              flexDirection: "row",
              borderWidth: 1,
              borderColor: "#bdc3c7",
              borderRadius: 30,
              margin: width * 0.02,
            }}
          >
            <TextInput
              style={{
                flex: 9,
                alignItems: "flex-start",
                paddingBottom: width * 0.015,
                paddingTop: width * 0.015,
                paddingLeft: width * 0.03,
              }}
              autoCapitalize="none"
              ref={(input) => {
                this.searchInput = input;
              }}
              placeholder="Search history by mobile number"
              autoCorrect={false}
              keyboardType="phone-pad"
              value={this.state.search}
              onChangeText={(search) => {
                this.setState({ search });
              }}
            />
            <TouchableOpacity
              onPress={this.state.search.length ? this.search_history : null}
              style={{
                flex: 1,
                alignItems: "flex-end",
                justifyContent: "center",
                paddingBottom: width * 0.015,
                paddingTop: width * 0.015,
                paddingRight: width * 0.03,
                backgroundColor: "#EEF1F1",
                borderTopEndRadius: 20,
                borderBottomEndRadius: 20,
              }}
            >
              <Ionicons name="md-search" size={width * 0.06} color="#4050B5" />
            </TouchableOpacity>
          </View>

          <FlatList
            data={this.state.historys}
            initialNumToRender={30}
            maxToRenderPerBatch={30}
            updateCellsBatchingPeriod={30}
            windowSize={30}
            refreshing={this.state.refreshing}
            onRefresh={this.handleRefresh}
            onEndReached={() => {
              if (this.state.paginator.next_page_url && !this.state.isLoading)
                this._getHistory(this.state.paginator.next_page_url);
            }}
            keyExtractor={(item) => item.id.toString()}
            renderItem={({ item }) => {
              return (
                <HistoryContent
                  pressed={() => {
                    this.setState({
                      isModalVisible: !this.state.isModalVisible,
                      modalData: item,
                    });
                  }}
                  serviceImg={item.serviceslogo}
                  serviceName={item.service_name}
                  serviceAcNo={item.number}
                  costOrTransectionTitle="Cost: "
                  costOrTransection={item.cost}
                  statusColor={item.requestStatusColor}
                  status={item.requestStatus}
                  requestDatetime={item.requestDatetime}
                  amount={item.amount}
                />
              );
            }}
            ListFooterComponent={this.renderFooter}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#4050B5",
    alignItems: "center",
  },
  gridContainer: {
    flex: 1,
    alignItems: "center",
    marginTop: width * 0.03,
    marginBottom: width * 0.03,
    backgroundColor: "#fff",
    borderRadius: 3,
  },
  footer: {
    padding: 10,
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
  },
  textStyle: { fontSize: width * 0.02, fontWeight: "bold" },
});
