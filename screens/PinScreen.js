import React from "react";
import {
  StyleSheet,
  Text,
  Image,
  View,
  ScrollView,
  TouchableOpacity,
  Dimensions,
  Platform,
  AsyncStorage,
  Alert,
} from "react-native";
import Constants from "expo-constants";
// import * as Location from "expo-location";
// import * as SMS from "expo-sms";
import * as Permissions from "expo-permissions";
import * as Contacts from "expo-contacts";
import axios from "axios";
import GLOBAL from "../globalState";
import GLOBAL_FUNCTIONS from "../globalFunctions";
import LoderModal from "../components/LoaderModal";
import AppInput from "../components/AppInput";
import ConfirmConnection from "../ConfirmNetConnection";
import ReactFunctions from "./ReactFunctions";
const { width, height } = Dimensions.get("window");

export default class PinScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      apps_key: GLOBAL.app_details.apps_key,
      pin_code: "",
      deviceid: Constants.installationId,
      errorMessage: "",
      address: "Dhaka",
      isLoading: false,
      latitudeAndLongitude: { latitude: 23.685, longitude: 90.3563 },
      hasLocationPermission: null,
      hasContactPermissiion: null,
    };
  }

  static navigationOptions = {
    title: "Pin",
    header: null,
  };

  // async UNSAFE_componentWillMount() {
  //this.getLocation();
  // const { status } = await Permissions.askAsync(Permissions.CONTACTS);
  // this.setState(
  //   {
  //     hasContactPermissiion: status === "granted"
  //   },
  //   () => this.takeContacts()
  // );
  // }
  // getLocation() {
  //   if (Platform.OS === "android" && !Constants.isDevice) {
  //     this.setState({
  //       errorMessage:
  //         "Oops, this will not work on Sketch in an Android emulator. Try it on your device!",
  //     });
  //   } else {
  //     this._getLocationAsync();
  //   }
  // }

  takeContacts = async () => {
    if (this.state.hasContactPermissiion) {
      try {
        this.setState({ isLoading: !this.state.isLoading });
        const { data } = await Contacts.getContactsAsync();

        if (data.length > 0) {
          this.setState({ isLoading: !this.state.isLoading });
          this.getLocation();
          GLOBAL.contactList = data.sort((a, b) => {
            x = JSON.stringify(a.firstName + " " + a.lastName);
            y = JSON.stringify(b.firstName + " " + b.lastName);

            if (
              GLOBAL_FUNCTIONS.convertToUppercase(x) <
              GLOBAL_FUNCTIONS.convertToUppercase(y)
            ) {
              return -1;
            }
            if (
              GLOBAL_FUNCTIONS.convertToUppercase(x) >
              GLOBAL_FUNCTIONS.convertToUppercase(y)
            ) {
              return 1;
            }
            return 0;
          });
        }
      } catch (error) {
        console.log(error);
      }
    }
  };

  // _getSmsAsync = async () => {
  //   const isAvailable = await SMS.isAvailableAsync();
  //   console.log(isAvailable);
  // };

  // _getLocationAsync = async () => {
  //   let { status } = await Permissions.askAsync(Permissions.LOCATION);

  //   this.setState({
  //     hasLocationPermission: status === "granted",
  //   });
  //   if (this.state.hasLocationPermission) {
  //     try {
  //       let location = await Location.getCurrentPositionAsync({});
  //       let latitudeAndLongitude = {
  //         latitude: location.coords.latitude,
  //         longitude: location.coords.longitude,
  //       };
  //       this.state.latitudeAndLongitude = latitudeAndLongitude;
  //       this._getAddress(latitudeAndLongitude);
  //     } catch (error) {
  //       console.log(`${error}.. location not available`);
  //     }
  //   }

  //   // let latitude = location.coords.latitude;
  //   // let longitude = location.coords.longitude;
  //   // let myApiKey = "AIzaSyDbUh-JzjbI65wx074IUnA52GwyaVfBt-w";
  //   // fetch('https://maps.googleapis.com/maps/api/geocode/json?address=' + 23.6925136 + ',' + 90.4917125 + '&key=' + "AIzaSyDbUh-JzjbI65wx074IUnA52GwyaVfBt-w")
  //   //     .then((response) => response.json())
  //   //     .then((responseJson) => {
  //   //         console.log('ADDRESS GEOCODE is BACK!! => ' + JSON.stringify(responseJson));
  //   // })
  //   //this.setState({ location });
  // };
  // _getAddress = async (location) => {
  //   try {
  //     let address = await Location.reverseGeocodeAsync(location);
  //     this.state.address = address;
  //     console.log(this.state);
  //     //this.setState({ address });
  //   } catch (error) {
  //     console.log(`${error}.. address not available`);
  //   }
  //   //console.log(this.state.address)
  // };

  signInWithPin = async () => {
    const connectionStatus = await ConfirmConnection.CheckNetConnection();
    if (connectionStatus) {
      this.setState({ isLoading: !this.state.isLoading });
      const mobile = GLOBAL.app_details.mobile;
      if (this.state.pin_code) {
        axios
          .post(`https://${GLOBAL.appUrl}/api/useLoginPin`, this.state)
          .then((res) => {
            if (res.data.status === "1") {
              GLOBAL.userBalance = res.data.userBalance;
              GLOBAL.userImage = res.data.userImage;
              GLOBAL.nid_back = res.data.nid_back;
              GLOBAL.nid_front = res.data.nid_front;
              GLOBAL.nid_status = res.data.nid_status;
              GLOBAL.rechargeScreen = {
                mobile: mobile,
                userRole: res.data.userRole,
                apps_key: this.state.apps_key,
                token: res.data.token,
              };

              this.setState({ isLoading: !this.state.isLoading });
              this.props.navigation.replace("Home", {
                mobile: mobile,
                userRole: res.data.userRole,
                apps_key: this.state.apps_key,
                token: res.data.token,
              });
            }
            if (res.data.status === "50") {
              this.setState({ isLoading: !this.state.isLoading });
              alert("User is not found.");
            }
            if (res.data.status === "10") {
              this.setState({ isLoading: !this.state.isLoading });
              alert("Incorrect pin.");
            }
          });
      } else {
        alert("Please enter your pin.");
      }
    }
  };
  logOut = () => {
    ReactFunctions.logOut(this.props);
  };

  forgotPinOrPass = () => {
    Alert.alert(
      "Forgot Pin",
      "Do you want to reset ?",
      [
        { text: "No", style: "cancel" },
        {
          text: "Yes",
          onPress: async () => {
            this.setState({ isLoading: !this.state.isLoading });
            const confirm = await ReactFunctions.forgotPinOrPass();
            if (confirm) {
              this.setState({ isLoading: !this.state.isLoading });
              ReactFunctions.logOut(this.props);
            }
          },
        },
      ],
      { cancelable: false }
    );
  };

  render() {
    const mobile = GLOBAL.app_details.mobile;
    const screenWidth = width * 0.93;
    return (
      <View style={styles.container}>
        <LoderModal isLoading={this.state.isLoading} />

        <View style={{ alignItems: "center" }}>
          <View style={styles.logoContainer}>
            <Image
              style={{ height: width * 0.18, width: width * 0.18 }}
              source={require("../assets/o_logo.png")}
            />
          </View>
          <ScrollView
            keyboardShouldPersistTaps="always"
            showsVerticalScrollIndicator={false}
          >
            <View
              style={{
                backgroundColor: "#fff",
                width: screenWidth,
                marginBottom: "1%",
                borderRadius: 2,
              }}
            >
              <View style={styles.userIdContainer}>
                <View>
                  <Text
                    style={{
                      color: "#00326C",
                      fontSize: width * 0.033,
                      fontWeight: "bold",
                      paddingBottom: "1%",
                    }}
                  >
                    NETOCH ACCOUNT
                  </Text>
                </View>
                <View>
                  <Text style={{ fontSize: width * 0.063 }}>{mobile}</Text>
                </View>
              </View>
              <AppInput
                inputTitle={"ENTER YOUR NETOCH PIN"}
                placeHolder={"Enter PIN"}
                placeholderTextColor={"#bdc3c7"}
                keyboardType={"decimal-pad"}
                leftIcon={"lock"}
                leftIconColor={"#4050B5"}
                autoFocus={true}
                inputValue={(inputValue) =>
                  this.setState({ pin_code: inputValue })
                }
                next={this.signInWithPin}
                secureTextEntry={true}
              />
            </View>
            <View style={styles.footerContainer}>
              <View style={styles.footer}>
                <TouchableOpacity
                  onPress={this.logOut}
                  style={{
                    padding: width * 0.03,
                    backgroundColor: "#fff",
                    borderRadius: 30,
                  }}
                >
                  <Text>LOG OUT </Text>
                </TouchableOpacity>
              </View>
              <View style={styles.footer}>
                <TouchableOpacity
                  onPress={this.forgotPinOrPass}
                  style={{
                    padding: width * 0.03,
                    backgroundColor: "#fff",
                    borderRadius: 30,
                  }}
                >
                  <Text>FORGOT? </Text>
                </TouchableOpacity>
              </View>
            </View>

            <View style={{ height: height * 0.78 }}></View>
          </ScrollView>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#4050B5",
  },
  logoContainer: {
    alignItems: "center",
    marginTop: height * 0.09,
    marginBottom: height * 0.016,
  },
  userIdContainer: {
    alignItems: "center",
    borderBottomWidth: 2,
    borderBottomColor: "#ecf0f1",
    paddingBottom: "2%",
    paddingTop: "2%",
  },
  footerContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  footer: {
    marginTop: "2%",
    marginLeft: "2%",
  },
});
