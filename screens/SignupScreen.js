import React from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  TouchableOpacity,
  Alert,
  Dimensions
} from "react-native";
import GLOBAL from "../globalState";
import { Form, Item, Input, Label, Button } from "native-base";
import LoderModal from "../components/LoaderModal";
import axios from "axios";
import ConfirmConnection from "../ConfirmNetConnection";
const { width, height } = Dimensions.get("window");

export default class SignupScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      full_name: "",
      mobile: "",
      email: "",
      referrer_id: GLOBAL.rechargeScreen.mobile || "",
      isLoading: false
    };
  }

  static navigationOptions = {
    title: "SignIn",
    header: null
  };

  signUp = async () => {
    const connectionStatus = await ConfirmConnection.CheckNetConnection();
    if (connectionStatus) {
      this.setState({ isLoading: !this.state.isLoading });

      try {
        let res = await axios.post(
          `https://${GLOBAL.appUrl}/api/signup`,
          this.state
        );
        this.setState({ isLoading: !this.state.isLoading });
        if (res) {
          //console.log(res);
          if (res.data.status == "10") {
            Alert.alert(
              "Sign Up",
              res.data.message,
              [{ text: "OK", onPress: () => this.props.navigation.goBack() }],
              { cancelable: false }
            );
          } else {
            alert(res.data.message);
          }
        }
      } catch (error) {
        this.setState({ isLoading: !this.state.isLoading });
        console.log(error);
      }
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <LoderModal isLoading={this.state.isLoading} />

        <View style={{ alignItems: "center" }}>
          <View style={styles.logoContainer}>
            <Image
              style={{ height: width * 0.18, width: width * 0.18 }}
              source={require("../assets/o_logo.png")}
            />
          </View>
        </View>

        <ScrollView
          keyboardShouldPersistTaps="always"
          showsVerticalScrollIndicator={false}
        >
          <Form style={styles.form}>
            <View style={styles.headerTitle}>
              <Text
                style={{
                  color: "#00326C",
                  fontSize: width * 0.031,
                  fontWeight: "bold",
                  paddingBottom: "1%"
                }}
              >
                Member Sign Up Panel
              </Text>
            </View>
            <Item floatingLabel>
              <Label>Full Name (as your national ID)</Label>
              <Input
                autoCapitalize="none"
                autoCorrect={false}
                keyboardType="default"
                onChangeText={full_name => this.setState({ full_name })}
              />
            </Item>

            <Item floatingLabel>
              <Label>Mobile No</Label>
              <Input
                autoCapitalize="none"
                autoCorrect={false}
                keyboardType="phone-pad"
                onChangeText={mobile => this.setState({ mobile })}
              />
            </Item>

            <Item floatingLabel>
              <Label>Email</Label>
              <Input
                autoCapitalize="none"
                autoCorrect={false}
                keyboardType="email-address"
                onChangeText={email => this.setState({ email })}
              />
            </Item>

            <Item floatingLabel>
              <Label>Refferal ID/Mobile</Label>
              <Input
                autoCapitalize="none"
                autoCorrect={false}
                keyboardType="default"
                value={this.state.referrer_id}
                onChangeText={referrer_id => this.setState({ referrer_id })}
              />
            </Item>

            <Button
              style={styles.button}
              disabled={
                !this.state.full_name ||
                !this.state.mobile ||
                !this.state.referrer_id
              }
              primary={
                this.state.full_name &&
                this.state.mobile &&
                this.state.referrer_id
                  ? true
                  : false
              }
              full
              onPress={this.signUp}
            >
              <Text style={styles.buttonText}>
                {" "}
                {!this.props.navigation.getParam("drawerMenu")
                  ? "Sign Up"
                  : "Add Member"}
              </Text>
            </Button>

            {!this.props.navigation.getParam("drawerMenu") ? (
              <View style={styles.footer}>
                <Text>OR</Text>
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate("UserName");
                  }}
                >
                  <Text>Login</Text>
                </TouchableOpacity>
              </View>
            ) : (
              <View style={styles.footer}>
                <Text>OR</Text>
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.goBack();
                  }}
                >
                  <Text>Back to previous screen</Text>
                </TouchableOpacity>
              </View>
            )}
          </Form>

          <View style={{ height: height * 0.6 }}></View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#4050B5"
  },
  logoContainer: {
    alignItems: "center",
    marginTop: height * 0.12,
    marginBottom: "4%"
  },
  form: {
    margin: height * 0.02,
    padding: height * 0.02,
    borderRadius: 5,
    backgroundColor: "#fff"
  },
  button: {
    marginTop: 20
  },
  buttonText: {
    color: "#fff"
  },
  headerTitle: {
    alignItems: "center"
  },
  footer: {
    paddingTop: width * 0.09,
    alignItems: "center"
  }
});
