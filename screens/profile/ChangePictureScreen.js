import React from "react";
import { StyleSheet, View, Dimensions, Alert, Platform } from "react-native";
import { Ionicons } from "@expo/vector-icons";
import ImageUploader from "../../components/ImageUploader";
import CustomButton from "../../components/CustomButton";
import LoderModal from "../../components/LoaderModal";
import GLOBAL from "../../globalState";
import GLOBAL_FUNCTION from "../../globalFunctions";
import ConfirmConnection from "../../ConfirmNetConnection";
const { width } = Dimensions.get("window");

export default class ChangePictureScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      profile_image: null,
      apps_key: GLOBAL.rechargeScreen.apps_key,
      token: GLOBAL.rechargeScreen.token,
      isLoading: false,
      hasCamerRollPermissiion: null,
      hasCamerPermissiion: null,
    };
  }

  static navigationOptions = ({ navigation }) => ({
    headerLeft: (
      <Ionicons
        name={Platform.OS === "ios" ? "ios-arrow-back" : "md-arrow-back"}
        size={Platform.OS === "ios" ? 35 : 24}
        color={"#fff"}
        style={
          Platform.OS === "ios"
            ? { marginBottom: -4, width: 25, marginLeft: 9 }
            : { marginBottom: -4, width: 25, marginLeft: 20 }
        }
        onPress={() => {
          navigation.goBack();
        }}
      />
    ),
    title: "Change Picture",
    headerStyle: {
      backgroundColor: "#4050B5",
    },
    headerTintColor: "#fff",
    headerTitleStyle: {
      fontWeight: "bold",
    },
  });

  _saveProfilePic = async () => {
    const connectionStatus = await ConfirmConnection.CheckNetConnection();
    if (connectionStatus) {
      this.setState({ isLoading: !this.state.isLoading });
      const response = await GLOBAL_FUNCTION.saveMultyPartFormData(
        `https://${GLOBAL.appUrl}/api/profilepicture`,
        {
          apps_key: this.state.apps_key,
          token: this.state.token,
          profile_image: {
            uri: this.state.profile_image,
            name: "userProfile.jpg",
            type: "image/jpg",
          },
        }
      );

      if (response) this.setState({ isLoading: !this.state.isLoading });

      if (response.data.status === "success") {
        GLOBAL.userImage = response.data.userImage;
        //alert(response.message);
        Alert.alert(
          "Profile Picture",
          response.data.message,
          [
            {
              text: "OK",
              onPress: () => GLOBAL_FUNCTION.backToHome(this.props),
            },
          ],
          { cancelable: false }
        );
      }
      //console.log(response);
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <LoderModal isLoading={this.state.isLoading} />

        <View>
          <ImageUploader
            image={(image) => this.setState({ profile_image: image })}
            imageWidth={200}
            imageHeight={200}
            imageStyle={styles.imageStyle}
            defaultImage={GLOBAL.userImage}
          />
          {this.state.profile_image ? (
            <CustomButton
              buttonTitle={"Save"}
              buttonPressed={this._saveProfilePic}
            />
          ) : null}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#4050B5",
    alignItems: "center",
  },
  imageStyle: {
    height: width * 0.5,
    width: width * 0.5,
    borderRadius: 100,
  },
});
