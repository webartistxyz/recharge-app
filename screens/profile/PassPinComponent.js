import React, { Component } from "react";
import { Text, View } from "react-native";
import { Button, Content, Form, Item, Input, Label } from "native-base";
export default class PassPinComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      current: "",
      newData: "",
      confirm: ""
    };
  }

  render() {
    return (
      <Content>
        <Form>
          <Item floatingLabel>
            <Label>{this.props.currentTitle}</Label>
            <Input
              autoCapitalize="none"
              onChangeText={current => {
                this.setState({ current });
              }}
              value={this.state.current}
              autoFocus={true}
              textContentType="password"
              secureTextEntry={true}
              keyboardType="decimal-pad"
            />
          </Item>
          <Item floatingLabel>
            <Label>{this.props.newTitle}</Label>
            <Input
              autoCapitalize="none"
              onChangeText={newData => {
                this.setState({ newData });
              }}
              value={this.state.newData}
              textContentType="password"
              secureTextEntry={true}
              keyboardType="decimal-pad"
            />
          </Item>
          <Item floatingLabel last>
            <Label>{this.props.confirmTitle}</Label>
            <Input
              autoCapitalize="none"
              onChangeText={confirm => {
                this.setState({ confirm });
              }}
              value={this.state.confirm}
              textContentType="password"
              secureTextEntry={true}
              keyboardType="decimal-pad"
            />
          </Item>
        </Form>
        <View style={{ margin: 17 }}>
          <Button
            disabled={
              !this.state.current || !this.state.newData || !this.state.confirm
            }
            primary={
              this.state.current && this.state.newData && this.state.confirm
                ? true
                : false
            }
            full
            onPress={() => this.props.buttonPress(this.state)}
          >
            <Text style={{ color: "#fff" }}> {this.props.buttonTitle} </Text>
          </Button>
        </View>
      </Content>
    );
  }
}
