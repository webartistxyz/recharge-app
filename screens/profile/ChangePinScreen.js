import React, { Component } from "react";
import { Platform } from "react-native";
import { Ionicons } from "@expo/vector-icons";
import GLOBAL from "../../globalState";
import GLOBAL_FUNCTION from "../../globalFunctions";
import LoderModal from "../../components/LoaderModal";
import ConfirmConnection from "../../ConfirmNetConnection";
import PassPinComponent from "./PassPinComponent";
import axios from "axios";
import { Container } from "native-base";
export default class ChangePinScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      current_pin: "",
      new_pin: "",
      confirm_pin: "",
      apps_key: GLOBAL.rechargeScreen.apps_key,
      token: GLOBAL.rechargeScreen.token,
      isLoading: false,
    };
  }

  static navigationOptions = ({ navigation }) => ({
    headerLeft: (
      <Ionicons
        name={Platform.OS === "ios" ? "ios-arrow-back" : "md-arrow-back"}
        size={Platform.OS === "ios" ? 35 : 24}
        color={"#fff"}
        style={
          Platform.OS === "ios"
            ? { marginBottom: -4, width: 25, marginLeft: 9 }
            : { marginBottom: -4, width: 25, marginLeft: 20 }
        }
        onPress={() => {
          navigation.goBack();
        }}
      />
    ),
    title: "Change your pin",
    headerTintColor: "#fff",
    headerStyle: {
      backgroundColor: "#4050B5",
    },
    headerTitleStyle: {
      color: "#fff",
    },
  });

  changePin = async (data) => {
    const connectionStatus = await ConfirmConnection.CheckNetConnection();
    if (connectionStatus) {
      this.setState({ isLoading: !this.state.isLoading });

      axios.post(`https://${GLOBAL.appUrl}/api/pinchange`, data).then((res) => {
        this.setState({
          isLoading: !this.state.isLoading,
        });
        if (res.data.status == "success") {
          GLOBAL_FUNCTION.logOut(this.props);
        } else {
          alert(res.data.message);
        }
      });
    }
  };

  render() {
    return (
      <Container>
        <LoderModal isLoading={this.state.isLoading} />
        <PassPinComponent
          currentTitle={"Current Pin"}
          newTitle={"New Pin"}
          confirmTitle={"Confirm Pin"}
          buttonTitle={"Change Pin"}
          buttonPress={(data) => {
            this.setState(
              {
                current_pin: data.current,
                new_pin: data.newData,
                confirm_pin: data.confirm,
              },
              () => this.changePin(this.state)
            );
          }}
        />
      </Container>
    );
  }
}
