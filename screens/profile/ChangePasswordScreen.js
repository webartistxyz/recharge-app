import React, { Component } from "react";
import { AsyncStorage, Platform } from "react-native";
import GLOBAL from "../../globalState";
import GLOBAL_FUNCTION from "../../globalFunctions";
import LoderModal from "../../components/LoaderModal";
import PassPinComponent from "./PassPinComponent";
import ConfirmConnection from "../../ConfirmNetConnection";
import { Ionicons } from "@expo/vector-icons";
import axios from "axios";
import { Container } from "native-base";
export default class ChangePasswordScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      current_pass: "",
      new_pass: "",
      confirm_pass: "",
      apps_key: GLOBAL.rechargeScreen.apps_key,
      token: GLOBAL.rechargeScreen.token,
      isLoading: false,
    };
  }

  static navigationOptions = ({ navigation }) => ({
    headerLeft: (
      <Ionicons
        name={Platform.OS === "ios" ? "ios-arrow-back" : "md-arrow-back"}
        size={Platform.OS === "ios" ? 35 : 24}
        color={"#fff"}
        style={
          Platform.OS === "ios"
            ? { marginBottom: -4, width: 25, marginLeft: 9 }
            : { marginBottom: -4, width: 25, marginLeft: 20 }
        }
        onPress={() => {
          navigation.goBack();
        }}
      />
    ),
    title: "Change your password",
    headerTintColor: "#fff",
    headerStyle: {
      backgroundColor: "#4050B5",
    },
    headerTitleStyle: {
      color: "#fff",
    },
  });

  changePassword = async (data) => {
    const connectionStatus = await ConfirmConnection.CheckNetConnection();
    if (connectionStatus) {
      this.setState({ isLoading: !this.state.isLoading });

      axios
        .post(`https://${GLOBAL.appUrl}/api/passwordchange`, data)
        .then((res) => {
          this.setState({ isLoading: !this.state.isLoading });
          if (res.data.status == "success") {
            AsyncStorage.removeItem("user_data");
            GLOBAL_FUNCTION.logOut(this.props);
          } else {
            alert(res.data.message);
          }
        });
    }
  };

  render() {
    return (
      <Container>
        <LoderModal isLoading={this.state.isLoading} />
        <PassPinComponent
          currentTitle={"Current Password"}
          newTitle={"New Password"}
          confirmTitle={"Confirm Password"}
          buttonTitle={"Change Password"}
          buttonPress={(data) => {
            this.setState(
              {
                current_pass: data.current,
                new_pass: data.newData,
                confirm_pass: data.confirm,
              },
              () => this.changePassword(this.state)
            );
          }}
        />
      </Container>
    );
  }
}
