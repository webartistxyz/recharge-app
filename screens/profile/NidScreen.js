import React from "react";
import {
  StyleSheet,
  View,
  Dimensions,
  ScrollView,
  Alert,
  Platform,
} from "react-native";
import { Ionicons } from "@expo/vector-icons";
import LoderModal from "../../components/LoaderModal";
import GLOBAL from "../../globalState";
import GLOBAL_FUNCTION from "../../globalFunctions";
import ConfirmConnection from "../../ConfirmNetConnection";
import ImageUploader from "../../components/ImageUploader";
import CustomButton from "../../components/CustomButton";
const { width } = Dimensions.get("window");

export default class NidScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      apps_key: GLOBAL.rechargeScreen.apps_key,
      token: GLOBAL.rechargeScreen.token,
      nid_front: "",
      nid_back: "",
      isLoading: false,
      hasCamerRollPermissiion: null,
      hasCamerPermissiion: null,
    };
  }

  static navigationOptions = ({ navigation }) => ({
    headerLeft: (
      <Ionicons
        name={Platform.OS === "ios" ? "ios-arrow-back" : "md-arrow-back"}
        size={Platform.OS === "ios" ? 35 : 24}
        color={"#fff"}
        style={
          Platform.OS === "ios"
            ? { marginBottom: -4, width: 25, marginLeft: 9 }
            : { marginBottom: -4, width: 25, marginLeft: 20 }
        }
        onPress={() => {
          navigation.goBack();
        }}
      />
    ),
    title: "Submit NID",
    headerStyle: {
      backgroundColor: "#4050B5",
    },
    headerTintColor: "#fff",
    headerTitleStyle: {
      fontWeight: "bold",
    },
  });

  _saveUserNid = async () => {
    const connectionStatus = await ConfirmConnection.CheckNetConnection();
    if (connectionStatus) {
      this.setState({ isLoading: !this.state.isLoading });
      const response = await GLOBAL_FUNCTION.saveMultyPartFormData(
        `https://${GLOBAL.appUrl}/api/nidpicture`,
        {
          apps_key: this.state.apps_key,
          token: this.state.token,
          nid_front: {
            uri: this.state.nid_front,
            name: "userNidFront.jpg",
            type: "image/jpg",
          },
          nid_back: {
            uri: this.state.nid_back,
            name: "userNidBack.jpg",
            type: "image/jpg",
          },
        }
      );

      if (response) this.setState({ isLoading: !this.state.isLoading });

      if (response.data.status === "success") {
        //GLOBAL.userImage = response.data.userImage
        //alert(response.message);
        Alert.alert(
          "NID",
          response.data.message,
          [
            {
              text: "OK",
              onPress: () => GLOBAL_FUNCTION.backToHome(this.props),
            },
          ],
          { cancelable: true }
        );
      }
      //console.log(response);
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <LoderModal isLoading={this.state.isLoading} />
        <ScrollView
          keyboardShouldPersistTaps="always"
          showsVerticalScrollIndicator={false}
        >
          <ImageUploader
            image={(image) => this.setState({ nid_front: image })}
            imageWidth={600}
            imageHeight={400}
            imageStyle={styles.imageStyle}
            defaultImage={GLOBAL.nid_front}
            uploaderBtnHide={GLOBAL.nid_status === "yes" ? true : false}
            imageTitle={"NID Front"}
          />
          <ImageUploader
            image={(image) => this.setState({ nid_back: image })}
            imageWidth={600}
            imageHeight={400}
            imageStyle={styles.imageStyle}
            defaultImage={GLOBAL.nid_back}
            uploaderBtnHide={GLOBAL.nid_status === "yes" ? true : false}
            imageTitle={"NID Back"}
          />

          {this.state.nid_front && this.state.nid_back ? (
            <CustomButton
              buttonTitle={"Save"}
              buttonPressed={this._saveUserNid}
            />
          ) : null}

          <View style={{ height: width * 0.3 }}></View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#4050B5",
    alignItems: "center",
  },
  imageStyle: {
    height: width * 0.5,
    width: width * 0.84,
    borderRadius: 5,
  },
});
