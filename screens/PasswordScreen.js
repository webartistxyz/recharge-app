import React from "react";
import {
  StyleSheet,
  Text,
  Image,
  View,
  ScrollView,
  TouchableOpacity,
  Dimensions,
  AsyncStorage,
  Alert
} from "react-native";
import { NavigationActions, StackActions } from "react-navigation";
import Constants from "expo-constants";
import axios from "axios";
import GLOBAL from "../globalState";
import LoderModal from "../components/LoaderModal";
import AppInput from "../components/AppInput";
import ConfirmConnection from "../ConfirmNetConnection";
import ReactFunctions from "./ReactFunctions";
const { width, height } = Dimensions.get("window");

export default class PasswordScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: GLOBAL.userId,
      password: "",
      deviceid: Constants.installationId,
      isLoading: false
    };
  }

  static navigationOptions = {
    title: "Pin",
    header: null
  };

  componentDidMount() {
    //this.secondTextInput.focus();
    //this._touchable.touchableHandlePress();
  }

  forgotPinOrPass = () => {
    Alert.alert(
      "Forgot Pin",
      "Do you want to reset ?",
      [
        { text: "No", style: "cancel" },
        {
          text: "Yes",
          onPress: async () => {
            this.setState({ isLoading: !this.state.isLoading });
            const confirm = await ReactFunctions.forgotPinOrPass();
            if (confirm) {
              this.setState({ isLoading: !this.state.isLoading });
              ReactFunctions.logOut(this.props);
            }
          }
        }
      ],
      { cancelable: false }
    );
  };

  signInUser = async () => {
    const connectionStatus = await ConfirmConnection.CheckNetConnection();
    if (connectionStatus) {
      this.setState({ isLoading: !this.state.isLoading });

      axios
        .post(`https://${GLOBAL.appUrl}/api/useLogin`, this.state)
        .then(res => {
          this.setState({ isLoading: !this.state.isLoading });
          if (res.data.status == "true") {
            //console.log(res.data)
            this.saveAppUserData(res.data);
          } else {
            alert(res.data.message);
          }
        });
    }
  };
  saveAppUserData = async data => {
    GLOBAL.app_details = {
      apps_key: data.apps_key,
      token: data.token,
      mobile: data.mobile
    };
    var user_data = {
      apps_key: data.apps_key,
      token: data.token,
      mobile: data.mobile
    };
    await AsyncStorage.setItem("user_data", JSON.stringify(user_data))
      .then(response => {
        //this.props.navigation.replace("PinScreen", user_data)

        const resetAction = StackActions.reset({
          index: 0,
          key: null,
          actions: [
            NavigationActions.navigate({ routeName: "PinScreen", user_data })
          ]
        });
        this.props.navigation.dispatch(resetAction);
      })
      .catch(error => {
        console.log(error);
      });
  };

  render() {
    return (
      <View style={styles.container}>
        <LoderModal isLoading={this.state.isLoading} />

        <View style={{ alignItems: "center" }}>
          <View style={styles.logoContainer}>
            <Image
              style={{ height: width * 0.18, width: width * 0.18 }}
              source={require("../assets/o_logo.png")}
            />
          </View>
          <ScrollView
            keyboardShouldPersistTaps="always"
            showsVerticalScrollIndicator={false}
          >
            <View
              style={{
                backgroundColor: "#fff",
                width: width * 0.93,
                marginBottom: "1%",
                borderRadius: 2
              }}
            >
              <View style={styles.userIdContainer}>
                <View>
                  <Text
                    style={{
                      color: "#00326C",
                      fontSize: width * 0.031,
                      fontWeight: "bold",
                      paddingBottom: "1%"
                    }}
                  >
                    NETOCH ACCOUNT
                  </Text>
                </View>
                <View>
                  <Text style={{ fontSize: width * 0.063 }}>
                    {GLOBAL.userId}
                  </Text>
                </View>
              </View>
              <AppInput
                inputTitle={"ENTER YOUR NETOCH PASSWORD"}
                placeHolder={"ENTER PASSWORD"}
                placeholderTextColor={"#bdc3c7"}
                keyboardType={"default"}
                leftIcon={"lock"}
                leftIconColor={"#4050B5"}
                autoFocus={true}
                inputValue={inputValue =>
                  this.setState({ password: inputValue })
                }
                next={this.signInUser}
                secureTextEntry={true}
              />
              {/* <View style={{justifyContent: "center", alignItems: "center", paddingTop: "2%"}}>
                                <Text style={{color: "#00326C", fontSize: width * .031, fontWeight: 'bold', paddingBottom: "1%"}}>
                                    ENTER YOUR NETOCH PASSWORD
                                </Text>
                            </View>
                            <View style={styles.inputContainer}>
                                <View style={[styles.nextButton, {flex: 1}]}>
                                    <Entypo
                                    name="lock"
                                    size={width * .09}
                                    color="#4050B5"
                                    />
                                </View>
                                <TextInput 
                                    style={{flex: 8, textAlign: "center"}}
                                    autoCapitalize="none"
                                    placeholderTextColor={"#bdc3c7"}
                                    onChangeText={password => {
                                        this.setState({password});
                                        }}
                                    value={this.state.password}
                                    autoFocus={true}
                                    ref={(touchable) => this._touchable = touchable}
                                    placeholder="Enter Password"
                                    textContentType="password"
                                    keyboardType="default"
                                    autoCorrect={false} secureTextEntry={true}  password={true} />
                                <View style={{flex: 1, justifyContent: "center", alignItems: "center"}}>
                                {
                                    this.state.password.length > 3 ? 
                                        <TouchableOpacity style={styles.nextButton} 
                                        onPress={ this.signInUser }>
                                            <Entypo
                                            name="arrow-right"
                                            size={width * .09}
                                            color="#4050B5"
                                            />
                                        </TouchableOpacity>
                                    : <View style={styles.nextButton}>
                                        <Entypo
                                            name="arrow-right"
                                            size={width * .09}
                                            color="#bdc3c7"
                                            />
                                    </View>
                                }
                                </View>
                            </View> */}
            </View>

            <View style={styles.footer}>
              <TouchableOpacity
                onPress={this.forgotPinOrPass}
                style={{
                  padding: width * 0.03,
                  backgroundColor: "#fff",
                  borderRadius: 30
                }}
              >
                <Text>FORGOT PASSWORD? </Text>
              </TouchableOpacity>
            </View>
            <View style={{ height: height * 0.78 }}></View>
          </ScrollView>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#4050B5"
  },
  logoContainer: {
    alignItems: "center",
    marginTop: height * 0.12,
    marginBottom: "4%"
  },
  userIdContainer: {
    alignItems: "center",
    borderBottomWidth: 2,
    borderBottomColor: "#ecf0f1",
    paddingBottom: "2%",
    paddingTop: "2%"
  },
  footer: {
    alignItems: "flex-start",
    marginTop: "2%",
    marginLeft: "2%"
  },
  inputContainer: {
    flexDirection: "row",
    justifyContent: "center",
    padding: width * 0.035
  },
  nextButton: {
    alignItems: "center",
    justifyContent: "center"
  }
});
