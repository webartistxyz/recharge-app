import React from "react";
import GetMobileNumbers from "../../components/GetMobileNumbersByPagi";
import { Platform } from "react-native";
import { Ionicons } from "@expo/vector-icons";

import axios from "axios";
import GLOBAL from "../../globalState";
import ConfirmConnection from "../../ConfirmNetConnection";

export default class PhoneNoScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      phone_no: "",
      operator: "",
      operator_id: "",
      operator_logo: "",
      apps_key: this.props.navigation.getParam("apps_key", ""),
      bd_operators: [],
      prefixes: [],
      isValidNo: false,
    };
  }

  static navigationOptions = ({ navigation }) => ({
    headerLeft: (
      <Ionicons
        name={Platform.OS === "ios" ? "ios-arrow-back" : "md-arrow-back"}
        size={Platform.OS === "ios" ? 35 : 24}
        color={"#fff"}
        style={
          Platform.OS === "ios"
            ? { marginBottom: -4, width: 25, marginLeft: 9 }
            : { marginBottom: -4, width: 25, marginLeft: 20 }
        }
        onPress={() => {
          navigation.goBack();
        }}
      />
    ),
    title: "Mobile Recharge",
    headerTintColor: "#fff",
    headerStyle: {
      backgroundColor: "#BA2F16",
    },
    headerTitleStyle: {
      color: "#fff",
    },
  });

  async componentDidMount() {
    const connectionStatus = await ConfirmConnection.CheckNetConnection();
    if (connectionStatus) this.getPrefixes();
  }

  getPrefixes = () => {
    axios.get(`https://${GLOBAL.appUrl}/api/Operator`).then((response) => {
      //console.log(response.data)
      this.setState({ bd_operators: response.data });
      GLOBAL.operators = response.data;

      var prefixesArr = [];
      this.state.bd_operators.forEach((element) => {
        prefixesArr.push(element.prefix);
      });
      var prefixesString = prefixesArr.join();
      var prefixesWithoutSpace = prefixesString.replace(/\s/g, "");
      var prefixesNewArr = prefixesWithoutSpace.split(",");

      var newOperatorsArr = [];
      prefixesNewArr.forEach((data) => {
        this.state.bd_operators.forEach((element) => {
          if (element.prefix.includes(data)) {
            newOperatorsArr.push({
              operator_id: element.id,
              operator_code: element.code,
              operator_name: element.name,
              operator_logo: element.logo,
              prefix: data,
            });
          }
        });
      });
      //console.log(newOperatorsArr);
      this.setState({ prefixes: newOperatorsArr });
    });
  };

  setOperatorEmpty() {
    GLOBAL.operator = "";
    GLOBAL.operatorLogo = "";
    GLOBAL.operator_id = "";
    this.setState({
      operator: "",
      operator_id: "",
      operator_logo: "",
      isValidNo: false,
    });
  }
  setOperatorExactly(data) {
    GLOBAL.operator = data.operator_name;
    GLOBAL.operatorLogo = data.operator_logo;
    GLOBAL.operator_id = data.operator_id;
    this.setState({
      operator: data.operator_name,
      operator_id: data.operator_id,
      operator_logo: data.operator_logo,
      isValidNo: true,
    });
  }

  next = (phone_no) => {
    //console.log(phone_no);
    if (phone_no.length === 11) {
      var prefix = phone_no.substr(0, 3);
      var prefix = prefix;
      if (this.state.prefixes.find((item) => item.prefix === prefix)) {
        var data = this.state.prefixes.find((item) => item.prefix === prefix);

        this.setOperatorExactly(data);
      }
      if (!this.state.prefixes.find((item) => item.prefix === prefix)) {
        this.setOperatorEmpty();
      }
    } else {
      this.setOperatorEmpty();
    }
  };

  setOperator(mobile_no, prefix) {
    if (this.state.prefixes.find((item) => item.prefix === prefix)) {
      var data = this.state.prefixes.find((item) => item.prefix === prefix);

      this.setOperatorExactly(data);

      this.moveToNext(mobile_no, data);
    } else {
      this.setOperatorEmpty();
    }
  }

  moveToNext = (mobile_no, data) => {
    ConfirmConnection.CheckConnectivity(() => {
      this.props.navigation.navigate("AmountScreen", {
        phone_no: mobile_no,
        operator: data.operator_name,
        operator_id: data.operator_id,
        operator_logo: data.operator_logo,
        apps_key: this.state.apps_key,
        token: this.props.navigation.getParam("token", ""),
      });
    });
  };

  render() {
    return (
      <GetMobileNumbers
        setPhoneNO={(phone_no) => {
          this.setState({ phone_no });
          this.next(phone_no);
        }}
        isValidNo={this.state.isValidNo}
        moveTo={() => {
          ConfirmConnection.CheckConnectivity(() => {
            this.props.navigation.navigate("AmountScreen", {
              phone_no: this.state.phone_no,
              operator: this.state.operator,
              operator_id: this.state.operator_id,
              operator_logo: this.state.operator_logo,
              apps_key: this.state.apps_key,
              token: this.props.navigation.getParam("token", ""),
            });
          });
        }}
        mobileNOAndPrefix={(mobile_no, prefix) =>
          this.setOperator(mobile_no, prefix)
        }
      />
    );
  }
}
