import React from "react";
import { Ionicons } from "@expo/vector-icons";
import ConfirmModal from "../../components/ConfirmModal";
import SuccessModal from "../../components/SuccessModal";
import DefaultScreen from "../../components/DefaultScreen";
import AppInput from "../../components/AppInput";
import ServiceAmountChargePrev from "../../components/ServiceAmountChargePrev";
import ServiceHeader from "../../components/ServiceHeader";
import ServiceType from "../../components/ServiceType";
import { StyleSheet, View, Platform, Dimensions } from "react-native";
import axios from "axios";
import GLOBAL from "../../globalState";
import GLOBAL_FUNCTION from "../../globalFunctions";
import ConfirmConnection from "../../ConfirmNetConnection";
const { width } = Dimensions.get("window");

export default class RechargePinScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      phone_no: this.props.navigation.getParam("phone_no", "01571774573"),
      operator: this.props.navigation.getParam("operator", "TT"),
      operator_id: this.props.navigation.getParam("operator_id", "1"),
      amount: this.props.navigation.getParam("amount", ""),
      contactName: this.props.navigation.getParam("contactName", ""),
      type: "PREPAID",
      trxid: "",
      datetime: "",
      isModalVisible: false,
      isSuccessModalVisible: false,
      isLoading: false,
      pin: "",
      userBalance: GLOBAL.userBalance,
    };
  }

  static navigationOptions = ({ navigation }) => ({
    headerLeft: (
      <Ionicons
        name={Platform.OS === "ios" ? "ios-arrow-back" : "md-arrow-back"}
        size={Platform.OS === "ios" ? 35 : 24}
        color={"#fff"}
        style={
          Platform.OS === "ios"
            ? { marginBottom: -4, width: 25, marginLeft: 9 }
            : { marginBottom: -4, width: 25, marginLeft: 20 }
        }
        onPress={() => {
          navigation.goBack();
        }}
      />
    ),
    title: "Mobile Recharge",
    headerTintColor: "#fff",
    headerStyle: {
      backgroundColor: "#00cec9",
    },
    headerTitleStyle: {
      color: "#fff",
    },
  });

  toggleModal = () => {
    this.setState({ isModalVisible: !this.state.isModalVisible });
  };
  toggleSuccessModal = () => {
    this.setState({ isSuccessModalVisible: !this.state.isSuccessModalVisible });
  };

  getRecharge = async () => {
    const connectionStatus = await ConfirmConnection.CheckNetConnection();
    if (connectionStatus) {
      this.setState({ isLoading: !this.state.isLoading });

      var allData = {
        apps_key: GLOBAL.rechargeScreen.apps_key,
        token: GLOBAL.rechargeScreen.token,
        phone_no: this.state.phone_no,
        amount: this.state.amount,
        type: this.state.type,
        operator: GLOBAL.operator,
        pin: this.state.pin,
        service_id: GLOBAL.service_id,
      };

      axios
        .post(`https://${GLOBAL.appUrl}/api/requestSubmit`, allData)
        .then((res) => {
          if (res.data.status == 1) {
            this.toggleModal();
            GLOBAL.userBalance = res.data.balance;
            //this.forceUpdate();
            //console.log(res.data.balance);
            this.setState({
              isLoading: !this.state.isLoading,
              trxid: res.data.trxid,
              datetime: res.data.datetime,
            });
            this.toggleSuccessModal();
          } else if (res.data.status == 50) {
            GLOBAL_FUNCTION.logOut(this.props);
          } else if (res.data.status == 40) {
            this.setState({ isLoading: !this.state.isLoading });
            alert(res.data.message);
          }
        });
    }
  };

  render() {
    return (
      <DefaultScreen
        gridFlex={true}
        screenData={
          <View>
            <ConfirmModal
              toggleModal={this.toggleModal}
              confirmAction={this.getRecharge}
              isModalVisible={this.state.isModalVisible}
              confirmTitle={"Mobile Recharge"}
              stateData={this.state}
              data={[
                {
                  text: "TOTAL",
                  value: this.state.amount,
                  rightBorder: 1,
                  bottomBorder: 0,
                },
                {
                  text: "OPERATOR",
                  value: GLOBAL.operator,
                  rightBorder: 0,
                  bottomBorder: 0,
                },
                {
                  text: "TYPE",
                  value: this.state.type,
                  rightBorder: 1,
                  bottomBorder: 1,
                },
                {
                  text: "NEW BALANCE",
                  value: Number(GLOBAL.userBalance) - Number(this.state.amount),
                  rightBorder: 0,
                  bottomBorder: 1,
                },
              ]}
            />

            <SuccessModal
              toggleModal={() => {
                this.toggleSuccessModal();
                GLOBAL_FUNCTION.backToHome(this.props);
              }}
              confirmTitle={"Recharge"}
              isSuccessModalVisible={this.state.isSuccessModalVisible}
              stateData={this.state}
              data={[
                {
                  text: "TRANS ID",
                  value: this.state.trxid,
                  rightBorder: 1,
                  bottomBorder: 0,
                },
                {
                  text: "REQUEST TIME",
                  value: this.state.datetime,
                  rightBorder: 0,
                  bottomBorder: 0,
                },
                {
                  text: "TOTAL",
                  value: this.state.amount,
                  rightBorder: 1,
                  bottomBorder: 0,
                },
                {
                  text: "OPERATOR",
                  value: GLOBAL.operator,
                  rightBorder: 0,
                  bottomBorder: 0,
                },
                {
                  text: "TYPE",
                  value: this.state.type,
                  rightBorder: 1,
                  bottomBorder: 1,
                },
                {
                  text: "NEW BALANCE",
                  value: Number(GLOBAL.userBalance),
                  rightBorder: 0,
                  bottomBorder: 1,
                },
              ]}
            />

            <ServiceHeader
              title={"FOR"}
              imgHeightWidth={{ width: width * 0.12, height: width * 0.12 }}
              centerData={this.state.phone_no}
              imgName={{ uri: `https://${GLOBAL.operatorLogo}` }}
            />
            {/* <View style={styles.sceenHeader}>
              <View style={{ justifyContent: "center" }}>
                <Text>Phone No:</Text>
              </View>

              <View style={{ justifyContent: "center" }}>
                <Text style={{ fontSize: width * 0.055 }}>
                  {this.state.phone_no}{" "}
                </Text>
              </View>
              <View style={{ justifyContent: "center" }}>
                <Image
                  style={{ width: width * 0.12, height: width * 0.12 }}
                  source={{ uri: `https://${GLOBAL.operatorLogo}` }}
                />
              </View>
            </View> */}

            <ServiceAmountChargePrev
              amount={this.state.amount}
              charge={"0.00"}
              total={this.state.amount}
            />

            <ServiceType
              typeOne={"PREPAID"}
              typeTwo={"POSTPAID"}
              defaultType={this.state.type}
              selectedType={(type) => this.setState({ type: type })}
            />

            <AppInput
              placeHolder={"Enter PIN"}
              placeholderTextColor={"#bdc3c7"}
              keyboardType={"decimal-pad"}
              leftIcon={"lock"}
              leftIconColor={"#00CDC8"}
              rightIconColor={"#00CDC8"}
              autoFocus={true}
              inputValue={(inputValue) => this.setState({ pin: inputValue })}
              next={this.toggleModal}
              secureTextEntry={true}
            />
          </View>
        }
      />
    );
  }
}

const styles = StyleSheet.create({});
