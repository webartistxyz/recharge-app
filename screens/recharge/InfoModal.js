import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Modal,
  TouchableOpacity,
  Dimensions
} from "react-native";
import { Ionicons } from "@expo/vector-icons";
const { width } = Dimensions.get("window");

export default class InfoModal extends React.Component {
  render() {
    return (
      <Modal
        animationType="none"
        transparent={true}
        visible={this.props.isInfoModal}
        onRequestClose={() => {
          this.props.toggleModal();
        }}
      >
        <View
          style={{
            flex: 1,
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: "#00000080"
          }}
        >
          <View
            style={{
              width: "91%",
              height: "95%",
              backgroundColor: "#fff",
              borderRadius: 1
            }}
          >
            <View
              style={{
                paddingTop: width * 0.03,
                paddingBottom: width * 0.03,
                paddingRight: width * 0.058,
                paddingLeft: width * 0.058
              }}
            >
              <View>
                <TouchableOpacity onPress={this.props.toggleModal}>
                  <Ionicons
                    name="md-close"
                    size={width * 0.095}
                    color="#00CDC8"
                  />
                </TouchableOpacity>
              </View>

              <View style={styles.confirmText}>
                <View style={styles.infoIcon}>
                  <Ionicons
                    name="md-information-circle"
                    size={width * 0.24}
                    color="#00CDC8"
                  />
                </View>
              </View>
            </View>

            <View style={styles.rechargeInfoContainer}>
              <Text style={{ fontSize: width * 0.06, textAlign: "center" }}>
                Offers shown are declared by your mobile operator, and Netoch is
                only acting as a payment service provider. To know offer details
                and your eligibility for any specific offer, please contact the
                concerned mobile operator
              </Text>
            </View>

            <TouchableOpacity
              style={styles.backButton}
              activeOpacity={1}
              onPress={this.props.toggleModal}
            >
              <View style={styles.backButtonText}>
                <Text style={{ color: "#fff", fontSize: width * 0.06 }}>
                  GOT IT
                </Text>
              </View>
              <View style={styles.backButtonSign}>
                <Ionicons
                  name="md-arrow-forward"
                  size={width * 0.065}
                  color="#fff"
                />
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  confirmText: {
    justifyContent: "center",
    alignItems: "center",
    paddingTop: width * 0.035
  },
  infoIcon: {
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#EEF1F1",
    padding: width * 0.01,
    height: width * 0.27,
    width: width * 0.27,
    borderRadius: 100
  },
  rechargeInfoContainer: {
    justifyContent: "center",
    alignItems: "center",
    padding: width * 0.09
  },
  rechargeInfo: {
    justifyContent: "center",
    alignItems: "center",
    borderBottomWidth: 1,
    borderColor: "#DAE0E2",
    width: "50%",
    height: width * 0.3
  },
  infoText: {
    fontSize: width * 0.033,
    fontWeight: "bold",
    color: "#00cec9"
  },
  backButton: {
    position: "absolute",
    bottom: 0,
    paddingLeft: width * 0.03,
    paddingRight: width * 0.03,
    paddingTop: width * 0.06,
    paddingBottom: width * 0.06,
    backgroundColor: "#00cec9",
    justifyContent: "center",
    flexDirection: "row"
  },
  backButtonText: {
    justifyContent: "center",
    flex: width * 0.035
  },
  backButtonSign: {
    justifyContent: "center",
    flex: 1
  }
});
