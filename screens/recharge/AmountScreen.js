import React from "react";
import AmountTab from "../../components/ServiceAmount";
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  Image,
  TouchableOpacity,
  ScrollView,
  Platform,
} from "react-native";
import DefaultScreen from "../../components/DefaultScreen";
import LoderModal from "../../components/LoaderModal";
import AvailAbleBalance from "../../components/AvailAbleBalance";
import { Container, Tab, Tabs } from "native-base";
import { Ionicons } from "@expo/vector-icons";
import axios from "axios";
import ConfirmConnection from "../../ConfirmNetConnection";
import InfoModal from "./InfoModal";
import GLOBAL from "../../globalState";
const { width } = Dimensions.get("window");
const leftData = [
  {
    title: "10",
    value: false,
  },
  {
    title: "20",
    value: false,
  },
  {
    title: "30",
    value: false,
  },
  {
    title: "50",
    value: false,
  },
];

export default class AmountScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      phone_no: this.props.navigation.getParam("phone_no", "01571774573"),
      operator: this.props.navigation.getParam("operator", "TT"),
      operator_id: this.props.navigation.getParam("operator_id", ""),
      operator_logo: this.props.navigation.getParam("operator_logo", ""),
      contactName: this.props.navigation.getParam("contactName", ""),
      isLoading: false,
      isInfoModal: false,
    };
  }

  static navigationOptions = ({ navigation }) => ({
    headerLeft: (
      <Ionicons
        name={Platform.OS === "ios" ? "ios-arrow-back" : "md-arrow-back"}
        size={Platform.OS === "ios" ? 35 : 24}
        color={"#fff"}
        style={
          Platform.OS === "ios"
            ? { marginBottom: -4, width: 25, marginLeft: 9 }
            : { marginBottom: -4, width: 25, marginLeft: 20 }
        }
        onPress={() => {
          navigation.goBack();
        }}
      />
    ),
    title: "Mobile Recharge",
    headerTintColor: "#fff",
    headerStyle: {
      backgroundColor: "#00cec9",
    },
    headerTitleStyle: {
      color: "#fff",
    },
  });

  async componentDidMount() {
    const connectionStatus = await ConfirmConnection.CheckNetConnection();
    if (connectionStatus) this._getOffersAndTypes();
  }

  _getOffersAndTypes = () => {
    this.setState({ isLoading: !this.state.isLoading });
    axios
      .post(`https://${GLOBAL.appUrl}/api/get_offers_by_op_id_and_type`, {
        operator_id: this.props.navigation.getParam("operator_id", ""),
      })
      .then((res) => {
        // /console.log(res.data);
        GLOBAL.offer_types = res.data;
        this.setState({ isLoading: !this.state.isLoading });
      });
  };

  _moveToRechargePin = (amount) => {
    this.props.navigation.navigate("RechargePin", {
      phone_no: this.state.phone_no,
      operator: this.state.operator,
      operator_id: this.state.operator_id,
      operator_logo: this.state.operator_logo,
      contactName: this.state.contactName,
      apps_key: this.props.navigation.getParam("apps_key", ""),
      token: this.props.navigation.getParam("token", ""),
      amount: amount,
    });
  };

  render() {
    return (
      <DefaultScreen
        screenData={
          <View>
            <LoderModal isLoading={this.state.isLoading} />

            <InfoModal
              toggleModal={() => {
                this.setState({ isInfoModal: !this.state.isInfoModal });
              }}
              isInfoModal={this.state.isInfoModal}
            />

            <View style={styles.sceenHeader}>
              <View
                style={{
                  flex: 3,
                  justifyContent: "center",
                  paddingLeft: width * 0.03,
                }}
              >
                <Text style={{ fontSize: width * 0.03, color: "#00cec9" }}>
                  FOR
                </Text>
              </View>

              <View style={{ flex: 5, justifyContent: "center" }}>
                <Text style={{ fontSize: width * 0.055 }}>
                  {this.state.phone_no}{" "}
                </Text>
              </View>

              <View style={{ flex: 1, justifyContent: "center" }}>
                <Image
                  style={{ width: width * 0.093, height: width * 0.093 }}
                  source={{ uri: `https://${GLOBAL.operatorLogo}` }}
                />
              </View>

              <TouchableOpacity
                onPress={() => {
                  this.props.navigation.navigate("Operator");
                }}
                style={{
                  flex: 1,
                  justifyContent: "flex-start",
                  paddingLeft: width * 0.013,
                }}
              >
                <View
                  style={{
                    justifyContent: "center",
                    alignItems: "center",
                    backgroundColor: "#EEF1F1",
                    padding: width * 0.013,
                    height: width * 0.09,
                    width: width * 0.09,
                    borderRadius: 100,
                  }}
                >
                  <Ionicons
                    name="md-create"
                    size={width * 0.035}
                    color="#00CEC9"
                  />
                </View>
              </TouchableOpacity>
            </View>
            {!this.state.isLoading ? (
              <Container>
                <Tabs
                  tabBarUnderlineStyle={{
                    backgroundColor: "#00cec9",
                    height: 3,
                  }}
                >
                  <Tab
                    textStyle={{ color: "#636e72" }}
                    tabStyle={{ backgroundColor: "#d2dae2" }}
                    activeTabStyle={{ backgroundColor: "#fff" }}
                    activeTextStyle={{ color: "#1e272e" }}
                    heading="Amount"
                  >
                    <AmountTab
                      moveToPin={(data) => this._moveToRechargePin(data.amount)}
                      inputValue={(amount) => {}}
                      selectedLeftData={(data) => {}}
                      leftData={leftData}
                    />
                    <AvailAbleBalance />
                  </Tab>

                  {GLOBAL.offer_types.map((type, key) => (
                    <Tab
                      textStyle={{ color: "#636e72" }}
                      key={key}
                      tabStyle={{ backgroundColor: "#d2dae2" }}
                      activeTabStyle={{ backgroundColor: "#fff" }}
                      activeTextStyle={{ color: "#1e272e" }}
                      heading={type.name}
                    >
                      <TouchableOpacity
                        onPress={() => {
                          this.setState({
                            isInfoModal: !this.state.isInfoModal,
                          });
                        }}
                        style={{ borderBottomWidth: 1, borderColor: "#00cec9" }}
                      >
                        <View
                          style={{
                            flexDirection: "row",
                            padding: width * 0.03,
                          }}
                        >
                          <View
                            style={{
                              justifyContent: "center",
                              alignItems: "center",
                              paddingEnd: width * 0.03,
                            }}
                          >
                            <Text
                              style={{
                                color: "#00CDC8",
                                fontSize: width * 0.03,
                                fontWeight: "bold",
                              }}
                            >
                              IMPORTANT INFORMATION
                            </Text>
                          </View>
                          <View
                            style={{
                              justifyContent: "center",
                              alignItems: "center",
                              backgroundColor: "#EEF1F1",
                              padding: width * 0.01,
                              height: width * 0.09,
                              width: width * 0.09,
                              borderRadius: 40,
                            }}
                          >
                            <Ionicons
                              name="md-information-circle"
                              size={width * 0.06}
                              color="#00CDC8"
                            />
                          </View>
                        </View>
                      </TouchableOpacity>

                      <ScrollView showsVerticalScrollIndicator={false}>
                        {type.offers.map((offer, key) => (
                          <TouchableOpacity
                            key={key}
                            onPress={() =>
                              this._moveToRechargePin(offer.offer_price)
                            }
                            style={{
                              width: width * 0.93,
                              borderBottomWidth: 1,
                              borderColor: "#DAE0E2",
                            }}
                          >
                            {offer.spacial_offer == "1" ? (
                              <View
                                style={{
                                  width: width * 0.25,
                                  backgroundColor: "#05B0AC",
                                  borderBottomEndRadius: 3,
                                }}
                              >
                                <View style={{ paddingLeft: width * 0.01 }}>
                                  <Text
                                    style={{
                                      fontSize: width * 0.027,
                                      color: "#fff",
                                      fontWeight: "bold",
                                    }}
                                  >
                                    ★SPECIAL OFFER
                                  </Text>
                                </View>
                              </View>
                            ) : (
                              <View></View>
                            )}

                            <View
                              style={{
                                flexDirection: "row",
                                padding: width * 0.03,
                              }}
                            >
                              <View
                                style={{
                                  flexDirection: "column",
                                  width: width * 0.4,
                                  justifyContent: "center",
                                  alignItems: "center",
                                }}
                              >
                                <Text
                                  style={{
                                    fontWeight: "bold",
                                    color: "#00CDC8",
                                  }}
                                >
                                  {offer.offer_details}
                                </Text>
                              </View>
                              <View
                                style={{
                                  flexDirection: "column",
                                  width: width * 0.23,
                                  justifyContent: "center",
                                  alignItems: "center",
                                }}
                              >
                                <Text
                                  style={{
                                    fontWeight: "bold",
                                    color: "#7f8c8d",
                                  }}
                                >
                                  {offer.duration}
                                </Text>
                              </View>
                              <View
                                style={{
                                  width: width * 0.23,
                                  justifyContent: "center",
                                  alignItems: "flex-end",
                                }}
                              >
                                <View
                                  style={{
                                    flexDirection: "column",
                                    justifyContent: "center",
                                    alignItems: "center",
                                    backgroundColor: "#EEF1F1",
                                    padding: width * 0.01,
                                    height: width * 0.095,
                                    width: width * 0.21,
                                    borderRadius: 40,
                                  }}
                                >
                                  <Text
                                    style={{
                                      color: "#00CDC8",
                                      fontWeight: "bold",
                                    }}
                                  >
                                    <Text>৳</Text>
                                    <Text>{offer.offer_price}</Text>
                                  </Text>
                                </View>
                              </View>
                            </View>
                          </TouchableOpacity>
                        ))}
                      </ScrollView>
                    </Tab>
                  ))}
                </Tabs>
              </Container>
            ) : (
              <View></View>
            )}
          </View>
        }
      />
    );
  }
}

const styles = StyleSheet.create({
  sceenHeader: {
    flexDirection: "row",
    padding: width * 0.03,
    height: width * 0.2,
    width: "100%",
  },
});
