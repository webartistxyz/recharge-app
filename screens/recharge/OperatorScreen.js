import React from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  TouchableOpacity,
  Dimensions,
  Platform,
} from "react-native";
import { Ionicons } from "@expo/vector-icons";
import GLOBAL from "../../globalState";
import GLOBAL_FUNCTION from "../../globalFunctions";
import LoderModal from "../../components/LoaderModal";
import axios from "axios";
import ConfirmConnection from "../../ConfirmNetConnection";
const { width } = Dimensions.get("window");

export default class OperatorScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      services: this.props.navigation.getParam("userRole", []),
      apps_key: this.props.navigation.getParam("apps_key", ""),
      isLoading: false,
    };
  }

  static navigationOptions = ({ navigation }) => ({
    headerLeft: (
      <Ionicons
        name={Platform.OS === "ios" ? "ios-arrow-back" : "md-arrow-back"}
        size={Platform.OS === "ios" ? 35 : 24}
        color={"#fff"}
        style={
          Platform.OS === "ios"
            ? { marginBottom: -4, width: 25, marginLeft: 9 }
            : { marginBottom: -4, width: 25, marginLeft: 20 }
        }
        onPress={() => {
          navigation.goBack();
        }}
      />
    ),
    title: "Select Operator",
    headerStyle: {
      backgroundColor: "#00cec9",
    },
    headerTintColor: "#fff",
    headerTitleStyle: {
      fontWeight: "bold",
    },
  });

  _getOffersAndTypes = (item) => {
    this.setState({ isLoading: !this.state.isLoading });
    axios
      .post(`https://${GLOBAL.appUrl}/api/get_offers_by_op_id_and_type`, {
        operator_id: item.id,
      })
      .then((res) => {
        //console.log(res.data);
        GLOBAL.offer_types = res.data;
        GLOBAL.operator = item.name;
        GLOBAL.operatorLogo = item.logo;
        GLOBAL.operator_id = item.id;

        this.props.navigation.navigate("AmountScreen", {
          operator_id: item.id,
          operator: item.name,
          operator_logo: item.logo,
          operator_code: item.code,
        });
        this.setState({ isLoading: !this.state.isLoading });
      });
  };

  render() {
    const operators = GLOBAL.operators;
    return (
      <View style={styles.container}>
        <View style={styles.gridContainer}>
          <LoderModal isLoading={this.state.isLoading} />

          <ScrollView showsVerticalScrollIndicator={false}>
            <View style={styles.row}>
              {operators.map((item, key) => (
                <TouchableOpacity
                  style={[
                    styles.item,
                    {
                      borderRightWidth: GLOBAL_FUNCTION.checkIndexIsEven(key)
                        ? 1
                        : 0,
                    },
                  ]}
                  key={key}
                  onPress={() => {
                    ConfirmConnection.CheckConnectivity(() => {
                      this._getOffersAndTypes(item);
                    });
                  }}
                >
                  <Image
                    style={{ width: width * 0.24, height: width * 0.24 }}
                    source={{ uri: `https://${item.logo}` }}
                  />
                  <View style={{ paddingTop: "10%" }}>
                    <Text>{item.name}</Text>
                  </View>
                </TouchableOpacity>
              ))}
            </View>
          </ScrollView>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#00cec9",
    alignItems: "center",
  },
  gridContainer: {
    flex: 1,
    alignItems: "center",
    marginTop: width * 0.03,
    marginBottom: width * 0.03,
    backgroundColor: "#fff",
    height: "95%",
    width: "95%",
    borderRadius: 3,
  },
  row: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    flexWrap: "wrap",
  },
  item: {
    justifyContent: "center",
    alignItems: "center",
    borderBottomWidth: 1,
    borderColor: "#DAE0E2",
    width: "50%",
    height: width * 0.45,
  },
});
