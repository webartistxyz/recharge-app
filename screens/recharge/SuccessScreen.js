import React from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  Platform,
} from "react-native";
import { Ionicons } from "@expo/vector-icons";

export default class SuccessScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };

  checkIndexIsEven = (n) => {
    //console.log(n)
    return n % 2 == 0;
  };

  render() {
    const { width, height } = Dimensions.get("window");
    const modalWidth = width * 0.95;
    const modalHeigth = height * 0.93;
    return (
      <View
        style={{
          flex: 1,
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
          backgroundColor: "#00000080",
        }}
      >
        <View
          style={{
            width: modalWidth,
            height: modalHeigth,
            backgroundColor: "#fff",
            borderRadius: 1,
            marginTop: 20,
          }}
        >
          <View
            style={{
              paddingTop: 10,
              paddingBottom: 10,
              paddingRight: 18,
              paddingLeft: 18,
            }}
          >
            <View style={styles.confirmText}>
              <Text>
                <Text style={{ fontSize: 25, color: "#00cec9" }}>Your </Text>
                <Text
                  style={{ fontSize: 25, fontWeight: "bold", color: "#00cec9" }}
                >
                  Recharge
                </Text>
                <Text style={{ fontSize: 25, color: "#00cec9" }}> is </Text>
              </Text>
              <Text
                style={{ fontSize: 20, fontWeight: "bold", color: "#27ae60" }}
              >
                successful
              </Text>
            </View>

            <View style={styles.confirmPhoneNO}>
              <Text style={{ fontSize: 35 }}>{this.props.data.phone_no}</Text>
            </View>
          </View>

          <View style={styles.rechargeInfoContainer}>
            <View
              style={[
                styles.rechargeInfo,
                { borderRightWidth: this.checkIndexIsEven(0) ? 1 : 0 },
              ]}
            >
              <Text style={styles.infoText}>TRANS ID</Text>
              <Text>{this.props.data.trxid}</Text>
            </View>
            <View
              style={[
                styles.rechargeInfo,
                { borderRightWidth: this.checkIndexIsEven(1) ? 1 : 0 },
              ]}
            >
              <Text style={styles.infoText}>REQUEST TIME</Text>
              <Text>{this.props.data.datetime}</Text>
            </View>

            <View
              style={[
                styles.rechargeInfo,
                {
                  borderRightWidth: this.checkIndexIsEven(0) ? 1 : 0,
                  borderTopWidth: 1,
                  borderColor: "#DAE0E2",
                },
              ]}
            >
              <Text style={styles.infoText}>TOTAL</Text>
              <Text>{this.props.data.amount}</Text>
            </View>
            <View
              style={[
                styles.rechargeInfo,
                {
                  borderRightWidth: this.checkIndexIsEven(1) ? 1 : 0,
                  borderTopWidth: 1,
                  borderColor: "#DAE0E2",
                },
              ]}
            >
              <Text style={styles.infoText}>OPERATOR</Text>
              <Text>
                {GLOBAL_FUNCTIONS.convertToUppercase(this.props.data.operator)}
              </Text>
            </View>

            <View
              style={[
                styles.rechargeInfo,
                { borderRightWidth: this.checkIndexIsEven(0) ? 1 : 0 },
              ]}
            >
              <Text style={styles.infoText}>TYPE</Text>
              <Text>
                {GLOBAL_FUNCTIONS.convertToUppercase(this.props.data.type)}
              </Text>
            </View>
            <View
              style={[
                styles.rechargeInfo,
                { borderRightWidth: this.checkIndexIsEven(1) ? 1 : 0 },
              ]}
            >
              <Text style={styles.infoText}>NEW BALANCE</Text>
              <Text>
                {this.props.data.userBalance - this.props.data.amount}
              </Text>
            </View>
          </View>

          <TouchableOpacity
            style={styles.backButton}
            onPress={() => {
              GLOBAL_FUNCTIONS.backToHome(this.props);
            }}
          >
            <View style={styles.backButtonText}>
              <Text style={{ color: "#fff", fontSize: 20 }}>Back to Home</Text>
            </View>
            <View style={styles.backButtonSign}>
              <Ionicons name="md-arrow-forward" size={25} color="#fff" />
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  confirmText: {
    justifyContent: "center",
    alignItems: "center",
    paddingTop: 15,
  },
  confirmPhoneNO: {
    justifyContent: "center",
    alignItems: "center",
    paddingTop: 10,
    paddingBottom: 10,
  },
  rechargeInfoContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    flexWrap: "wrap",
  },
  rechargeInfo: {
    justifyContent: "center",
    alignItems: "center",
    borderBottomWidth: 1,
    borderColor: "#DAE0E2",
    width: "50%",
    height: 100,
  },
  infoText: {
    fontSize: 13,
    fontWeight: "bold",
    color: "#00cec9",
  },
  backButton: {
    position: "absolute",
    bottom: 0,
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 20,
    paddingBottom: 20,
    backgroundColor: "#00cec9",
    justifyContent: "center",
    flexDirection: "row",
  },
  backButtonText: {
    justifyContent: "center",
    flex: 15,
  },
  backButtonSign: {
    justifyContent: "center",
    flex: 1,
  },
});
