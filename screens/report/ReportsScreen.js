import React from "react";
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  Platform,
  TouchableOpacity,
  Dimensions,
} from "react-native";
import DefaultScreen from "../../components/DefaultScreen";
import ConfirmConnection from "../../ConfirmNetConnection";
import { Ionicons } from "@expo/vector-icons";
const { width, height } = Dimensions.get("window");

export default class ReportsScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  static navigationOptions = ({ navigation }) => ({
    headerLeft: (
      <Ionicons
        name={Platform.OS === "ios" ? "ios-arrow-back" : "md-arrow-back"}
        size={Platform.OS === "ios" ? 35 : 24}
        color={"#fff"}
        style={
          Platform.OS === "ios"
            ? { marginBottom: -4, width: 25, marginLeft: 9 }
            : { marginBottom: -4, width: 25, marginLeft: 20 }
        }
        onPress={() => {
          navigation.goBack();
        }}
      />
    ),
    title: "Reports",
    headerStyle: {
      backgroundColor: "#4050B5",
    },
    headerTintColor: "#fff",
    headerTitleStyle: {
      fontWeight: "bold",
    },
  });

  reportData(iconName, reportName, nav) {
    return (
      <TouchableOpacity
        onPress={async () => {
          const connectionStatus = await ConfirmConnection.CheckNetConnection();
          if (connectionStatus) this.props.navigation.navigate(nav);
        }}
        style={styles.item}
      >
        <Ionicons name={iconName} size={width * 0.08} color="#4050B5" />
        <Text style={{ fontSize: 12 }}>{reportName}</Text>
      </TouchableOpacity>
    );
  }

  render() {
    return (
      <DefaultScreen
        screenBackgroundColor={"#4050B5"}
        screenData={
          <View>
            {/* <LoderModal isLoading={this.state.isLoading} /> */}

            <ScrollView showsVerticalScrollIndicator={false}>
              <View style={styles.row}>
                {this.reportData("md-timer", "History", "History")}
                {this.reportData(
                  "md-battery-charging",
                  "Recharge",
                  "RechargeReport"
                )}
                {this.reportData(
                  "md-paper-plane",
                  "Transfer",
                  "TransferReport"
                )}
                {this.reportData("md-swap", "Transaction", "TransactionReport")}
              </View>
            </ScrollView>
          </View>
        }
      />
    );
  }
}

const styles = StyleSheet.create({
  row: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    flexWrap: "wrap",
  },
  item: {
    padding: width * 0.04,
    alignItems: "center",
  },
});
