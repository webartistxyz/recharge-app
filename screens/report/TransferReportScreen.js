import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Dimensions,
  FlatList,
  Platform,
  ActivityIndicator,
} from "react-native";
// import FilterModal from "../../components/Modal";
import FilterModal from "../../components/FilterModal";
import PreviewButton from "../../components/PreviewButton";
import HistoryContent from "../../components/HistoryContent";
import ConfirmConnection from "../../ConfirmNetConnection";
import ReportDetails from ".//ReportDetails";
import { Ionicons } from "@expo/vector-icons";
import moment from "moment";
import axios from "axios";
import GLOBAL from "../../globalState";
import { Content, Item, Picker, DatePicker, Button } from "native-base";
const { width, height } = Dimensions.get("window");

export default class HistoryReportScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      start_date: "",
      end_date: "",
      type: "SENT",
      member_id: "",
      isLoading: false,
      historys: [],
      tempHistorys: [],
      isModalVisible: false,
      isFilterModal: false,
      modalData: {},
      refreshing: false,
      paginator: {},
    };
  }

  static navigationOptions = ({ navigation }) => ({
    headerLeft: (
      <Ionicons
        name={Platform.OS === "ios" ? "ios-arrow-back" : "md-arrow-back"}
        size={Platform.OS === "ios" ? 35 : 24}
        color={"#fff"}
        style={
          Platform.OS === "ios"
            ? { marginBottom: -4, width: 25, marginLeft: 9 }
            : { marginBottom: -4, width: 25, marginLeft: 20 }
        }
        onPress={() => {
          navigation.goBack();
        }}
      />
    ),
    title: "Services Report",
    headerTintColor: "#fff",
    headerStyle: {
      backgroundColor: "#4050B5",
    },
    headerTitleStyle: {
      color: "#fff",
    },
  });

  async componentDidMount() {
    const connectionStatus = await ConfirmConnection.CheckNetConnection();
    if (connectionStatus) {
      this._getHistory(`https://${GLOBAL.appUrl}/api/balanceTransferList`);
    }
  }

  makePagination(data) {
    let paginator = {
      current_page: data.current_page,
      last_page: data.last_page,
      next_page_url: data.next_page_url,
      prev_page_url: data.prev_page_url,
    };
    this.setState({ paginator });
  }

  _getHistory = (url) => {
    this.setState({ isLoading: !this.state.isLoading });
    axios
      .post(url, {
        apps_key: GLOBAL.rechargeScreen.apps_key,
        token: GLOBAL.rechargeScreen.token,
        type: this.state.type,
        member_id: this.state.member_id,
        start_date: this.state.start_date,
        end_date: this.state.end_date,
      })
      .then((res) => {
        //console.log(res.data);
        this.setState(
          {
            isLoading: !this.state.isLoading,
            start_date: "",
            end_date: "",
            historys: [...this.state.historys, ...res.data.data],
            tempHistorys: [...this.state.historys, ...res.data.data],
          },
          () => this.makePagination(res.data)
        );
      });
  };

  refreshAndSearchHistory = () => {
    axios
      .post(`https://${GLOBAL.appUrl}/api/balanceTransferList`, {
        apps_key: GLOBAL.rechargeScreen.apps_key,
        token: GLOBAL.rechargeScreen.token,
        type: this.state.type,
        member_id: this.state.member_id,
        start_date: this.state.start_date,
        end_date: this.state.end_date,
      })
      .then((res) => {
        //console.log(res.data);
        this.setState(
          {
            isLoading: false,
            refreshing: false,
            start_date: "",
            end_date: "",
            historys: res.data.data,
            tempHistorys: res.data.data,
          },
          () => {
            this.makePagination(res.data);
          }
        );
      });
  };

  handleRefresh = () => {
    this.setState(
      {
        refreshing: !this.state.refreshing,
        member_id: "",
        type: "SENT",
        start_date: "",
        end_date: "",
        historys: [],
        tempHistorys: [],
      },
      () => {
        this.refreshAndSearchHistory();
      }
    );
  };

  search_history = (url) => {
    this.setState({
      isLoading: !this.state.isLoading,
      historys: [],
      tempHistorys: [],
    });
    this.refreshAndSearchHistory();
  };

  toggleFilterModal = () => {
    this.setState({ isFilterModal: !this.state.isFilterModal });
  };

  renderFooter = () => {
    return (
      <View style={styles.footer}>
        {this.state.isLoading ? (
          <ActivityIndicator color="black" style={{ margin: 15 }} />
        ) : null}
      </View>
    );
  };

  render() {
    return (
      <View style={styles.container}>
        <View
          style={[
            styles.gridContainer,
            { height: height * 0.95, width: width * 0.93 },
          ]}
        >
          <ReportDetails
            toggleModal={() => {
              this.setState({ isModalVisible: !this.state.isModalVisible });
            }}
            isModalVisible={this.state.isModalVisible}
            modalData={this.state.modalData}
          />
          <FilterModal
            toggleModal={this.toggleFilterModal}
            isModalVisible={this.state.isFilterModal}
            setService={(service) => {}}
            setStatus={(status) => {}}
            setStartDate={(start_date) => this.setState({ start_date })}
            setEndDate={(end_date) => this.setState({ end_date })}
            search={(member_id) => this.setState({ member_id: member_id })}
            buttonPressed={() => {
              this.toggleFilterModal();
              this.search_history();
            }}
            iconColor={"#4050B5"}
            type={this.state.type}
            typeSelected={(type) => this.setState({ type })}
          />

          <PreviewButton
            buttonPressed={this.toggleFilterModal}
            iconColor={"#4050B5"}
            buttonTitle={"FILTER REPORT"}
          />

          <FlatList
            data={this.state.historys}
            initialNumToRender={30}
            maxToRenderPerBatch={30}
            updateCellsBatchingPeriod={30}
            windowSize={30}
            refreshing={this.state.refreshing}
            onRefresh={this.handleRefresh}
            onEndReached={() => {
              if (this.state.paginator.next_page_url && !this.state.isLoading)
                this._getHistory(this.state.paginator.next_page_url);
            }}
            keyExtractor={(item) => item.id.toString()}
            renderItem={({ item }) => {
              return (
                <HistoryContent
                  pressed={() => {
                    this.setState({
                      isModalVisible: !this.state.isModalVisible,
                      modalData: item,
                    });
                  }}
                  serviceImg={item.member_image}
                  serviceName={item.member_name}
                  serviceAcNo={item.member_mobile}
                  // costOrTransectionTitle="Cost: "
                  // costOrTransection={item.cost}
                  statusColor={"black"}
                  statusTitle={"M.ID: "}
                  status={item.member_code}
                  requestDatetime={item.requestDatetime}
                  amountColor={
                    item.payment_type == "SENT" ? "#c0392b" : "green"
                  }
                  amountSign={item.payment_type == "SENT" ? "-" : "+"}
                  amount={item.amount}
                />
              );
            }}
            ListFooterComponent={this.renderFooter}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#4050B5",
    alignItems: "center",
  },
  gridContainer: {
    flex: 1,
    marginTop: width * 0.03,
    marginBottom: width * 0.03,
    backgroundColor: "#fff",
    borderRadius: 3,
  },
  footer: {
    padding: 10,
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "row",
  },
});
