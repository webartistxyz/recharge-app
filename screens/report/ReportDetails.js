import React from "react";
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Dimensions,
} from "react-native";
import AppModal from "../../components/Modal";
import GLOBAL_FUNCTIONS from "../../globalFunctions";
const { width, height } = Dimensions.get("window");

export default class HistoryDetailsModal extends React.Component {
  modalData(text, value, rightBorder, topBorder = 0) {
    return (
      <View
        style={[
          styles.modalInfo,
          {
            borderRightWidth: rightBorder,
            borderTopWidth: topBorder,
            borderColor: "#DAE0E2",
          },
        ]}
      >
        <Text style={styles.infoText}>{text}</Text>
        <Text style={{ fontSize: width * 0.03 }}>{value}</Text>
      </View>
    );
  }

  _modalContent = () => {
    return (
      <View>
        <View style={{ paddingRight: width * 0.06, paddingLeft: width * 0.06 }}>
          <View style={styles.modalHeader}>
            <Text>
              <Text style={{ fontSize: width * 0.08, color: "#00cec9" }}>
                Transfer{" "}
              </Text>
              <Text
                style={{
                  fontSize: width * 0.08,
                  fontWeight: "bold",
                  color: "#00cec9",
                }}
              >
                Details of
              </Text>
            </Text>
          </View>

          <View style={styles.transId}>
            <Text style={{ fontSize: width * 0.09 }}>
              {this.props.modalData.member_mobile}
            </Text>
          </View>
        </View>

        <View style={styles.modalInfoContainer}>
          {this.modalData("DATE", this.props.modalData.paymentDate, 1, 1)}
          {this.modalData("TRX ID", this.props.modalData.trxid, 0, 1)}
          {this.modalData(
            "CHARGE",
            GLOBAL_FUNCTIONS.convertToUppercase(this.props.modalData.charge),
            1
          )}
          {this.modalData("AMOUNT", this.props.modalData.amount, 0)}
        </View>
        {this.props.modalData.description ? (
          <View
            style={{
              borderBottomColor: "#DAE0E2",
              borderBottomWidth: 1,
              padding: width * 0.03,
              alignItems: "center",
            }}
          >
            <Text>{this.props.modalData.description}</Text>
          </View>
        ) : null}

        <View
          style={{
            justifyContent: "center",
            alignItems: "center",
            paddingTop: 40,
          }}
        >
          <TouchableOpacity
            style={{
              justifyContent: "center",
              alignItems: "center",
              height: 70,
              width: 70,
              borderRadius: 100,
              backgroundColor: "#EEF1F1",
            }}
            onPress={this.props.toggleModal}
          >
            <Text style={{ color: "#00CDC8" }}>Close</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  render() {
    return (
      <AppModal
        toggleModal={this.props.toggleModal}
        isModalVisible={this.props.isModalVisible}
        modalContent={this._modalContent()}
      />
    );
  }
}

const styles = StyleSheet.create({
  modalHeader: {
    justifyContent: "center",
    alignItems: "center",
    paddingTop: height * 0.02,
  },
  transId: {
    justifyContent: "center",
    alignItems: "center",
    paddingTop: height * 0.02,
    paddingBottom: height * 0.02,
  },
  modalInfoContainer: {
    marginTop: height * 0.05,
    flexDirection: "row",
    justifyContent: "space-between",
    flexWrap: "wrap",
  },
  modalInfo: {
    justifyContent: "center",
    alignItems: "center",
    borderBottomWidth: 1,
    borderColor: "#DAE0E2",
    width: "50%",
    height: height * 0.12,
  },
  infoText: {
    fontSize: width * 0.04,
    fontWeight: "bold",
    color: "#00cec9",
  },
  backButton: {
    position: "absolute",
    bottom: 0,
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: height * 0.06,
    paddingBottom: height * 0.06,
    backgroundColor: "#00cec9",
    justifyContent: "center",
    flexDirection: "row",
  },
  backButtonText: {
    justifyContent: "center",
    flex: 15,
  },
  backButtonSign: {
    justifyContent: "center",
    flex: 1,
  },
});
