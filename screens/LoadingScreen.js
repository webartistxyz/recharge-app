import React from "react";
import { StyleSheet, View, ActivityIndicator } from "react-native";

export default class LoadingScreen extends React.Component {
  static navigationOptions = {
    title: "Loading",
    header: null
  };

  componentWillMount() {}

  render() {
    return (
      <View style={styles.container}>
        <ActivityIndicator size="large" color="#4050B5" />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  }
});
