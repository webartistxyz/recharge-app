import GLOBAL from "./globalState";
import axios from "axios";
import { NavigationActions, StackActions } from "react-navigation";
import { Alert } from "react-native";
module.exports = {
  checkIndexIsEven(n) {
    return n % 1 == 0;
  },
  convertToUppercase(data) {
    return data && typeof data == "string" ? data.toUpperCase() : data;
  },
  logOut(props) {
    GLOBAL.userBalance = "";
    GLOBAL.rechargeScreen = {
      mobile: "",
      userRole: "",
      apps_key: "",
      token: "",
    };
    const resetAction = StackActions.reset({
      index: 0,
      key: null,
      actions: [NavigationActions.navigate({ routeName: "UserName" })],
    });
    props.navigation.dispatch(resetAction);
  },
  saveMultyPartFormData(url, formData) {
    const formDataObj = new FormData();
    for (var key in formData) {
      if (formData.hasOwnProperty(key)) {
        formDataObj.append(key, formData[key] ? formData[key] : "");
      }
    }

    return new Promise((resolve, reject) => {
      axios({
        method: "post",
        url: url,
        config: {
          headers: {
            Accept: "application/json",
            "Content-Type": "multipart/form-data",
          },
        },
        data: formDataObj,
      })
        .then((response) => {
          resolve({ status: true, data: response.data });
        })
        .catch((error) => {
          console.log(error);
          reject({ status: false, error: error.response.data.errors });
        });
    });
  },

  backToHome(props) {
    const resetAction = StackActions.reset({
      index: 0,
      key: null,
      actions: [NavigationActions.navigate({ routeName: "Home" })],
    });
    props.navigation.dispatch(resetAction);
  },

  // forgotPinOrPass() {
  //   Alert.alert(
  //     "Forgot Pin",
  //     "Do you want to reset your pin?",
  //     [
  //       { text: "No", style: "cancel" },
  //       {
  //         text: "Yes",
  //         onPress: async () => {
  //           const connectionStatus = await ConfirmConnection.CheckNetConnection();
  //           if (connectionStatus) {
  //             try {
  //               this.setState({ isLoading: !this.state.isLoading });

  //               const confirm = await axios.post(
  //                 `https://${GLOBAL.appUrl}/api/forgot_all`,
  //                 {
  //                   member_id: GLOBAL.userId
  //                 }
  //               );
  //               if (confirm) {
  //                 logOut();
  //               }
  //             } catch (error) {
  //               alert(error);
  //               this.setState({ isLoading: !this.state.isLoading });
  //             }
  //           }
  //         }
  //       }
  //     ],
  //     { cancelable: false }
  //   );
  // },
  // logOut() {
  //   AsyncStorage.removeItem("user_data");
  //   GLOBAL_FUNCTIONS.logOut(this.props);
  // }
};
