import { Alert, Platform } from "react-native";
import NetInfo from "@react-native-community/netinfo";

class confirmNetConnection {
  CheckNetConnection() {
    return new Promise((resolve, reject) => {
      // For Android devices
      if (Platform.OS === "android")
        NetInfo.fetch().then((isConnected) => {
          if (isConnected) {
            resolve(true);
          } else {
            Alert.alert("You are offline!");
          }
        });
      // For iOS devices
      else
        NetInfo.isConnected.addEventListener(
          "connectionChange",
          this.handleFirstConnectivityChange
        );
    });
  }

  async CheckConnectivity(useFunction) {
    // For Android devices
    if (Platform.OS === "android")
      await NetInfo.fetch().then((isConnected) => {
        if (isConnected) {
          if (useFunction) useFunction();
        } else Alert.alert("You are offline!");
      });
    // For iOS devices
    else
      NetInfo.isConnected.addEventListener(
        "connectionChange",
        this.handleFirstConnectivityChange
      );
  }

  handleFirstConnectivityChange(isConnected) {
    NetInfo.isConnected.removeEventListener(
      "connectionChange",
      this.handleFirstConnectivityChange
    );

    if (isConnected === false) {
      Alert.alert("You are offline!");
    } else return true;
  }
}

export default ConfirmConnection = new confirmNetConnection();
