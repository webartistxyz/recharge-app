module.exports = {
  appUrl: "osesco.com",
  userId: "",
  userBalance: 0,
  userImage: "",
  nid_back: "",
  nid_front: "",
  nid_status: "",
  service_id: "",

  rechargeScreen: null, //object
  app_details: null, //object

  operator: "",
  operatorLogo: "",
  operator_id: "",
  operators: [],
  offer_types: [],

  //service
  imgName: "",
  serviceName: "",
  service_mob_no: "",

  //contact list
  contactList: [],

  //sms
  smsText: "",
  smsRecipients: [],
  smsRecipient: "",
  totalSms: "",
  smsType: "",
};
