import React from "react";
import ContactList from "./ContactList";
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  Modal,
  TouchableOpacity,
  TextInput,
  Dimensions,
} from "react-native";

import GLOBAL_FUNCTIONS from "../globalFunctions";
import { Input, Button } from "native-base";
import { Entypo } from "@expo/vector-icons";
import { Ionicons } from "@expo/vector-icons";
import GLOBAL from "../globalState";
const { width } = Dimensions.get("window");
let arrayholder = [];

export default class GetMobileNumbers extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      phone_no: "",
      hasContactPermissiion: null,
      contactList: [],
      contactName: "",
      phoneNoArr: [],
      isModalVisible: false,
      started: null,
    };
  }

  async componentDidMount() {
    this.setState(
      { contactList: JSON.parse(JSON.stringify(GLOBAL.contactList)) },
      () => (arrayholder = JSON.parse(JSON.stringify(GLOBAL.contactList)))
    );
  }

  formatNumber = (phone_no) => {
    let onlyDigitNumber = phone_no.replace(/[^\d]/g, "");
    //console.log(onlyDigitNumber);
    if (onlyDigitNumber.length > 11) {
      var prefix = onlyDigitNumber.substr(0, 2);

      if (prefix == 88) {
        var accurate_no = onlyDigitNumber.substr(2);
        if (accurate_no.length === 11) {
          var prefix = accurate_no.substr(0, 3);

          this.setState({ phone_no: accurate_no });
          this.props.mobileNOAndPrefix(accurate_no, prefix);
        }
      } else {
        if (!this.props.notValidate) {
          this.setState({ phone_no: "" });
          alert("Invalid Mobile Number");
        } else this.props.mobileNOAndPrefix(onlyDigitNumber, prefix);
      }
    } else if (
      onlyDigitNumber.length === 11 &&
      onlyDigitNumber.substr(0, 2) == "01"
    ) {
      var prefix = phone_no.substr(0, 3);

      this.setState({ phone_no: onlyDigitNumber });
      this.props.mobileNOAndPrefix(onlyDigitNumber, prefix);
    } else {
      if (!this.props.notValidate) {
        this.setState({ phone_no: "" });
        alert("Invalid Mobile Number");
      } else this.props.mobileNOAndPrefix(onlyDigitNumber, prefix);
    }
  };

  toggleModal = () => {
    this.setState({ isModalVisible: !this.state.isModalVisible });
  };

  filterPhoneNo = (contact) => {
    let numberObj = contact.phoneNumbers;
    if (typeof numberObj === "object") {
      if (numberObj.length > 1) {
        this.setState({
          contactName: contact.lastName
            ? contact.firstName + " " + contact.lastName
            : contact.firstName,
          phoneNoArr: contact.phoneNumbers,
        });
        this.toggleModal();
      } else {
        this.setState({
          contactName: contact.lastName
            ? contact.firstName + " " + contact.lastName
            : contact.firstName,
          phoneNoArr: contact.phoneNumbers,
        });
        this.formatNumber(numberObj[0].number);
      }
    }
  };

  searchQuery = (phone_no) => {
    var newData = [];

    if (phone_no.length && arrayholder.length) {
      if (!isNaN(phone_no)) {
        newData = arrayholder.filter((contactDetails) => {
          return typeof contactDetails.phoneNumbers === "object"
            ? Object.keys(contactDetails.phoneNumbers).some((key) => {
                //let string = String()
                let JsonString = JSON.stringify(
                  contactDetails.phoneNumbers[key]
                );
                let string = String(JsonString);
                let numberString = String(phone_no);

                //console.log(phone_no)
                return string.indexOf(numberString) > -1;
              })
            : "";
        });
        this.setState({
          contactList: newData,
        });
      }

      if (isNaN(phone_no)) {
        newData = arrayholder.filter((contactDetails) => {
          let string = String(
            GLOBAL_FUNCTIONS.convertToUppercase(contactDetails.firstName)
          );

          let numberString = String(
            GLOBAL_FUNCTIONS.convertToUppercase(phone_no)
          );

          //console.log(phone_no)
          return string.indexOf(numberString) > -1;
        });

        this.setState({
          contactList: newData,
        });
      }
    } else {
      this.setState({
        contactList: arrayholder,
      });
    }
  };

  goBackPrevScreen = () => {
    this.props.goBackPrvScreen();
    GLOBAL.smsRecipients = [];
  };

  render() {
    return (
      <View style={styles.container} behavior="padding" enabled>
        <Modal
          animationType="none"
          transparent={false}
          visible={this.state.isModalVisible}
          presentationStyle="formSheet"
          onRequestClose={this.toggleModal}
        >
          <View
            style={{
              flex: 1,
              flexDirection: "column",
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: "#00000080",
            }}
          >
            <View
              style={{
                width: width * 0.9,
                backgroundColor: "#fff",
                borderRadius: 5,
              }}
            >
              <View
                style={{
                  paddingTop: width * 0.03,
                  paddingBottom: width * 0.058,
                  paddingRight: width * 0.058,
                  paddingLeft: width * 0.058,
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <View style={{ marginBottom: width * 0.06 }}>
                  <Text style={{ fontSize: width * 0.09, fontWeight: "bold" }}>
                    {this.state.contactName}
                  </Text>
                </View>
                <View>
                  {this.state.phoneNoArr.map((item, key) => (
                    <Button
                      style={styles.button}
                      rounded
                      info
                      key={key}
                      onPress={() => {
                        this.formatNumber(item.number);
                        this.toggleModal();
                      }}
                    >
                      <Text
                        style={{
                          color: "#fff",
                          paddingLeft: width * 0.03,
                          paddingRight: width * 0.03,
                          fontSize: width * 0.06,
                        }}
                      >
                        {item.number}
                      </Text>
                    </Button>
                  ))}
                </View>
                <Button
                  style={styles.button}
                  full
                  danger
                  rounded
                  onPress={this.toggleModal}
                >
                  <Text style={styles.buttonText}>Cancel</Text>
                </Button>
              </View>
            </View>
          </View>
        </Modal>

        {!this.props.isSelectable ? (
          <View>
            <Text
              style={{
                fontSize: width * 0.036,
                color: "#BA2F16",
                paddingLeft: width * 0.05,
                paddingTop: width * 0.03,
              }}
            >
              Mobile No
            </Text>
            <View style={styles.inputContainer}>
              <View style={{ flex: 10 }}>
                <Input
                  autoCapitalize="none"
                  placeholder="01XXXXXXXXX"
                  autoFocus={true}
                  autoCorrect={false}
                  keyboardType="phone-pad"
                  value={this.state.phone_no}
                  onChangeText={(phone_no) => {
                    this.props.setPhoneNO(phone_no);
                    this.setState({ phone_no: phone_no });
                    this.searchQuery(phone_no);
                  }}
                />
              </View>
              <View style={{ flex: 1 }}>
                {this.props.isValidNo ? (
                  <TouchableOpacity
                    style={styles.nextButton}
                    onPress={this.props.moveTo}
                  >
                    <Entypo
                      name="arrow-right"
                      size={width * 0.09}
                      color="#B83227"
                    />
                  </TouchableOpacity>
                ) : (
                  <View style={styles.nextButton}>
                    <Entypo
                      name="arrow-right"
                      size={width * 0.09}
                      color="#bdc3c7"
                    />
                  </View>
                )}
              </View>
            </View>
          </View>
        ) : (
          <View>
            <View
              style={{
                paddingTop: width * 0.03,
                paddingLeft: width * 0.07,
                paddingRight: width * 0.07,
                flexDirection: "row",
                justifyContent: "space-between",
              }}
            >
              <TouchableOpacity onPress={this.props.goBackPrvScreen}>
                <Ionicons name="md-close" size={width * 0.07} color="#B83227" />
              </TouchableOpacity>
              <Text>
                {GLOBAL.smsRecipients.length
                  ? `Selected ${GLOBAL.smsRecipients.length} numbers`
                  : "Select Number"}
              </Text>
              {GLOBAL.smsRecipients.length ? (
                <TouchableOpacity onPress={this.props.nextScreen}>
                  <Ionicons
                    name="md-checkmark"
                    size={width * 0.07}
                    color="#B83227"
                  />
                </TouchableOpacity>
              ) : (
                <Ionicons
                  name="md-checkmark"
                  size={width * 0.07}
                  color="#bdc3c7"
                />
              )}
            </View>
            <View style={{ padding: width * 0.04 }}>
              <TextInput
                style={{
                  borderColor: "#BA2F16",
                  borderWidth: 1,
                  borderRadius: 30,
                  paddingTop: width * 0.01,
                  paddingBottom: width * 0.01,
                  paddingLeft: width * 0.03,
                }}
                autoCapitalize="none"
                placeholder="Search by number or name"
                autoFocus={false}
                autoCorrect={false}
                keyboardType="default"
                value={this.state.phone_no}
                onChangeText={(phone_no) => {
                  this.props.setPhoneNO(phone_no);
                  this.setState({ phone_no: phone_no });
                  this.searchQuery(phone_no);
                }}
              />
            </View>
          </View>
        )}

        {this.state.contactList.length ? (
          <FlatList
            data={this.state.contactList}
            initialNumToRender={20}
            maxToRenderPerBatch={30}
            updateCellsBatchingPeriod={30}
            windowSize={30}
            keyExtractor={(item) => item.id}
            keyboardShouldPersistTaps="always"
            renderItem={({ item }) => {
              return (
                <ContactList
                  OnTouchSetPhoneNo={(data) => {
                    if (!this.props.isSelectable) {
                      this.filterPhoneNo(data);
                    }
                    if (this.props.isSelectable) {
                      let numIndex = GLOBAL.smsRecipients.findIndex(
                        (number) => number == data
                      );
                      if (numIndex >= 0) {
                        GLOBAL.smsRecipients.splice(numIndex, 1);
                      } else {
                        GLOBAL.smsRecipients.push(data);
                      }
                      this.forceUpdate();
                      //console.log(GLOBAL.smsRecipients);
                    }
                  }}
                  contact={item}
                  isSelectable={this.props.isSelectable}
                />
              );
            }}
          />
        ) : (
          <View>
            {this.props.isValidNo ? (
              <View style={{ alignItems: "center", paddingTop: width * 0.06 }}>
                <Text style={{ paddingBottom: width * 0.03 }}>
                  <Text
                    style={{
                      fontSize: width * 0.065,
                      fontWeight: "bold",
                      color: "#BA2F16",
                    }}
                  >
                    Mobile Recharge
                  </Text>
                  <Text style={{ fontSize: width * 0.065, color: "#BA2F16" }}>
                    {" "}
                    for
                  </Text>
                </Text>
                <Text style={{ fontSize: width * 0.062, color: "#BA2F16" }}>
                  {this.state.phone_no}
                </Text>
                <TouchableOpacity
                  onPress={this.props.moveTo}
                  style={{ paddingTop: width * 0.03 }}
                >
                  <Text
                    style={{
                      color: "#BA2F16",
                      padding: width * 0.035,
                      fontSize: width * 0.032,
                      backgroundColor: "#E6D7DC",
                      borderRadius: 40,
                    }}
                  >
                    TAP TO CONTINUE
                  </Text>
                </TouchableOpacity>
              </View>
            ) : (
              <View style={{ alignItems: "center", paddingTop: width * 0.06 }}>
                <Text style={{ fontSize: width * 0.06, color: "#BA2F16" }}>
                  No matching contact
                </Text>
              </View>
            )}
          </View>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  inputContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingLeft: width * 0.035,
    paddingRight: width * 0.035,
    paddingBottom: width * 0.035,
    borderBottomColor: "#bdc3c7",
    borderBottomWidth: 1,
  },
  button: {
    marginTop: width * 0.06,
  },
  buttonText: {
    color: "#fff",
  },
  footer: {
    alignItems: "center",
  },
  rechargeText: {
    width: "100%",
    justifyContent: "center",
    alignItems: "center",
    marginTop: width * 0.06,
  },
});
