import React from "react";
import { StyleSheet, Text, TouchableOpacity, Dimensions } from "react-native";
import { Ionicons } from "@expo/vector-icons";
const { width } = Dimensions.get("window");

export default class CustomButton extends React.Component {
  render() {
    return (
      <TouchableOpacity
        onPress={this.props.buttonPressed}
        style={styles.button}
        activeOpacity={0.6}
      >
        <Text style={styles.buttonText}>{this.props.buttonTitle}</Text>
        <Ionicons
          style={styles.buttonIcon}
          name="md-arrow-forward"
          size={width * 0.06}
          color="#4050B5"
        />
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  button: {
    marginTop: width * 0.02,
    padding: width * 0.05,
    flexDirection: "row",
    justifyContent: "space-between",
    backgroundColor: "#EEF1F1",
    borderRadius: 5
  },
  buttonText: {
    alignItems: "center",
    fontSize: width * 0.04
  },
  buttonIcon: {
    alignItems: "center"
  }
});
