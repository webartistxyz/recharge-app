import React from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TextInput,
  Dimensions
} from "react-native";
import { Entypo } from "@expo/vector-icons";
const { width } = Dimensions.get("window");

export default class AmountTab extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      leftData: JSON.parse(JSON.stringify(this.props.leftData)),
      selectedLeftData: "",
      amount: ""
    };
  }

  changeValue = key => {
    let reserve = JSON.parse(JSON.stringify(this.props.leftData));
    reserve.forEach(element => {
      element.value = false;
    });
    let selectedLeftData = reserve[key].title;
    reserve[key].value = true;

    this.setState({ leftData: reserve, selectedLeftData });

    this.props.selectedLeftData(selectedLeftData);
    if (!this.props.notChangeInputValue)
      this.setState({ amount: selectedLeftData });
  };

  checkSelectedData(amount) {
    let reserve = JSON.parse(JSON.stringify(this.props.leftData));
    if (!this.props.notChangeInputValue)
      this.setState({ leftData: reserve, selectedLeftData: "" });
  }

  render() {
    return (
      <View style={styles.amountContainer}>
        <View style={[styles.leftData, { flex: this.props.leftDataFlex || 2 }]}>
          {this.state.leftData.map((item, key) => (
            <TouchableOpacity
              key={key}
              onPress={() => {
                this.changeValue(key);
              }}
              style={
                !item.value ? styles.amountAndSign : styles.amountAndSignPressed
              }
            >
              {!this.props.notChangeInputValue ? (
                <View>
                  <Text
                    style={[
                      !item.value
                        ? styles.textAndBorderColor
                        : styles.textAndBorderColorPressed,
                      this.props.leftDataFontSize
                    ]}
                  >
                    ৳
                  </Text>
                </View>
              ) : null}
              <View>
                <Text
                  style={[
                    !item.value
                      ? styles.textAndBorderColor
                      : styles.textAndBorderColorPressed,
                    this.props.leftDataFontSize
                      ? this.props.leftDataFontSize
                      : null
                  ]}
                >
                  {item.title}
                </Text>
              </View>
            </TouchableOpacity>
          ))}
        </View>
        <TextInput
          style={styles.amountInput}
          autoCapitalize="none"
          placeholderTextColor={"#636e72"}
          onChangeText={amount => {
            this.setState({ amount });
            this.checkSelectedData(amount);
            this.props.inputValue(amount);
          }}
          value={this.state.amount}
          autoFocus={true}
          placeholder="0"
          autoCorrect={false}
          keyboardType={"decimal-pad"}
        />
        {this.state.amount && !this.props.hideRightArrow ? (
          <TouchableOpacity
            style={styles.nextButton}
            onPress={() => {
              this.props.moveToPin(this.state);
            }}
          >
            <Entypo name="arrow-right" size={width * 0.09} color="#00CDC8" />
          </TouchableOpacity>
        ) : (
          <View style={styles.nextButton}></View>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  amountContainer: {
    flexDirection: "row",
    justifyContent: "center",

    padding: width * 0.035
  },
  leftData: {
    flexDirection: "column",
    justifyContent: "center"
  },
  amountInput: {
    alignItems: "center",
    textAlign: "center",
    flex: 8,
    fontSize: width * 0.12,
    color: "#00CDC8",
    fontWeight: "normal"
  },
  nextButton: {
    alignItems: "center",
    justifyContent: "center",
    flex: 2
  },
  amountAndSign: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    margin: width * 0.01,
    backgroundColor: "#fff",
    padding: width * 0.014,
    borderWidth: 1,
    borderColor: "#00CDC8",
    borderRadius: width * 0.12
  },
  amountAndSignPressed: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    margin: width * 0.01,
    backgroundColor: "#fff",
    padding: width * 0.014,
    borderWidth: 1,
    borderColor: "#00CDC8",
    backgroundColor: "#00CDC8",
    borderRadius: width * 0.12
  },
  textAndBorderColor: {
    color: "#00CDC8"
  },
  textAndBorderColorPressed: {
    color: "#fff"
  }
});
