import React from "react";
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  Dimensions
} from "react-native";
import { Ionicons } from "@expo/vector-icons";
const { width } = Dimensions.get("window");

export default class PreviewButton extends React.Component {
  render() {
    return (
      <TouchableOpacity
        onPress={this.props.buttonPressed}
        style={{
          flexDirection: "row",
          margin: width * 0.02,
          padding: width * 0.02,
          borderRadius: 30,
          backgroundColor: "#EEF1F1",
          justifyContent: "center",
          alignContent: "center"
        }}
      >
        <View style={{ paddingRight: width * 0.02 }}>
          <Ionicons
            name="md-arrow-dropdown"
            size={width * 0.035}
            color={this.props.iconColor || "#00CEC9"}
          />
        </View>
        <View>
          <Text
            style={{
              fontSize: width * 0.028
            }}
          >
            {this.props.buttonTitle || "SEE ALL NUMBERS"}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }
}
