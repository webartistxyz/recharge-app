import React from "react";
import { StyleSheet, View, Dimensions } from "react-native";
const { width } = Dimensions.get("window");

export default class DefaultScreen extends React.Component {
  render() {
    return (
      <View
        style={[
          styles.container,
          { backgroundColor: this.props.screenBackgroundColor || "#00cec9" }
        ]}
      >
        <View
          style={[
            styles.gridContainer,
            {
              width: width * 0.93,
              flex: !this.props.gridFlex ? 1 : 0,
              backgroundColor: this.props.contentBackgroundColor || "#fff"
            }
          ]}
        >
          {this.props.screenData}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center"
  },

  gridContainer: {
    alignItems: "center",
    marginTop: width * 0.03,
    marginBottom: width * 0.03,
    borderRadius: 3
  }
});
