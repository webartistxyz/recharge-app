import React from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions
} from "react-native";
import { Ionicons } from "@expo/vector-icons";
import GLOBAL_FUNCTIONS from "../globalFunctions";
import GLOBAL from "../globalState";
const { width } = Dimensions.get("window");

export default class ConfirmModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      progressWidth: 0,
      started: null
    };
  }

  componentDidMount() {
    //console.log(this.props.data);
  }

  modalData(text, value, rightBorder, bottomBorder = 1, key) {
    return (
      <View
        key={key}
        style={[
          styles.rechargeInfo,
          {
            borderRightWidth: rightBorder,
            borderTopWidth: 1,
            borderColor: "#DAE0E2",
            borderBottomWidth: bottomBorder
          }
        ]}
      >
        <Text style={styles.infoText}>
          {GLOBAL_FUNCTIONS.convertToUppercase(text)}
        </Text>
        <Text style={styles.infoValue}>
          {GLOBAL_FUNCTIONS.convertToUppercase(value)}
        </Text>
      </View>
    );
  }

  start = () => {
    this.setState({ started: setInterval(() => this.clockRunning(), 10) });
  };
  reset = () => {
    clearInterval(this.state.started);
    this.setState({ progressWidth: 0 });
  };

  clockRunning = () => {
    this.setState({ progressWidth: this.state.progressWidth + 1 });
    if (this.state.progressWidth === 100) {
      this.reset();
      this.props.confirmAction();
    }
  };

  dynamicWidth = () => {
    return {
      width: this.state.progressWidth + "%",
      height: width * 0.029,
      position: "absolute",
      bottom: width * 0.25,
      backgroundColor: "#00cec9",
      justifyContent: "center"
    };
  };

  render() {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: "column",
          justifyContent: "center",
          alignItems: "center",
          backgroundColor: "#00000080"
        }}
      >
        <View
          style={{
            width: "91%",
            height: "95%",
            backgroundColor: "#fff",
            borderRadius: 1
          }}
        >
          <View
            style={{
              paddingTop: width * 0.03,
              paddingBottom: width * 0.03,
              paddingRight: width * 0.05,
              paddingLeft: width * 0.05
            }}
          >
            <View>
              <TouchableOpacity onPress={this.props.toggleModal}>
                <Ionicons
                  name="md-close"
                  size={width * 0.095}
                  color="#00CDC8"
                />
              </TouchableOpacity>
            </View>

            <View style={styles.confirmText}>
              <Text>
                <Text style={{ fontSize: width * 0.06, color: "#00cec9" }}>
                  Confirm{" "}
                </Text>
                <Text
                  style={{
                    fontSize: width * 0.06,
                    fontWeight: "bold",
                    color: "#00cec9"
                  }}
                >
                  {this.props.confirmTitle}
                </Text>
              </Text>
            </View>

            <View style={styles.confirmPhoneNO}>
              <Text style={{ fontSize: width * 0.095 }}>
                {this.props.accountNo || this.props.stateData.phone_no}
              </Text>
            </View>
          </View>

          <View style={styles.rechargeInfoContainer}>
            {this.props.data.map((item, key) =>
              this.modalData(
                item.text,
                item.value,
                item.rightBorder,
                item.bottomBorder,
                key
              )
            )}
          </View>

          <View style={this.dynamicWidth()}></View>

          <TouchableOpacity
            style={styles.tapButton}
            activeOpacity={0.6}
            onPressIn={this.start}
            onPressOut={this.reset}
          >
            <View style={styles.tapButtonText}>
              <Text style={{ color: "#fff", fontSize: width * 0.06 }}>
                Tap and hold
              </Text>
            </View>
            <View style={styles.tapButtonSign}>
              <Ionicons name="md-send" size={width * 0.08} color="#fff" />
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  confirmText: {
    justifyContent: "center",
    alignItems: "center",
    paddingTop: width * 0.035
  },
  confirmPhoneNO: {
    justifyContent: "center",
    alignItems: "center",
    paddingTop: width * 0.03,
    paddingBottom: width * 0.03
  },
  rechargeInfoContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    flexWrap: "wrap"
  },
  rechargeInfo: {
    justifyContent: "center",
    alignItems: "center",
    borderColor: "#DAE0E2",
    width: "50%",
    height: width * 0.3
  },
  infoText: {
    fontSize: width * 0.034,
    fontWeight: "bold",
    color: "#00cec9"
  },
  infoValue: {
    fontSize: width * 0.03
  },
  tapButton: {
    position: "absolute",
    bottom: 0,
    paddingTop: width * 0.065,
    paddingBottom: width * 0.065,
    paddingRight: width * 0.035,
    paddingLeft: width * 0.035,
    backgroundColor: "#00cec9",
    justifyContent: "center",
    flexDirection: "row"
  },
  tapButtonText: {
    justifyContent: "center",
    flex: width * 0.035
  },
  tapButtonSign: {
    justifyContent: "center",
    flex: 2
  }
});
