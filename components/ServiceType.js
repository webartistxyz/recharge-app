import React from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions
} from "react-native";
const { width } = Dimensions.get("window");

export default class ServiceType extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      type: this.props.defaultType || ""
    };
  }

  setType(type) {
    return (
      <TouchableOpacity
        onPress={() => {
          this.setState({ type: type });
          this.props.selectedType(type);
        }}
        style={
          this.state.type === type
            ? [
                styles.activeBtn,
                {
                  borderColor: this.props.btnColor || "#00CDC8",
                  backgroundColor: this.props.btnColor || "#00CDC8"
                }
              ]
            : [
                styles.inactiveBtn,
                { borderColor: this.props.btnColor || "#00CDC8" }
              ]
        }
      >
        <View>
          <Text
            style={
              this.state.type === type
                ? styles.activeBtnTxt
                : [
                    styles.inActiveBtnTxt,
                    { color: this.props.btnColor || "#00CDC8" }
                  ]
            }
          >
            {type}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }

  render() {
    return (
      <View style={styles.typeContainer}>
        {!this.props.hideTypeTitle ? (
          <View style={styles.typeTitle}>
            <Text style={{ color: "#00cec9", fontSize: width * 0.03 }}>
              SELECT TYPE
            </Text>
          </View>
        ) : null}
        <View style={styles.types}>
          {this.setType(this.props.typeOne)}
          {this.setType(this.props.typeTwo)}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  typeContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    padding: width * 0.035,
    borderBottomWidth: 1,
    borderBottomColor: "#ecf0f1"
  },
  typeTitle: {
    color: "#00cec9",
    flex: 1
  },
  types: {
    flex: 2,
    flexDirection: "row",
    justifyContent: "center"
  },
  inactiveBtn: {
    alignItems: "center",
    justifyContent: "center",
    width: width * 0.3,
    padding: width * 0.015,
    marginEnd: width * 0.015,
    backgroundColor: "#fff",
    borderWidth: 1,
    borderRadius: width * 0.12,
    marginEnd: width * 0.029
  },
  activeBtn: {
    alignItems: "center",
    justifyContent: "center",
    width: width * 0.3,
    padding: width * 0.015,
    marginEnd: width * 0.015,
    backgroundColor: "#fff",
    borderWidth: 1,
    borderRadius: width * 0.12,
    marginEnd: width * 0.029
  },
  inActiveBtnTxt: {
    fontSize: width * 0.032
  },
  activeBtnTxt: {
    color: "#fff",
    fontSize: width * 0.032
  }
});
