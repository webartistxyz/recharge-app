import React from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions
} from "react-native";
import { Ionicons } from "@expo/vector-icons";
import GLOBAL_FUNCTIONS from "../globalFunctions";
const { width } = Dimensions.get("window");

export default class ContactList extends React.PureComponent {
  getUniqueNumbers(arr) {
    let numberArr = [];
    arr.forEach(numberDetails => {
      let onlyDigitNumber = numberDetails.number.replace(/[^\d]/g, "");
      if (onlyDigitNumber.length > 11) {
        var prefix = onlyDigitNumber.substr(0, 2);

        if (prefix == 88) {
          var accurate_no = onlyDigitNumber.substr(2);
          if (accurate_no.length === 11) {
            numberArr.push({ number: accurate_no });
          }
        }
      } else if (
        onlyDigitNumber.length === 11 &&
        onlyDigitNumber.substr(0, 2) == "01"
      ) {
        numberArr.push({ number: onlyDigitNumber });
      }
    });
    return Array.from(new Set(numberArr.map(x => x.number))).map(number => {
      return { number, isSelectable: null };
    });
  }

  render() {
    return (
      <View>
        {this.props.isSelectable ? ( // for selectable
          typeof this.props.contact.phoneNumbers === "object" ? (
            this.getUniqueNumbers(this.props.contact.phoneNumbers).map(
              (Item, key) => (
                <TouchableOpacity
                  onPress={() => {
                    if (this.props.contact.phoneNumbers[key].isSelectable) {
                      this.props.contact.phoneNumbers[key].isSelectable = false;
                      this.forceUpdate();
                      // console.log(Item.number);
                    } else {
                      this.props.contact.phoneNumbers[key].isSelectable = true;
                      this.forceUpdate();
                      // console.log(Item.number);
                    }
                    this.props.OnTouchSetPhoneNo(Item.number);
                  }}
                  key={key}
                  style={{
                    borderBottomWidth: 1,
                    borderColor: "#fcf0f0"
                  }}
                >
                  <View style={styles.listItem}>
                    <View style={styles.iconContainer}>
                      <Text style={styles.contactIcon}>
                        {typeof this.props.contact.firstName === "string"
                          ? GLOBAL_FUNCTIONS.convertToUppercase(
                              this.props.contact.firstName[0] ||
                                this.props.contact.lastName[0]
                            )
                          : ""}
                      </Text>
                    </View>

                    <View style={styles.infoContainer}>
                      <Text style={styles.infoText}>
                        {this.props.contact.firstName}{" "}
                        {this.props.contact.lastName}
                      </Text>
                      <View>
                        <Text style={styles.infoText}>{Item.number}</Text>
                      </View>
                    </View>

                    <View
                      style={{
                        position: "absolute",
                        right: width * 0.09,
                        top: "54%"
                      }}
                    >
                      {this.props.contact.phoneNumbers[key].isSelectable ? (
                        <Ionicons
                          name="md-radio-button-on"
                          size={width * 0.04}
                          color="#B83227"
                        />
                      ) : (
                        <Ionicons
                          name="md-radio-button-off"
                          size={width * 0.04}
                          color="#bdc3c7"
                        />
                      )}
                    </View>
                  </View>
                </TouchableOpacity>
              )
            )
          ) : null
        ) : (
          // not selectable

          <TouchableOpacity
            onPress={() => {
              this.props.OnTouchSetPhoneNo(this.props.contact);
            }}
            style={{
              borderBottomWidth: 1,
              borderColor: "#fcf0f0"
            }}
          >
            <View style={styles.listItem}>
              <View style={styles.iconContainer}>
                <Text style={styles.contactIcon}>
                  {typeof this.props.contact.firstName === "string"
                    ? GLOBAL_FUNCTIONS.convertToUppercase(
                        this.props.contact.firstName[0] ||
                          this.props.contact.lastName[0]
                      )
                    : ""}
                </Text>
              </View>

              <View style={styles.infoContainer}>
                <Text style={styles.infoText}>
                  {this.props.contact.firstName} {this.props.contact.lastName}
                </Text>
                <View>
                  {typeof this.props.contact.phoneNumbers === "object" ? (
                    this.props.contact.phoneNumbers.length > 1 ? (
                      <View
                        style={{
                          flexDirection: "row",
                          marginLeft: width * 0.02,
                          paddingTop: width * 0.01,
                          paddingBottom: width * 0.007,
                          paddingRight: width * 0.02,
                          paddingLeft: width * 0.02,
                          borderRadius: 30,
                          backgroundColor: "#E3D5D2",
                          justifyContent: "center",
                          alignContent: "center",
                          marginTop: width * 0.01
                        }}
                      >
                        <View style={{ paddingRight: width * 0.02 }}>
                          <Ionicons
                            name="md-arrow-dropdown"
                            size={width * 0.035}
                            color="#BA2E14"
                          />
                        </View>
                        <View>
                          <Text
                            style={{
                              fontSize: width * 0.024,
                              fontWeight: "bold"
                            }}
                          >
                            SEE ALL NUMBERS
                          </Text>
                        </View>
                      </View>
                    ) : (
                      <Text style={styles.infoText}>
                        {this.props.contact.phoneNumbers[0].number}
                      </Text>
                    )
                  ) : null}
                </View>
              </View>
            </View>
          </TouchableOpacity>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  listItem: {
    flexDirection: "row",
    paddingLeft: width * 0.05,
    paddingTop: width * 0.03,
    paddingBottom: width * 0.03,
    alignItems: "center"
  },
  iconContainer: {
    width: width * 0.12,
    height: width * 0.12,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#B83227",
    borderRadius: 100,
    marginEnd: width * 0.02
  },
  contactIcon: {
    fontSize: width * 0.04,
    color: "#fff"
  },
  infoContainer: {
    flexDirection: "column"
  },
  infoText: {
    fontSize: width * 0.04,
    fontWeight: "400",
    paddingLeft: width * 0.03,
    paddingTop: 2
  }
});
