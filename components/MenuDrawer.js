import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  AsyncStorage
} from "react-native";
import GLOBAL_FUNCTIONS from "../globalFunctions";
import { Ionicons } from "@expo/vector-icons";
const { width } = Dimensions.get("window");
import ConfirmConnection from "../ConfirmNetConnection";

export default class MenuDrawer extends React.Component {
  navLink = (nav, icon, text) => {
    return (
      <TouchableOpacity
        onPress={async () => {
          const connectionStatus = await ConfirmConnection.CheckNetConnection();
          if (connectionStatus)
            this.props.navigation.navigate(nav, { drawerMenu: true });
        }}
        style={{ height: width * 0.15 }}
      >
        <Text style={styles.link}>
          <Ionicons name={icon} size={width * 0.07} color="#4050B5" />
          <Text>{"   "}</Text>
          <Text>{text}</Text>
        </Text>
      </TouchableOpacity>
    );
  };
  render() {
    return (
      <View style={styles.container}>
        <ScrollView
          style={styles.scrollView}
          showsVerticalScrollIndicator={false}
        >
          <View style={styles.topLinks}>
            <View style={styles.profile}>
              <View style={styles.imgView}>
                <Image
                  style={styles.img}
                  source={require("../assets/o_logo.png")}
                />
              </View>
              <View style={styles.profileText}>
                <Text style={styles.name}>Osesco</Text>
              </View>
            </View>
          </View>
          <View style={styles.bottomLinks}>
            {this.navLink("Home", "md-home", "Home")}
            {this.navLink("Reports", "md-stats", "Reports")}
            {this.navLink("BalanceTransfer", "md-swap", "Balance Transfer")}
            {this.navLink("ChangePin", "md-key", "Change pin")}
            {this.navLink("ChangePass", "md-key", "Change pass")}
            {this.navLink("Signup", "md-person-add", "Add Member")}
            {this.navLink("Settings", "md-settings", "Settings")}
          </View>
        </ScrollView>
        <View style={styles.footer}>
          <TouchableOpacity
            style={styles.footerContent}
            onPress={() => {
              AsyncStorage.removeItem("user_data");
              GLOBAL_FUNCTIONS.logOut(this.props);
            }}
          >
            <Ionicons
              name="md-log-out"
              size={width * 0.08}
              color="#4050B5"
              style={{ paddingEnd: width * 0.015 }}
            />
            <Text style={{ fontSize: width * 0.04, fontWeight: "bold" }}>
              LOG OUT
            </Text>
          </TouchableOpacity>
          {/* <Text style={styles.description}>Osesco</Text>
            <Text style={styles.version}> V1.0</Text> */}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "lightgray"
  },
  scrollView: {
    flex: 1
  },
  topLinks: {
    height: width * 0.48,
    backgroundColor: "#4050B5"
  },
  profile: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    paddingTop: width * 0.075,
    borderBottomWidth: 1,
    borderBottomColor: "#777777"
  },
  profileText: {
    flex: 3,
    flexDirection: "column",
    justifyContent: "center"
  },
  name: {
    fontSize: width * 0.06,
    paddingEnd: 5,
    color: "white",
    textAlign: "left"
  },
  imgView: {
    flex: 3,
    paddingLeft: 20,
    paddingRight: 20,
    flexDirection: "column",
    justifyContent: "center"
  },
  img: {
    height: 70,
    width: 70,
    borderRadius: 50
  },
  bottomLinks: {
    flex: 1,
    backgroundColor: "white",
    paddingTop: width * 0.03,
    paddingBottom: width * 1.35
  },
  link: {
    flex: 1,
    fontSize: width * 0.06,
    padding: width * 0.016,
    paddingLeft: width * 0.034,
    margin: width * 0.015,
    textAlign: "left"
  },
  footer: {
    height: width * 0.18,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "white",
    borderTopWidth: 1,
    borderTopColor: "lightgray"
  },
  footerContent: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#EEF1F1",
    padding: width * 0.02,
    width: width * 0.35,
    borderRadius: 40
  },
  version: {
    flex: 1,
    textAlign: "right",
    marginRight: 20,
    color: "gray"
  },
  description: {
    flex: 1,
    marginLeft: 20,
    fontSize: width * 0.056
  }
});
