import React from "react";
import { StyleSheet, Text, View, Image, Dimensions } from "react-native";
import ImageEditor from "@react-native-community/image-editor";
import * as ImagePicker from "expo-image-picker";
import * as Permissions from "expo-permissions";
import { Button } from "native-base";
const { width, height } = Dimensions.get("window");

const ImageOffset = {
  x: 0,
  y: 0,
};

export default class ImageUploader extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      image: null,
      hasCamerRollPermissiion: null,
      hasCamerPermissiion: null,
    };
  }

  _crop = async (result) => {
    try {
      const croppedImageURI = await ImageEditor.cropImage(result.uri, {
        offset: ImageOffset,
        size: { width: result.width, height: result.height },
        displaySize: {
          width: this.props.imageWidth,
          height: this.props.imageHeight,
        },
        resizeMode: "cover",
      });

      if (croppedImageURI) {
        //console.log(croppedImageURI);
        this.setState({ image: croppedImageURI });
        this.props.image(this.state.image);
      }
    } catch (cropError) {
      console.log(cropError);
      //this.setState({ cropError });
    }
  };

  _pickImage = async () => {
    const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);

    this.setState({ hasCamerRollPermissiion: status === "granted" });

    if (this.state.hasCamerRollPermissiion) {
      let result = await ImagePicker.launchImageLibraryAsync({
        allowsEditing: true,
        aspect: [6, 5],
      });
      if (result.cancelled) {
        return;
      }
      this._crop(result);
    }
  };

  _takePicture = async () => {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);

    this.setState({ hasCamerPermissiion: status === "granted" });

    if (this.state.hasCamerPermissiion) {
      let result = await ImagePicker.launchCameraAsync({
        allowsEditing: true,
        aspect: [6, 5],
      });
      if (result.cancelled) {
        return;
      }
      this._crop(result);
    }
  };

  render() {
    let { image } = this.state;
    return (
      <View style={styles.gridContainer}>
        <View style={styles.picContainer}>
          {this.props.imageTitle ? (
            <Text style={{ paddingBottom: width * 0.03 }}>
              {this.props.imageTitle}
            </Text>
          ) : null}
          {image ? (
            <Image style={this.props.imageStyle} source={{ uri: image }} />
          ) : (
            <Image
              style={this.props.imageStyle}
              source={{ uri: `https://${this.props.defaultImage}` }}
            />
          )}

          {/* <Text style={{paddingTop: width* .05, fontSize: width* .05}}>Tap to add your picture</Text> */}
        </View>
        {!this.props.uploaderBtnHide ? (
          <View style={styles.buttonContainer}>
            <Button
              light
              onPress={this._pickImage}
              style={{ padding: width * 0.05, margin: width * 0.05 }}
            >
              <Text> Gallery </Text>
            </Button>
            <Button
              light
              onPress={this._takePicture}
              style={{ padding: width * 0.05, margin: width * 0.05 }}
            >
              <Text> Camera </Text>
            </Button>
          </View>
        ) : null}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  gridContainer: {
    width: width * 0.93,
    backgroundColor: "white",
    marginTop: height * 0.03,
    borderRadius: 4,
  },
  picContainer: {
    alignItems: "center",
    padding: width * 0.05,
  },
  buttonContainer: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    paddingBottom: width * 0.05,
  },
  gridContent: {
    padding: width * 0.05,
    flexDirection: "row",
    justifyContent: "space-between",
    backgroundColor: "#EEF1F1",
    borderBottomLeftRadius: 4,
    borderBottomRightRadius: 4,
  },
  contentText: {
    alignItems: "center",
    fontSize: width * 0.04,
  },
  contentIcon: {
    alignItems: "center",
  },
});
