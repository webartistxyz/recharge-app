import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  TouchableHighlight,
  Image,
} from "react-native";
import GLOBAL from "../globalState";
const { width } = Dimensions.get("window");

export default class historyContent extends React.Component {
  render() {
    return (
      <TouchableHighlight
        underlayColor="#ecf0f1"
        onPress={this.props.pressed}
        style={{
          width: width * 0.93,
          borderBottomWidth: 1,
          borderColor: "#DAE0E2",
        }}
      >
        <View style={{ flexDirection: "row", padding: width * 0.03 }}>
          <View style={{ flex: 3, paddingEnd: width * 0.032 }}>
            <View style={styles.imgageShadow}>
              <Image
                style={styles.imageSize}
                source={{ uri: `https://${this.props.serviceImg}` }}
              />
            </View>
          </View>
          <View style={{ flexDirection: "column", flex: 8 }}>
            <Text style={{ fontSize: width * 0.035, color: "#7f8c8d" }}>
              {this.props.serviceName}
            </Text>
            <Text style={{ fontSize: width * 0.05 }}>
              {this.props.serviceAcNo}
            </Text>
            {this.props.costOrTransection ? (
              <Text style={{ fontSize: width * 0.03 }}>
                {this.props.costOrTransectionTitle}
                {this.props.costOrTransection}
              </Text>
            ) : null}

            {this.props.status ? (
              <Text style={{ fontSize: width * 0.037 }}>
                <Text>{this.props.statusTitle || "Status:"} </Text>
                <Text style={{ color: this.props.statusColor }}>
                  {this.props.status}
                </Text>
              </Text>
            ) : null}
          </View>
          <View
            style={{
              flexDirection: "column",
              flex: 4,
              alignItems: "flex-end",
            }}
          >
            <Text
              style={{
                color: "#95a5a6",
                fontWeight: "bold",
                fontSize: width * 0.03,
              }}
            >
              {this.props.requestDatetime}
            </Text>
            <Text
              style={{
                fontSize: width * 0.06,
                fontWeight: "bold",
                color: this.props.amountColor || "#c0392b",
              }}
            >
              {this.props.amountSign || "-"}
              {this.props.amount}
            </Text>
          </View>
        </View>
      </TouchableHighlight>
    );
  }
}

const styles = StyleSheet.create({
  imgageShadow: {
    width: width * 0.16,
    height: width * 0.16,
    borderRadius: 100,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 4 },
    shadowOpacity: 0.3,
    shadowRadius: 2.65,
    elevation: 8,
  },
  imageSize: {
    width: width * 0.16,
    height: width * 0.16,
    borderRadius: 100,
  },
});
