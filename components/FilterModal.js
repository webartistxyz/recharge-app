import React from "react";
import { StyleSheet, View, Dimensions, TextInput, Text } from "react-native";
import AppModal from "./Modal";
import { Ionicons } from "@expo/vector-icons";
import ServiceType from "./ServiceType";
import {
  Content,
  Item,
  Picker,
  DatePicker,
  Button,
  ListItem,
  Radio,
  Right,
  Left
} from "native-base";
const { width, height } = Dimensions.get("window");
import moment from "moment";

export default class FilterModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      service: undefined,
      status: undefined,
      type: "",
      search: ""
    };
  }

  setService = service => {
    this.props.setService(service);
    this.setState({ service });
  };

  setStatus = status => {
    this.props.setStatus(status);
    this.setState({ status });
  };

  setStartDate = start_date => {
    this.props.setStartDate(moment(start_date).format("YYYY-MM-DD"));
  };
  setEndDate = end_date => {
    this.props.setEndDate(moment(end_date).format("YYYY-MM-DD"));
  };
  _filterContent = () => {
    return (
      <View style={{ padding: width * 0.04 }}>
        {this.props.type ? (
          <Item picker>
            <ServiceType
              typeOne={"SENT"}
              typeTwo={"RECEIVE"}
              defaultType={this.props.type}
              selectedType={type => this.props.typeSelected(type)}
              hideTypeTitle={true}
              btnColor="#4050B5"
            />
          </Item>
        ) : null}

        {this.props.services && this.props.services.length > 0 ? (
          <Item picker>
            <Picker
              mode="dropdown"
              iosIcon={
                <Ionicons
                  name="md-arrow-down"
                  size={width * 0.06}
                  color="#4050B5"
                />
              }
              style={{ width: undefined }}
              selectedValue={this.state.service}
              onValueChange={this.setService}
            >
              <Picker.Item label="Select service" />
              {this.props.services.map((item, key) => (
                <Picker.Item label={item.name} value={item.id} key={key} />
              ))}
            </Picker>
          </Item>
        ) : null}

        {this.props.statusList && this.props.statusList.length > 0 ? (
          <Item picker>
            <Picker
              mode="dropdown"
              iosIcon={
                <Ionicons
                  name="md-arrow-down"
                  size={width * 0.06}
                  color="#4050B5"
                />
              }
              style={{ width: undefined }}
              selectedValue={this.state.status}
              onValueChange={this.setStatus}
            >
              <Picker.Item label="Select status" />
              {this.props.statusList.map((item, key) => (
                <Picker.Item label={item.name} value={item.value} key={key} />
              ))}
            </Picker>
          </Item>
        ) : null}

        <Item picker>
          <Content>
            <DatePicker
              defaultDate={new Date()}
              locale={"en"}
              timeZoneOffsetInMinutes={undefined}
              modalTransparent={false}
              animationType={"fade"}
              androidMode={"default"}
              placeHolderText="Select start Date"
              textStyle={{ color: "black" }}
              placeHolderTextStyle={{ color: "black" }}
              onDateChange={this.setStartDate}
              disabled={false}
            />
            {/* <Text>Date: {this.state.chosenDate.toString().substr(4, 12)}</Text> */}
          </Content>
        </Item>

        <Item picker>
          <Content>
            <DatePicker
              defaultDate={new Date()}
              locale={"en"}
              timeZoneOffsetInMinutes={undefined}
              modalTransparent={false}
              animationType={"fade"}
              androidMode={"default"}
              placeHolderText="Select end Date"
              textStyle={{ color: "black" }}
              placeHolderTextStyle={{ color: "black" }}
              onDateChange={this.setEndDate}
              disabled={false}
            />
            {/* <Text>Date: {this.state.chosenDate.toString().substr(4, 12)}</Text> */}
          </Content>
        </Item>

        <Item picker>
          <TextInput
            style={{
              padding: width * 0.02,
              paddingLeft: width * 0.027,
              fontSize: width * 0.04
            }}
            autoCapitalize="none"
            placeholder="Search by account"
            autoCorrect={false}
            keyboardType="phone-pad"
            value={this.state.search}
            onChangeText={search => {
              this.setState({ search });
              this.props.search(search);
            }}
          />
        </Item>

        <Button
          onPress={this.props.buttonPressed}
          Primary
          block
          style={{ marginTop: width * 0.09 }}
        >
          <Text style={{ color: "#fff" }}> Filter Now </Text>
        </Button>
      </View>
    );
  };
  render() {
    return (
      <AppModal
        toggleModal={this.props.toggleModal}
        isModalVisible={this.props.isModalVisible}
        modalContent={this._filterContent()}
        iconColor={this.props.iconColor}
      />
    );
  }
}

const styles = StyleSheet.create({});
