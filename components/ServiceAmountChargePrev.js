import React from "react";
import { StyleSheet, Text, View, Dimensions } from "react-native";
const { width } = Dimensions.get("window");

export default class ServiceAmountChargePrev extends React.Component {
  render() {
    return (
      <View style={styles.rechargeDetails}>
        {this.props.month ? (
          <View>
            <Text style={styles.headingColor}>MONTH</Text>
            <Text>{this.props.month}</Text>
          </View>
        ) : null}
        <View>
          <Text style={styles.headingColor}>
            {this.props.amountTitle || "AMOUNT"}
          </Text>
          <Text>{this.props.amount}</Text>
        </View>
        <View>
          <Text style={styles.headingColor}>
            {this.props.chargeTitle || "CHARGE"}
          </Text>
          <Text>
            ৳{this.props.hidePlusSign ? null : "+"} {this.props.charge}
          </Text>
        </View>
        <View>
          <Text style={styles.headingColor}>
            {this.props.totalTitle || "TOTAL"}
          </Text>
          <Text>{this.props.total}</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  rechargeDetails: {
    flexDirection: "row",
    justifyContent: "space-between",
    flexWrap: "wrap",
    padding: width * 0.035,
    borderBottomWidth: 1,
    borderBottomColor: "#ecf0f1"
  },
  headingColor: {
    color: "#00cec9"
  }
});
