import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Modal,
  TouchableOpacity,
  Dimensions
} from "react-native";
import { Ionicons } from "@expo/vector-icons";
import GLOBAL_FUNCTIONS from "../globalFunctions";
const { width, height } = Dimensions.get("window");

export default class ConfirmModal extends React.Component {
  modalData(text, value, rightBorder, bottomBorder = 1, key) {
    return (
      <View
        key={key}
        style={[
          styles.rechargeInfo,
          {
            borderRightWidth: rightBorder,
            borderTopWidth: 1,
            borderColor: "#DAE0E2",
            borderBottomWidth: bottomBorder
          }
        ]}
      >
        <Text style={styles.infoText}>
          {GLOBAL_FUNCTIONS.convertToUppercase(text)}
        </Text>
        <Text style={styles.infoValue}>
          {GLOBAL_FUNCTIONS.convertToUppercase(value)}
        </Text>
      </View>
    );
  }

  render() {
    return (
      <Modal
        animationType="none"
        transparent={true}
        visible={this.props.isSuccessModalVisible}
        onShow={() => {
          this.forceUpdate(), this._touchable.touchableHandlePress();
        }}
        onRequestClose={() => {
          this.props.toggleModal();
        }}
      >
        <View
          style={{
            flex: 1,
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: "#00000080"
          }}
        >
          <View
            style={{
              width: "91%",
              height: "95%",
              backgroundColor: "#fff",
              borderRadius: 1
            }}
          >
            <View
              style={{
                paddingTop: height * 0.02,
                paddingBottom: height * 0.03,
                paddingRight: width * 0.06,
                paddingLeft: width * 0.06
              }}
            >
              <View style={styles.confirmText}>
                <Text>
                  <Text style={{ fontSize: width * 0.08, color: "#00cec9" }}>
                    Your{" "}
                  </Text>
                  <Text
                    style={{
                      fontSize: width * 0.08,
                      fontWeight: "bold",
                      color: "#00cec9"
                    }}
                  >
                    {this.props.confirmTitle}
                  </Text>
                  <Text style={{ fontSize: width * 0.08, color: "#00cec9" }}>
                    {" "}
                    is{" "}
                  </Text>
                </Text>
                <Text
                  style={{
                    fontSize: width * 0.06,
                    fontWeight: "bold",
                    color: "#27ae60"
                  }}
                >
                  successful
                </Text>
              </View>

              <View style={styles.confirmPhoneNO}>
                <Text style={{ fontSize: width * 0.095 }}>
                  {this.props.stateData.phone_no || this.props.accountNo}
                </Text>
              </View>
            </View>

            <View style={styles.rechargeInfoContainer}>
              {this.props.data.map((item, key) =>
                this.modalData(
                  item.text,
                  item.value,
                  item.rightBorder,
                  item.bottomBorder,
                  key
                )
              )}
            </View>
            <TouchableOpacity
              ref={touchable => (this._touchable = touchable)}
            ></TouchableOpacity>

            <TouchableOpacity
              style={styles.backButton}
              activeOpacity={0.7}
              onPress={this.props.toggleModal}
            >
              <View style={styles.backButtonText}>
                <Text style={{ color: "#fff", fontSize: width * 0.06 }}>
                  Back to Home
                </Text>
              </View>
              <View style={styles.backButtonSign}>
                <Ionicons
                  name="md-arrow-forward"
                  size={width * 0.075}
                  color="#fff"
                />
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  confirmText: {
    justifyContent: "center",
    alignItems: "center",
    paddingTop: height * 0.045
  },
  confirmPhoneNO: {
    justifyContent: "center",
    alignItems: "center",
    paddingTop: height * 0.02,
    paddingBottom: height * 0.02
  },
  rechargeInfoContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    flexWrap: "wrap"
  },
  rechargeInfo: {
    justifyContent: "center",
    alignItems: "center",
    borderBottomWidth: 1,
    borderColor: "#DAE0E2",
    width: "50%",
    height: height * 0.12
  },
  infoText: {
    fontSize: width * 0.034,
    fontWeight: "bold",
    color: "#00cec9"
  },
  infoValue: {
    fontSize: width * 0.03
  },
  backButton: {
    position: "absolute",
    bottom: 0,
    paddingLeft: width * 0.03,
    paddingRight: width * 0.03,
    paddingTop: height * 0.04,
    paddingBottom: height * 0.04,
    backgroundColor: "#00cec9",
    justifyContent: "center",
    flexDirection: "row"
  },
  backButtonText: {
    justifyContent: "center",
    flex: 15
  },
  backButtonSign: {
    justifyContent: "center",
    flex: 1
  }
});
