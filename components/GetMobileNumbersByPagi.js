import React from "react";
import ContactList from "./ContactList";
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  Modal,
  TouchableOpacity,
  Dimensions,
} from "react-native";

import { Input, Button } from "native-base";
import * as Permissions from "expo-permissions";
import * as Contacts from "expo-contacts";
import { Entypo } from "@expo/vector-icons";
import GLOBAL_FUNCTIONS from "../globalFunctions";
const { width } = Dimensions.get("window");
let arrayholder = [];

export default class GetMobileNumbersByPagi extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      phone_no: "",
      hasContactPermissiion: null,
      contactList: [],
      contactName: "",
      phoneNoArr: [],
      isModalVisible: false,
    };
  }

  async componentDidMount() {
    const { status } = await Permissions.askAsync(Permissions.CONTACTS);

    this.setState({
      hasContactPermissiion: status === "granted",
    });
    this.takeContacts();
  }

  takeContacts = async () => {
    if (this.state.hasContactPermissiion) {
      //const { data } = await Contacts.getContactsAsync();
      let hasMore = true;
      let new_contacts;
      let contacts = [];
      let offset = 0;
      let pageSize = 1000;

      while (hasMore) {
        new_contacts = await Contacts.getContactsAsync({
          fields: [Contacts.PHONE_NUMBERS, Contacts.EMAILS],
          pageOffset: offset,
          pageSize: pageSize,
        });
        contacts = contacts.concat(new_contacts);
        hasMore = new_contacts.hasNextPage;
        offset += pageSize;
      }

      //   if (contacts.length) {
      //     this.setState({ contactList: contacts[0].data });
      //   }

      if (contacts.length > 0) {
        let allContacts = contacts[0].data;
        this.setState(
          {
            contactList: allContacts.sort(function (a, b) {
              x = a.firstName;
              y = b.firstName;

              if (
                GLOBAL_FUNCTIONS.convertToUppercase(x) <
                GLOBAL_FUNCTIONS.convertToUppercase(y)
              ) {
                return -1;
              }
              if (
                GLOBAL_FUNCTIONS.convertToUppercase(x) >
                GLOBAL_FUNCTIONS.convertToUppercase(y)
              ) {
                return 1;
              }
              return 0;
            }),
          },
          () =>
            (arrayholder = JSON.parse(JSON.stringify(this.state.contactList)))
        );
      }
    }
  };

  formatNumber = (phone_no) => {
    let onlyDigitNumber = phone_no.replace(/[^\d]/g, "");
    //console.log(onlyDigitNumber);
    if (onlyDigitNumber.length > 11) {
      var prefix = onlyDigitNumber.substr(0, 2);

      if (prefix == 88) {
        var accurate_no = onlyDigitNumber.substr(2);
        if (accurate_no.length === 11) {
          var prefix = accurate_no.substr(0, 3);

          this.setState({ phone_no: accurate_no });
          this.props.mobileNOAndPrefix(accurate_no, prefix);
        }
      } else {
        this.setState({ phone_no: "" });
        alert("Invalid Mobile Number");
      }
    } else if (
      onlyDigitNumber.length === 11 &&
      onlyDigitNumber.substr(0, 2) == "01"
    ) {
      var prefix = phone_no.substr(0, 3);

      this.setState({ phone_no: onlyDigitNumber });
      this.props.mobileNOAndPrefix(onlyDigitNumber, prefix);
    } else {
      this.setState({ phone_no: "" });
      alert("Invalid Mobile Number");
    }
  };

  toggleModal = () => {
    this.setState({ isModalVisible: !this.state.isModalVisible });
  };

  filterPhoneNo = (contact) => {
    let numberObj = contact.phoneNumbers;
    if (typeof numberObj === "object") {
      if (numberObj.length > 1) {
        this.setState({
          contactName: contact.lastName
            ? contact.firstName + " " + contact.lastName
            : contact.firstName,
          phoneNoArr: contact.phoneNumbers,
        });
        this.toggleModal();
      } else {
        this.setState({
          contactName: contact.lastName
            ? contact.firstName + " " + contact.lastName
            : contact.firstName,
          phoneNoArr: contact.phoneNumbers,
        });
        this.formatNumber(numberObj[0].number);
      }
    }
  };

  searchQuery = (phone_no) => {
    var newData = [];

    if (phone_no.length && arrayholder.length) {
      newData = arrayholder.filter((contactDetails) => {
        return typeof contactDetails.phoneNumbers === "object"
          ? Object.keys(contactDetails.phoneNumbers).some((key) => {
              //let string = String()
              let JsonString = JSON.stringify(contactDetails.phoneNumbers[key]);
              let string = String(JsonString);
              let numberString = String(phone_no);

              //console.log(phone_no)
              return string.indexOf(numberString) > -1;
            })
          : "";
      });
      this.setState({
        contactList: newData,
      });
    } else {
      this.setState({
        contactList: arrayholder,
      });
    }
  };

  render() {
    return (
      <View style={styles.container} behavior="padding" enabled>
        <Modal
          animationType="none"
          transparent={false}
          visible={this.state.isModalVisible}
          presentationStyle="formSheet"
          onRequestClose={this.toggleModal}
        >
          <View
            style={{
              flex: 1,
              flexDirection: "column",
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: "#00000080",
            }}
          >
            <View
              style={{
                width: width * 0.9,
                backgroundColor: "#fff",
                borderRadius: 5,
              }}
            >
              <View
                style={{
                  paddingTop: width * 0.03,
                  paddingBottom: width * 0.058,
                  paddingRight: width * 0.058,
                  paddingLeft: width * 0.058,
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <View style={{ marginBottom: width * 0.06 }}>
                  <Text style={{ fontSize: width * 0.09, fontWeight: "bold" }}>
                    {this.state.contactName}
                  </Text>
                </View>
                <View>
                  {this.state.phoneNoArr.map((item, key) => (
                    <Button
                      style={styles.button}
                      rounded
                      info
                      key={key}
                      onPress={() => {
                        this.formatNumber(item.number);
                        this.toggleModal();
                      }}
                    >
                      <Text
                        style={{
                          color: "#fff",
                          paddingLeft: width * 0.03,
                          paddingRight: width * 0.03,
                          fontSize: width * 0.06,
                        }}
                      >
                        {item.number}
                      </Text>
                    </Button>
                  ))}
                </View>
                <Button
                  style={styles.button}
                  full
                  danger
                  rounded
                  onPress={this.toggleModal}
                >
                  <Text style={styles.buttonText}>Cancel</Text>
                </Button>
              </View>
            </View>
          </View>
        </Modal>

        <Text
          style={{
            fontSize: width * 0.036,
            color: "#BA2F16",
            paddingLeft: width * 0.05,
            paddingTop: width * 0.03,
          }}
        >
          Mobile No
        </Text>
        <View style={styles.inputContainer}>
          <View style={{ flex: 10 }}>
            <Input
              autoCapitalize="none"
              placeholder="01XXXXXXXXX"
              autoFocus={false}
              autoCorrect={false}
              keyboardType="phone-pad"
              value={this.state.phone_no}
              onChangeText={(phone_no) => {
                this.props.setPhoneNO(phone_no);
                this.setState({ phone_no: phone_no });
                this.searchQuery(phone_no);
              }}
            />
          </View>
          <View style={{ flex: 1 }}>
            {this.props.isValidNo ? (
              <TouchableOpacity
                style={styles.nextButton}
                onPress={this.props.moveTo}
              >
                <Entypo
                  name="arrow-right"
                  size={width * 0.09}
                  color="#B83227"
                />
              </TouchableOpacity>
            ) : (
              <View style={styles.nextButton}>
                <Entypo
                  name="arrow-right"
                  size={width * 0.09}
                  color="#bdc3c7"
                />
              </View>
            )}
          </View>
        </View>

        {this.state.contactList.length ? (
          <FlatList
            data={this.state.contactList}
            initialNumToRender={10}
            maxToRenderPerBatch={30}
            updateCellsBatchingPeriod={30}
            windowSize={50}
            keyExtractor={(item) => item.id}
            keyboardShouldPersistTaps="always"
            renderItem={({ item }) => {
              return (
                <ContactList
                  OnTouchSetPhoneNo={(data) => {
                    this.filterPhoneNo(data);
                  }}
                  contact={item}
                />
              );
            }}
          />
        ) : (
          <View>
            {this.props.isValidNo ? (
              <View style={{ alignItems: "center", paddingTop: width * 0.06 }}>
                <Text style={{ paddingBottom: width * 0.03 }}>
                  <Text
                    style={{
                      fontSize: width * 0.065,
                      fontWeight: "bold",
                      color: "#BA2F16",
                    }}
                  >
                    Mobile Recharge
                  </Text>
                  <Text style={{ fontSize: width * 0.065, color: "#BA2F16" }}>
                    {" "}
                    for
                  </Text>
                </Text>
                <Text style={{ fontSize: width * 0.062, color: "#BA2F16" }}>
                  {this.state.phone_no}
                </Text>
                <TouchableOpacity
                  onPress={this.props.moveTo}
                  style={{ paddingTop: width * 0.03 }}
                >
                  <Text
                    style={{
                      color: "#BA2F16",
                      padding: width * 0.035,
                      fontSize: width * 0.032,
                      backgroundColor: "#E6D7DC",
                      borderRadius: 40,
                    }}
                  >
                    TAP TO CONTINUE
                  </Text>
                </TouchableOpacity>
              </View>
            ) : (
              <View style={{ alignItems: "center", paddingTop: width * 0.06 }}>
                <Text style={{ fontSize: width * 0.06, color: "#BA2F16" }}>
                  No matching contact
                </Text>
              </View>
            )}
          </View>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  inputContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    padding: width * 0.035,
    borderBottomColor: "#bdc3c7",
    borderBottomWidth: 1,
  },
  button: {
    marginTop: width * 0.06,
  },
  buttonText: {
    color: "#fff",
  },
  footer: {
    alignItems: "center",
  },
  rechargeText: {
    width: "100%",
    justifyContent: "center",
    alignItems: "center",
    marginTop: width * 0.06,
  },
});
