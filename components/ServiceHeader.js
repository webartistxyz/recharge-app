import React from "react";
import { Text, View, Dimensions, Image } from "react-native";
import PreviewButton from "../components/PreviewButton";
const { width } = Dimensions.get("window");

export default class ServiceHeader extends React.Component {
  render() {
    return (
      <View
        style={{
          width: width * 0.93,
          borderBottomWidth: 2,
          borderColor: "#00cec9"
        }}
      >
        <View style={{ flexDirection: "row", padding: width * 0.03 }}>
          <Text style={{ fontSize: width * 0.03, flex: 3, color: "#00cec9" }}>
            {this.props.title || "To"}
          </Text>

          {typeof this.props.centerData === "object" &&
          this.props.centerData.length > 1 ? (
            <View style={{ flex: 15 }}>
              <PreviewButton buttonPressed={this.props.buttonPressed} />
            </View>
          ) : (
            <View style={{ flex: 15 }}>
              <View>
                <Text style={{ fontSize: width * 0.06 }}>
                  {typeof this.props.centerData === "object"
                    ? this.props.centerData[0]
                    : this.props.centerData}
                </Text>
                {this.props.centerDataName ? (
                  <Text style={{ color: "#00cec9" }}>
                    {this.props.centerDataName}
                  </Text>
                ) : null}
              </View>
            </View>
          )}

          <View
            style={{
              flex: 3,
              paddingEnd: width * 0.032,
              justifyContent: "center"
            }}
          >
            <View>
              <Image
                style={this.props.imgHeightWidth}
                source={this.props.imgName}
              />
            </View>
          </View>
          {this.props.serviceName ? (
            <View
              style={{
                flexDirection: "column",
                flex: 12,
                justifyContent: "center"
              }}
            >
              <Text style={{ fontSize: width * 0.05 }}>
                {this.props.serviceName}
              </Text>
              {this.props.desc ? (
                <Text style={{ fontSize: width * 0.036 }}>
                  {this.props.desc}
                </Text>
              ) : null}
            </View>
          ) : null}
        </View>
      </View>
    );
  }
}
