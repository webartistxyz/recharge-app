import React from "react";
import { StyleSheet, View, Modal, TouchableOpacity } from "react-native";
import { Ionicons } from "@expo/vector-icons";

export default class AppModal extends React.Component {
  render() {
    return (
      <Modal
        animationType="none"
        transparent={true}
        visible={this.props.isModalVisible}
        onRequestClose={this.props.toggleModal}
      >
        <View
          style={{
            flex: 1,
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: "#00000080"
          }}
        >
          <View
            style={{
              width: this.props.modalWidth || "91%",
              height: this.props.modalHeight || "95%",
              backgroundColor: "#fff",
              borderRadius: 3
            }}
          >
            <View
              style={{
                paddingTop: 10,
                paddingBottom: 10,
                paddingRight: 18,
                paddingLeft: 18
              }}
            >
              <TouchableOpacity onPress={this.props.toggleModal}>
                <Ionicons
                  name="md-close"
                  size={35}
                  color={this.props.iconColor || "#00CDC8"}
                />
              </TouchableOpacity>
            </View>
            {this.props.modalContent}
          </View>
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({});
