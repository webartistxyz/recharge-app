import React from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  TextInput,
} from "react-native";
import { Entypo } from "@expo/vector-icons";
const { width } = Dimensions.get("window");

export default class AppInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      inputValue: this.props.setInputValue || "",
    };
  }

  render() {
    return (
      <View>
        {this.props.inputTitle ? (
          <View
            style={{
              justifyContent: "center",
              alignItems: "center",
              paddingTop: "2%",
            }}
          >
            <Text
              style={{
                color: "#00326C",
                fontSize: width * 0.031,
                fontWeight: "bold",
                paddingBottom: "1%",
              }}
            >
              {this.props.inputTitle}
            </Text>
          </View>
        ) : null}

        <View style={styles.inputContainer}>
          <View style={[styles.nextButton, { flex: 1 }]}>
            <Entypo
              name={this.props.leftIcon}
              size={width * 0.09}
              color={this.props.leftIconColor}
            />
          </View>
          <TextInput
            style={{ flex: 8, textAlign: "center" }}
            autoCapitalize="none"
            placeholderTextColor={this.props.placeholderTextColor}
            onChangeText={(inputValue) => {
              this.setState({ inputValue });
              this.props.inputValue(inputValue);
            }}
            value={this.state.inputValue}
            autoFocus={this.props.autoFocus}
            placeholder={this.props.placeHolder}
            keyboardType={this.props.keyboardType}
            autoCorrect={false}
            secureTextEntry={this.props.secureTextEntry || false}
            password={this.props.secureTextEntry || false}
          />
          <View
            style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
          >
            {this.state.inputValue.length > 3 && !this.props.hideRightArrow ? (
              <TouchableOpacity
                style={styles.nextButton}
                onPress={this.props.next}
              >
                <Entypo
                  name="arrow-right"
                  size={width * 0.09}
                  color={this.props.rightIconColor || "#4050B5"}
                />
              </TouchableOpacity>
            ) : (
              <View style={styles.nextButton}>
                <Entypo
                  name="arrow-right"
                  size={width * 0.09}
                  color="#bdc3c7"
                />
              </View>
            )}
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  inputContainer: {
    flexDirection: "row",
    justifyContent: "center",
    padding: width * 0.035,
  },
  nextButton: {
    alignItems: "center",
    justifyContent: "center",
  },
});
