import React from 'react';
import { View, Modal, Image, Dimensions } from 'react-native';
const { width } = Dimensions.get('window');

export default class LoaderModal extends React.Component {

  render() {
    return (
        <Modal
            animationType="none"
            transparent={true}
            visible={this.props.isLoading}>
            
            <View style={{flex: 1, flexDirection: 'column', justifyContent: 'center', alignItems: 'center', backgroundColor: '#00000080'}}>
            <View style={{width: width*.5, height: width*.5, backgroundColor: "#fff", borderRadius: 1, justifyContent: 'center', alignItems: 'center'}}>
                <Image source={require('../assets/o_loader.gif')} />
            </View>
            </View>
        </Modal>
    );
  }

}
