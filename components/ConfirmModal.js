import React from "react";
import { View, Modal, Image } from "react-native";
import ConfirmModalData from "./ConfirmModalData";

export default class ConfirmModal extends React.Component {
  render() {
    return (
      <Modal
        animationType="none"
        transparent={true}
        visible={this.props.isModalVisible}
        onRequestClose={() => this.props.toggleModal()}
      >
        {this.props.stateData.isLoading ? (
          <View
            style={{
              flex: 1,
              flexDirection: "column",
              justifyContent: "center",
              alignItems: "center",
              backgroundColor: "#33313B"
            }}
          >
            <Image
              style={{ width: "100%", height: "100%" }}
              source={require("../assets/rocket.gif")}
            />
          </View>
        ) : (
          <ConfirmModalData
            stateData={this.props.stateData}
            data={this.props.data}
            toggleModal={this.props.toggleModal}
            confirmAction={this.props.confirmAction}
            confirmTitle={this.props.confirmTitle}
            accountNo={this.props.accountNo}
          />
        )}
      </Modal>
    );
  }
}
