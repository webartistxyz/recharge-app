import React from "react";
import { Text, View, Dimensions } from "react-native";
import GLOBAL from "../globalState";
const { width } = Dimensions.get("window");

export default class AvailAbleBalance extends React.Component {
  render() {
    return (
      <View
        style={{
          alignItems: "center",
          justifyContent: "center",
          paddingBottom: width * 0.1,
          paddingTop: width * 0.04
        }}
      >
        <Text style={{ fontSize: width * 0.05, color: "#00cec9" }}>
          Available Balance ৳{GLOBAL.userBalance}
        </Text>
      </View>
    );
  }
}
