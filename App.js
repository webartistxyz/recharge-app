import React from "react";
import { Dimensions } from "react-native";
import {
  createStackNavigator,
  createDrawerNavigator,
  createAppContainer,
} from "react-navigation";
import MenuDrawer from "./components/MenuDrawer";
const WIDTH = Dimensions.get("window").width;
const DrawerConfig = {
  drawerWidth: WIDTH * 0.83,
  contentComponent: ({ navigation }) => <MenuDrawer navigation={navigation} />,
};

//import all screen

import UserNameScreeen from "./screens/UserNameScreeen";
import PasswordScreen from "./screens/PasswordScreen";
import SignupScreen from "./screens/SignupScreen";
import HomeScreen from "./screens/HomeScreen";
import PinScreen from "./screens/PinScreen";
import PhoneNoScreen from "./screens/recharge/PhoneNoScreen";
import AmountScreen from "./screens/recharge/AmountScreen";
import OperatorScreen from "./screens/recharge/OperatorScreen";
import RechargePinScreen from "./screens/recharge/RechargePinScreen";
import SuccessScreen from "./screens/recharge/SuccessScreen";
import WebViewScreen from "./screens/webview/WebViewScreen";
import AllBillsScreen from "./screens/billpay/AllBillsScreen";
import PayBillScreen from "./screens/billpay/PayBillScreen";
import BillPinScreen from "./screens/billpay/BillPinScreen";
import PymentServiceScreen from "./screens/payment_services/PymentServiceScreen";
import ServiceAmountScreen from "./screens/payment_services/ServiceAmountScreen";
import ServicePinScreen from "./screens/payment_services/ServicePinScreen";
import SmsScreen from "./screens/sms/SmsScreen";
import SmsPhoneNoScreen from "./screens/sms/SmsPhoneNoScreen";
import SmsPinScreen from "./screens/sms/SmsPinScreen";
import BalanceTransferScreen from "./screens/balance_transfer/BalanceTransferScreen";
import BlanceTransferPinScreen from "./screens/balance_transfer/BlanceTransferPinScreen";
import ReportsScreen from "./screens/report/ReportsScreen";
import RechargeReportScreen from "./screens/report/RechargeReportScreen";
import TransferReportScreen from "./screens/report/TransferReportScreen";
import TransactionReportScreen from "./screens/report/TransactionReports";

//dreawer screens
import HistoryScreen from "./screens/history/HistoryScreen";
import ChangePinScreen from "./screens/profile/ChangePinScreen";
import ChangePasswordScreen from "./screens/profile/ChangePasswordScreen";
import SettingsScreen from "./screens/settings/SettingsScreen";
import ChangePictureScreen from "./screens/profile/ChangePictureScreen";
import NidScreen from "./screens/profile/NidScreen";

const StackNavigator = createStackNavigator(
  {
    Home: { screen: HomeScreen },
    Recharge: { screen: PhoneNoScreen },
    AmountScreen: { screen: AmountScreen },
    Operator: { screen: OperatorScreen },
    RechargePin: { screen: RechargePinScreen },
    SuccessScreen: { screen: SuccessScreen },
    BillPay: { screen: AllBillsScreen },
    PayBill: { screen: PayBillScreen },
    BillPin: { screen: BillPinScreen },
    PymentService: { screen: PymentServiceScreen },
    ServiceAmount: { screen: ServiceAmountScreen },
    ServicePin: { screen: ServicePinScreen },
    SMS: { screen: SmsScreen },
    SmsPhoneNunbers: { screen: SmsPhoneNoScreen },
    SmsPin: { screen: SmsPinScreen },
    BalanceTransfer: { screen: BalanceTransferScreen },
    BlanceTransferPin: { screen: BlanceTransferPinScreen },
    Reports: { screen: ReportsScreen },
    RechargeReport: { screen: RechargeReportScreen },
    TransferReport: { screen: TransferReportScreen },
    TransactionReport: { screen: TransactionReportScreen },

    History: { screen: HistoryScreen },
    ChangePin: { screen: ChangePinScreen },
    ChangePass: { screen: ChangePasswordScreen },
    Settings: { screen: SettingsScreen },
    ChangePicture: { screen: ChangePictureScreen },
    Nid: { screen: NidScreen },
    Signup: { screen: SignupScreen },
  },
  { headerLayoutPreset: "center" }
);

const DrawerNavigator = createDrawerNavigator(
  {
    Home: { screen: StackNavigator },
  },
  DrawerConfig,
  {
    defaultNavigationOptions: {
      header: null,
    },
  }
);

const MainNavigator = createStackNavigator(
  {
    UserName: { screen: UserNameScreeen },
    Signup: { screen: SignupScreen },
    WebView: { screen: WebViewScreen },
    Password: { screen: PasswordScreen },
    PinScreen: { screen: PinScreen },
    Home: { screen: DrawerNavigator },
  },
  {
    defaultNavigationOptions: {
      header: null,
    },
  }
);

//create app container
const App = createAppContainer(MainNavigator);
export default App;
